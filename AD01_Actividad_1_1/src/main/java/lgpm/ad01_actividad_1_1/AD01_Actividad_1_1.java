/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lgpm.ad01_actividad_1_1;

import java.io.File;
import java.io.IOException;

/**
 *
 * @author lgpmc
 */
public class AD01_Actividad_1_1 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        String strDir;
        if (args.length < 1) {
            System.out.println(
                    "No se introdujo ningún directorio. Utilizaremos el directorio actual");
            strDir = "."; // representa el directorio actual
        } else {
            strDir = args[0]; // tomamos el directorio introducido por usuario
        }
        
        File directorio = new File(strDir);
        if (!directorio.isDirectory()) {
            System.out.println(
                    "El argumento no es un directorio. Utilizaremos el directorio actual");
            strDir = ".";
            directorio = new File(strDir);
        }
        
        try {
            System.out.printf("Listado del directorio: %s%n", 
                    directorio.getCanonicalPath());
            //línea separadora del listado
            System.out.println("=".repeat(24 + directorio.getCanonicalPath().length()));
        } catch (IOException ioEx) {
            System.out.println(ioEx.getMessage());
        }
        
        for (File f: directorio.listFiles()) {
            System.out.printf("%-50s %s%n", 
                    f.getName(), f.isDirectory()?"<DIR>":f.length());
        }
    }
    
}
