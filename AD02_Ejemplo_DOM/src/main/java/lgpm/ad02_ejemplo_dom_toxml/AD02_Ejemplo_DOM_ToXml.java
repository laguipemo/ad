/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lgpm.ad02_ejemplo_dom_toxml;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.DOMException;
import org.w3c.dom.DOMImplementation;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Text;

/**
 *
 * @author lgpmc
 */
public class AD02_Ejemplo_DOM_ToXml {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();
        try {
            DocumentBuilder docBuilder = docBuilderFactory.newDocumentBuilder();
            DOMImplementation domImplementation = docBuilder.getDOMImplementation();
            Document documento = domImplementation.createDocument(null, "empleados", null);
            documento.setXmlVersion("1.0");
            
            double salarioJuan = 1048.50;
            addEmplado(documento, "Juan Pérez", "001", "00000001F", Double.toString(salarioJuan));
            addEmplado(documento, "Alain Brito", "002", "00000002G", "2000.00");
            
            transformToXml(documento);
            
        } catch (ParserConfigurationException parsconfEx) {
            System.out.println(parsconfEx.getMessage());
        } catch (TransformerConfigurationException transfconfEx) {
            System.out.println(transfconfEx.getMessage());
        } catch (IOException ioEx) {
            System.out.println(ioEx.getMessage());
        } catch (TransformerException transfEx) {
            System.out.println(transfEx.getMessage());
        }
    }

    private static void addEmplado(
            Document documento, String nomEmpleado, String codEmpleado, 
            String dniEmpleado, String salEmpleado) throws DOMException {
        
        Element empleado = documento.createElement("empleado");
        empleado.setAttribute("codigo", codEmpleado);
        
        Text text;
        
        Element nombre = documento.createElement("nombre");
        text = documento.createTextNode(nomEmpleado);
        nombre.appendChild(text);
        empleado.appendChild(nombre);
        
        Element dni = documento.createElement("dni");
        text = documento.createTextNode(dniEmpleado);
        dni.appendChild(text);
        empleado.appendChild(dni);
        
        Element salario = documento.createElement("salario");
        text = documento.createTextNode(salEmpleado);
        salario.appendChild(text);
        empleado.appendChild(salario);
        
        documento.getDocumentElement().appendChild(empleado);
    }

    private static void transformToXml(Document documento) throws TransformerConfigurationException, 
            IOException, TransformerException, TransformerFactoryConfigurationError {
        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        Transformer transformer = transformerFactory.newTransformer();
        transformer.setOutputProperty(OutputKeys.INDENT, "yes");
        
        Source source = new DOMSource(documento);
        Result result = new StreamResult(new OutputStreamWriter(
                new FileOutputStream("mis_empleados.xml", false), "UTF-8"));
//        Result result = new StreamResult(System.out);
        transformer.transform(source, result);
    }
    
}
