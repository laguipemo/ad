/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lgpm.ad02_ejemplo_dom_fromxml;

import java.io.File;
import java.io.IOException;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 *
 * @author lgpmc
 */
public class AD02_Ejemplo_DOM_FromXml {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();
        try {
            DocumentBuilder docBuilder = docBuilderFactory.newDocumentBuilder();
            //A partir del DocumentBuilder, en este caso, obtenemos un parse que
            //nos permite leer el XML y convertirlo en un documento DOM.
            Document documento = docBuilder.parse(new File("mis_empleados.xml"));
            //Del documento obtenido con el parse obetemos su elemento ejemplar y
            //lo normalizamos para obtener los resultado correctos. 
            documento.getDocumentElement().normalize();
            //Obetenemos el jemplar normalizado y de él su nombre mediante el método
            //getNodeName()
            String nombreEjemplar = documento.getDocumentElement().getNodeName();
            System.out.printf("El nombre del ejemplar es: %s%n", nombreEjemplar);
            
            //Conociendo que el ejemplar del fichero a leer, está contituido por
            //varios elementos que son nodos de tipo "empleado" entonces podemos
            //obtner el objeto NodeList que contine cada uno de estos nodos empleado
            //mediante el método getElementByTagName("empleado")
            NodeList elementosEmpleados = documento.getElementsByTagName("empleado");
            
            //Variable de tipo Nodo en la que iremos almacenando cado uno de los
            //nodos leidos mientra recorremos el NodeList
            Node nodo;
            //Ciclo for para recorrer cada elemento del NodoList (no admite un bucle
            //foreach) por ello utilizamos un for tradicional concientod el tamaño
            //del NodeList.
            for (int i=0; i<elementosEmpleados.getLength(); i++) {
                //nodo correspondiente al empleado con indice i en el NodeList 
                nodo = elementosEmpleados.item(i);
                System.out.printf("Este nodo es: %s%n", nodo.getNodeName());
                //Si el nodo es de tipo ELEMENT_NODE (no es terminal) accedo a sus
                //elementos internos
                if (nodo.getNodeType() == Node.ELEMENT_NODE) {
                    // Hago casting a un objeto de tipo Element al nodo para acceder 
                    // correctamente a su atributo y los diferentes elementos que 
                    // lo componen
                    Element elemento = (Element) nodo;
                    
                    // obtengo el valor asociado al atributo codigo y lo imprimo
                    String codEmpleado = elemento.getAttribute("codigo");
                    System.out.printf("El código de este empleado es: %s%n", codEmpleado);
                    // obtengo, en este elemento nodo, los elementos con etiqueta nombre
                    // y como siempre devuelve un NodeList, aunque existe un solo 
                    // elemento con esa etiqueta, es por ello que lo recuperamos como
                    // el item(0). Finalmente extraemos el contendido de texto entre
                    // las etiquitas de apertura y cierre que en este caso es el nombre
                    // del empleado.
                    String nomEpleado = elemento.getElementsByTagName("nombre").item(0).getTextContent();
                    System.out.printf("El nonbre del empleado es: %s%n", nomEpleado);
                    // De igual modo al caso anterior recuperamos entonces los datos
                    // sobre dni y el salario del empleado
                    String dniEmpleado = elemento.getElementsByTagName("dni").item(0).getTextContent();
                    System.out.printf("El dni del empleado es: %s%n", dniEmpleado);
                    String strSalEmpleado = elemento.getElementsByTagName("salario").item(0).getTextContent();
                    double salEmpleado = Double.valueOf(strSalEmpleado);
                    System.out.printf("El salario del empleado es: %.2f%n", salEmpleado);
                }
            }
            
        } catch (ParserConfigurationException parsconfEx) {
            System.out.println(parsconfEx.getMessage());
        } catch (SAXException saxEx) {
            System.out.println(saxEx.getMessage());
        } catch (IOException ioEx) {
            System.err.println(ioEx.getMessage());
        }
        
    }
    
}
