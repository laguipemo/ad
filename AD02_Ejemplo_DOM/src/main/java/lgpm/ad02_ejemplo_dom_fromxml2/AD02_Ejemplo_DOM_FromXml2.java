/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lgpm.ad02_ejemplo_dom_fromxml2;

import java.io.File;
import java.io.IOException;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 *
 * @author lgpmc
 */
public class AD02_Ejemplo_DOM_FromXml2 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // path del docuemtno .xml que se leerá, se pudiera prepara para obtenerlo
        // desde la línea de comando en el array args.
        String strPathFile = "tvShows.xml";
        // obtengo el documento traducido a DOM y normalizado
        Document docXml = getDocXml(new File(strPathFile));
        // obtendo el nombre o etiqueta del ejemplar del xml leído
        String ejemplarName = getEjemplarName(docXml);
        // imprimo nombre del ejemplar a modo de comprobación
        System.out.println("Etiqueta del ejemplar: " + ejemplarName);
        // ejemplo de como comprobar si el ejemplar del xml tiene atributos o no
        System.out.println("Ejemplar tiene atributos: " + 
                docXml.getDocumentElement().hasAttributes());
        // obtengo el NodeList o abstranción de la colección de los nodos que 
        // presentes en el ejemplar. Para ello primero obtengo el NodeList de los
        // elementos que tengan por etiqueta el nombre del ejemplar obtenido 
        // anteriormente, en especifíco el primer y único elemento de dicha 
        // colección (item(0)) y luego sus hijos (getChildNodes()).
        NodeList nodosEjemplar = docXml.getElementsByTagName(ejemplarName).item(0).getChildNodes();
        // obtendo los elementos del NodeList con los nodos del ejemplar
        getElementosDelNodo(nodosEjemplar);
        
    }

    private static void getElementosDelNodo(NodeList nodosEjemplar) {
        // a partir de un NodeList, que es una interfaz que representa abstractamente
        // una colección o lista de nodos. Al no ser una colección o lista al uso
        // solo se puede recorrer mediante un bucle for hasta llegar al tamaño de
        // dicha colección.
        for (int i=0; i<nodosEjemplar.getLength(); i++) {
            // obtengo el nodo en la posición i, mediante el metodo item(i)
            Node nodo = nodosEjemplar.item(i);
            // compruebo si el elemento no es terminal, es dicir, es un elemento
            // de tipo nodo que tendrá hijos. De ser así pues trabajo con el para
            // obtener lista de atributos y los elementos hijos de forma recursiva
            // teniendo en cuenta que un nodo puede albergar a otro nodo no terminal
            if (nodo.getNodeType() == Node.ELEMENT_NODE) {
                // hago castin al (Element) sobre el nodo en cuestión
                Element elemento = (Element) nodo;
                // imprimo nombre del elemento (etiqueta) a modo de comprobación
                System.out.println("Elemento con nombre: " + elemento.getNodeName());
                // compruebo si el elemento tiene atributos y de ser así, obtengo
                // la lista de esos atributos.
                if (elemento.hasAttributes()) {
                    // leeo los atributos que perteneces al elemento
                    listAttrValor(elemento);
                }
                // compruebo si este elemento que estamos leyendo tiene hijos y 
                // de ser así, hago la llamada recursiba a este método pásandolo
                // el NodeList de los hijos de elemento
                if (elemento.hasChildNodes()) {
                    getElementosDelNodo(elemento.getChildNodes());
                } 
            } else {
                // Este nodo es ternimal por lo que si su contenido no está vacío
                // o formado por espacion en blanco comprobado mediante el método
                // isBlank(), imprimo el texto contenido entre etiquetas.
                if (!nodo.getTextContent().isBlank()) {
                    System.out.println("\tTexto del elemento: " + 
                            nodo.getTextContent());
                }
            }
        }
    }

    private static void listAttrValor(Element elemento) {
        // obtener objeto de tipo NamedNodeMap que resulta ser una interface que
        // es una abstración de una colección de atributos pertenecientes a un 
        // elemento dado
        NamedNodeMap attrNodeMap = elemento.getAttributes();
        // NamedNodeMap no es un map corriente, sino una abstracción de una 
        // que tendremos que recorres con un bucle for tradicional hasta alcanzar
        // el tamaño de dicha colección
        for (int j=0; j<attrNodeMap.getLength(); j++) {
            // el objeto atributo se obtiene haciendo un casting a (Attr) sobre el
            // nodo obtenido mediante el método item(j) que devuelve el atributo
            // en la posición j de la colección NamedNodeMap
            Attr attrInfo = (Attr) attrNodeMap.item(j);
            // imprimo el nobre y el valor del atributo leido como comprobación
            System.out.println("\tAttributo: " + attrInfo.getNodeName() +
                    " Valor: " + attrInfo.getNodeValue());
        }
    }

    private static Document getDocXml(File fileFuenteXml) {
        // objeto de tipo Doucment a devolver y que representa el docuento
        //traducido y normalizado de un fichero .xml determinado
        Document docXml = null;
        // Creación de la factoría o fábrica de constructor de documentos
        DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();
        try {
            // constructor de docuemento desde el que se obtendrá un traducción
            // del fichero xml dado.
            DocumentBuilder docBuilder = docBuilderFactory.newDocumentBuilder();
            docXml = docBuilder.parse(fileFuenteXml); //obtención del documento DOM
            docXml.getDocumentElement().normalize(); // normalización del documento
            
        } catch (ParserConfigurationException parsconfEx) {
            System.out.println(parsconfEx.getMessage());
        } catch (SAXException saxEx) {
            System.out.println(saxEx.getMessage());
        } catch (IOException ioEx) {
            System.out.println(ioEx.getMessage());
        }
        return docXml; // retorno ducumento DOM leido y normalizado o null si error
    }

    private static String getEjemplarName(Document docXml) {
        // varieble donde almacenar el nombre o etiqueta del ejemplar del documento
        String ejemplarName = "";
        ejemplarName = docXml.getDocumentElement().getNodeName(); //obtención del nombre
        return ejemplarName;
    }
    
}
