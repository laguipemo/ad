/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lgpm.ad02_actividad_1_3_4;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import static java.lang.System.in;
import java.util.Scanner;

/**
 *
 * @author lgpmc
 */
public class AD02_Actividad_1_3_4 {

    private final static long LONGITUD_REGISTRO = 36L;
    private final static int LONGITUD_APELLIDO = 10;
    private static String strFichero = "empleados.dat";
    //Datos para crear fichero inicial
    private final static String[] APELLIDOS = {"FERNANDEZ", "GIL", "LOPEZ", "RAMOS", "SEVILLA", "CASILLA", "REY"}; //apellidos
    private final static int[] DEPARTAMENTOS = {10, 20, 10, 10, 30, 30, 20}; //departamentos
    private final static Double[] SALARIOS = {1000.45, 2400.60, 3000.0, 1500.56, 2200.0, 1435.87, 2000.0}; //salarios

    private static void crearAleatorio() {
        File fichero = new File(strFichero);
        try (RandomAccessFile ficheroRandom = new RandomAccessFile(fichero, "rw")) {
            //Adiciono datos al fichero
            StringBuffer buffer; //buffer para manejar apellido y ajustarlo al tamanho max
            int numEmpleados = APELLIDOS.length;
            for (int i = 0; i < numEmpleados; i++) {
                ficheroRandom.writeInt(i + 1); // escribo el indicador que es un entero (4 bytes)
                buffer = new StringBuffer(APELLIDOS[i]); // guardo el apellido como un buffer de caracteres
                buffer.setLength(LONGITUD_APELLIDO); // fijo tamaño del buffer, los APELLIDOS se guardaran en 10 caracteres (20 bytes)
                ficheroRandom.writeChars(buffer.toString()); // escribo en el fichero el apellido
                ficheroRandom.writeInt(DEPARTAMENTOS[i]); // escribo en el fichero el departamento (int 4 bytes)
                ficheroRandom.writeDouble(SALARIOS[i]); // escribo el salario en el fichero (Double 8 bytes)
            }
        } catch (FileNotFoundException fnfEx) {
            System.out.println(fnfEx.getMessage());
        } catch (IOException ioEx) {
            System.out.println(ioEx.getMessage());
        }
    }

    private static void leerAleatorio() {
        File fichero = new File(strFichero);
        long posicion = 0;
        int id, departamento;
        Double salario;
        char[] letrasApellido = new char[10];
        try (RandomAccessFile ficheroRandom = new RandomAccessFile(fichero, "r")) {
            while (posicion < ficheroRandom.length()) {
                ficheroRandom.seek(posicion);
                id = ficheroRandom.readInt();
                for (int i = 0; i < 10; i++) {
                    letrasApellido[i] = ficheroRandom.readChar();
                }
                String apellido = new String(letrasApellido);
                departamento = ficheroRandom.readInt();
                salario = ficheroRandom.readDouble();
                //mostrar datos formateados en pantalla
                System.out.printf("%3d %-10s %5d %10.2f %n", id, apellido, departamento, salario);
                //cambiar posicion al proximo registro
                posicion += LONGITUD_REGISTRO;
            }
        } catch (FileNotFoundException fnfEx) {
            System.out.println(fnfEx.getMessage());
        } catch (IOException ioEx) {
            System.out.println(ioEx.getMessage());
        }
    }

    private static void consultarAleatorio(String[] args) {
        int id;
        if (args.length < 1) {
            System.out.println("Se requiere un identificador del empleado a consultar");
            return;
        } else {
            id = Integer.parseInt(args[0]);
        }
        File fichero = new File(strFichero);
        long posicion = (id - 1) * LONGITUD_REGISTRO;
        if (posicion >= fichero.length() || posicion < 1) {
            System.out.println("No existe empleado con indicador " + id);
            return;
        }
        StringBuffer bufferApellido = new StringBuffer(10);
        try (RandomAccessFile ficheroRandom = new RandomAccessFile(fichero, "r")) {
            System.out.printf("%3s %-10s %5s %10s %n",
                    "ID", "Apellido", "Dpto", "Salario");
            ficheroRandom.seek(posicion);
            int idEmpleado = ficheroRandom.readInt();
            for (int i = 0; i < 10; i++) {
                bufferApellido.append(ficheroRandom.readChar());
            }
            String apellido = bufferApellido.toString().strip();
            int departamento = ficheroRandom.readInt();
            Double salario = ficheroRandom.readDouble();
            System.out.printf("%3d %-10s %5d %10.2f %n",
                    idEmpleado, apellido, departamento, salario);
        } catch (FileNotFoundException fnfEx) {
            System.out.println(fnfEx.getMessage());
        } catch (IOException ioEx) {
            System.out.println(ioEx.getMessage());
        }
    }

    public static void insertarEmpleado(String[] args) {
        final long longitudRegistro = 36L;
        int id;
        String apellido;
        int departamento;
        Double salario;
        if (args.length < 4) {
            System.out.println("Se requieren 4 parámetros: int id, String apellido, int departamento y double salario");
            return;
        } else {
            id = Integer.parseInt(args[0]);
            apellido = args[1];
            departamento = Integer.parseInt(args[2]);
            salario = Double.parseDouble(args[3]);
        }
        long insertPos = (id - 1) * longitudRegistro;
        String strFichero = "empleados.dat";
        File fichero = new File(strFichero);

        if (insertPos < 0) {
            System.out.println("Indicador no puede ser negativo");
            return;
        } else if (insertPos >= 0 && insertPos < fichero.length()) {
            System.out.println("El indicador de empleado ya existe:");
            consultarAleatorio(args);

            Scanner scan = new Scanner(in);
            System.out.print("Desea actualizar el existente [y/n]: ");
            char resp = scan.next().charAt(0);
            if (resp == 'n' || resp == 'N') {
                return;
            }

        } else if (insertPos > fichero.length()) {
            long ultimo = fichero.length() / longitudRegistro;
            System.out.println("Indicador demaciado grande. El ultimo indicador empleado es: " + ultimo);

            Scanner scan = new Scanner(in);
            System.out.print("Desea insertar el nuevo empleado con ID=" + (int) (ultimo + 1) + " [y/n]: ");
            char resp = scan.next().charAt(0);
            if (resp == 'n' || resp == 'N') {
                return;
            } else {
                id = (int) (ultimo + 1);
            }
        }

        try {
            RandomAccessFile ficheroRandom = new RandomAccessFile(fichero, "rw");
            long posicion = (id - 1) * longitudRegistro;
            ficheroRandom.seek(posicion);
            ficheroRandom.writeInt(id);
            StringBuffer bufferApellido = new StringBuffer(apellido);
            bufferApellido.setLength(10);
            ficheroRandom.writeChars(bufferApellido.toString());
            ficheroRandom.writeInt(departamento);
            ficheroRandom.writeDouble(salario);

        } catch (FileNotFoundException fnfEx) {
            fnfEx.printStackTrace();
        } catch (IOException ioEx) {
            ioEx.printStackTrace();
        }
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        //creo el fichero con los datos iniciales
//        crearAleatorio();

        //leo e imprimo en pantalla el fichero
        System.out.println("\nFichero completo\n");
        leerAleatorio();
        
        //consultar si existe un determinado empleado por su id
        System.out.println("\nConsulta\n");
        consultarAleatorio(args);

        //insertar empleado
        String[] datosEmpleado = {"8", "CARRION", "80", "3000.01"};
        insertarEmpleado(datosEmpleado);
        leerAleatorio();
    }

}
