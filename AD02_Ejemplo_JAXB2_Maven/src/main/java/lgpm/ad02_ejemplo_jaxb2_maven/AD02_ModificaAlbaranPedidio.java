/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lgpm.ad02_ejemplo_jaxb2_maven;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.math.BigDecimal;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeConstants;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import jaxb.myschema.Articulos;
import jaxb.myschema.Articulos.Articulo;
import jaxb.myschema.Direccion;
import jaxb.myschema.PedidoType;

/**
 *
 * @author lgpmc
 */
public class AD02_ModificaAlbaranPedidio {
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        /*
        Crear instancia de un contexto del binding JAXB (JAXBContex) que me 
        permite manipular trabajar con las clases generadas con ese binding 
        y que se encuentran el paquete jaxb.myschema.
        De modo que la clase JAXBContext nos proporciona el punto de entrada 
        a la API JAXB, facilitando una abstracción para manejar la información 
        generada para implementar las operaciones del JAXB binding framework 
        como unmarshal y marshal
        unmarshal: consiste en convertir datos XML en un árbol de objetos Java
        marshal: consiste en convertir un árbol de objetos Java a datos XML
        */
        try {
            /*
            Crear una instancia del contexto del binging. Para ello a la 
            nueva instacia se le pasa el paquete dondes están las clases
            generadas por el compilador a partir del schema dado.
            */
            JAXBContext contextoAlbaran = JAXBContext.newInstance("jaxb.myschema");
            /*
            Crear un objeto unmarshaller que permita leer y convertir los datos
            del fichero .xml para pasarlo a objetos java según su clase.
            */
            Unmarshaller unmarshAlbaran = contextoAlbaran.createUnmarshaller();
            /*
            Por medio del unmarshaller leemos la fuente por medio de un stream
            de lectura conectado al fichero, es decir, un FileInputStream que
            está conectado al fichero .xml. El objeto leeido por esta vía lo 
            convertimos en un objeto binding JAXBElement mediante un casting.
            */
            JAXBElement jaxbElementAlbaran = (JAXBElement) unmarshAlbaran.unmarshal(
                    new FileInputStream(new File("mi_albaran.xml")));
            /*
            Ahora el objeto de tipo ejemplar, en este caso PedidoType se obtiene
            del objeto binding JAXBElement mediante su método getValue() y 
            como siempre que un método devuelve un objeto genérico, hacemos el
            casting correspondiente.
            */
            PedidoType pedidoType = (PedidoType) jaxbElementAlbaran.getValue();
            /*
            Según la estructura definida por el schema para el xml a leer, 
            tenemos un elemento de tipo <direccion> con nombre facturarA
            */
            Direccion direccionFacturacion = pedidoType.getFacturarA();
            leerContenido(direccionFacturacion, pedidoType);
            
            //Cambiar los datos de facturación
            changeDireccionFacturacion(direccionFacturacion);
            
            //Cambia el comentario del pedido
            pedidoType.setComentario("Líneas pedidas por Daya");
            
            // Crear un nuevo artículo para añadirlo al pedido.
            Articulo articulo = createArticuloPedido(
                    "Losartan", "002-AA", 1, new BigDecimal("1.05"), 
                    "Para la tensión", 2020, 11, 17);
            
            // para añadir el artículo al pedio, tengo que recuperar a partir de
            // el objeto pedidoType, el objeto Artículos mediante su método 
            // getArticulos(). Luego acceder a su atributo que es una lista de los
            // articulos mediante el geter correspondient (getArticulo()) y
            // finalmente adicionar a esa lista el objeto articulo contruido.
            pedidoType.getArticulos().getArticulo().add(articulo);
            
            // Los cambio hechos hay que llevarlos a disco, es decir, al fichero
            // .xml mediante un proceso marshall. Para ello tengo que crear un
            // marshaller que me permita llevar el árbol de objetos Java a datos
            // en el fichero xml.
            Marshaller marshAlbaran = contextoAlbaran.createMarshaller();
            
            // El método setProperty(String nombrePropiedad, Object value) recibe 
            // en este caso la propiedad "jaxb.formatted.output". Esta propiedad 
            // controla si al realizar un marshal, debe formatear el resultado 
            // XML con saltos de linea e indentaciones para que las personas
            // podamos leerlo cómodamente. Por defecto su valor es falso es decir
            // el XML creado no está formateado. El argumento value en este caso
            // tiene que ser un objeto, concretamente de tipo Boolean por ello
            // utilizamos la constante TRUE de esta clase envoltoria y así indicar 
            // que si queremos que el resultado XML esté formateado.
            marshAlbaran.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
            
            // Finalmente hacemos el volcado. Para ello utilizamos el método 
            // marshall(Object elementoJAXB, OutputStream os) que recibe un objeto
            // de tipo JAXBElement para que su contenido lo muestre en la salida estándar
            // debido a que este método está sobrecargo, si miramos la documentación de
            //la API podemos ver como podemos mostrar o escribir el resultado XML de
            //diferentes maneras.
            // Escribir resultado por salida estándar
            marshAlbaran.marshal(jaxbElementAlbaran, System.out);
            // Escribir el resultado en un fichero
            marshAlbaran.marshal(jaxbElementAlbaran, 
                    new FileOutputStream(new File("mi_albaran2.xml"), false));
            
        } catch (JAXBException jaxbEx) {
            System.out.println(jaxbEx.getMessage());
        } catch (FileNotFoundException fnfEx) {
            System.out.println(fnfEx.getMessage());
        } catch (DatatypeConfigurationException datatconfEx) {
            System.out.println(datatconfEx.getMessage());
        }
        
    }

    private static void changeDireccionFacturacion(Direccion direccionFacturacion) {
        //Cambia los datos de la facturación (facturarA)
        direccionFacturacion.setNombre("Farmacia José Teijeiro Vega");
        direccionFacturacion.setCalle("Avenida de Cambados 60");
        direccionFacturacion.setCiudad("Vilagarcía de Arousa");
        direccionFacturacion.setProvincia("Pontevedra");
        direccionFacturacion.setCodigoPostal(new BigDecimal("36601"));
    }
    
    private static Articulo createArticuloPedido(
            String nombreProducto, String codProducto, int cantidad, BigDecimal precio, 
            String comentario, int year, int month, int day) throws DatatypeConfigurationException {
        // Crear un artículo y añadirlo a la lista de artículos del pedido
        Articulo articulo = new Articulo();
        articulo.setNombreProducto(nombreProducto);
        articulo.setCodigo(codProducto);
        articulo.setCantidad(cantidad);
        articulo.setPrecio(precio);
        articulo.setComentario(comentario);
        XMLGregorianCalendar fechaEnvio = DatatypeFactory.newInstance()
                .newXMLGregorianCalendarDate(year, month, day,  DatatypeConstants.FIELD_UNDEFINED);
        articulo.setFechaEnvio(fechaEnvio);
        return articulo;
    }

    private static void leerContenido(Direccion direccionFacturacion, PedidoType pedidoType) {
        /*
        Los objetos Dirección o tienen como atributos nombre,
        calle, ciudad, provincia, código postal..
        */
        String nombreFarmacia = direccionFacturacion.getNombre();
        String calle = direccionFacturacion.getCalle();
        String ciudad = direccionFacturacion.getCiudad();
        String provincia = direccionFacturacion.getProvincia();
        BigDecimal codPostal = direccionFacturacion.getCodigoPostal();
        //El pedido cuenta con un comentario tipo string
        String comentario = pedidoType.getComentario();
        
        System.out.printf("Farmacia: %s%n Calle: %s%n Ciudad: %s %n Provincia: %s %n Código Postal: %s %n",
                nombreFarmacia, calle, ciudad, provincia, codPostal.toString());
        System.out.printf("%s%n%s%n%s%n", "=".repeat(40), comentario, "=".repeat(40));
        
        //El pedido tiene un elemento de tipo <articulos> qeu es una secuencia
        //de elementos <articulo>. Para ello el compilador ha creado la clase
        //Articulos que tiene un atributo que es una lista de objetos Articulo
        //cada uno de ellos con sus atributos...
        //uno de los artículos. E
        Articulos articulos = pedidoType.getArticulos();
        //Recorro la lista de articulos según
        for (Articulo articulo: articulos.getArticulo()) {
            String nombreProducto = articulo.getNombreProducto();
            String codProducto = articulo.getCodigo();
            int cantidad = articulo.getCantidad();
            BigDecimal precio = articulo.getPrecio();
            String artComentario = articulo.getComentario();
            XMLGregorianCalendar fechaEnvio = articulo.getFechaEnvio();
            
            System.out.printf("Producto: %s %n Código: %s %n Cantidad: %d %n Precio: %.2f %n Comentario: %s %n Fecha Envio: %s %n",
                    nombreProducto, codProducto, cantidad, precio, artComentario, fechaEnvio.toString());
        }
    }
    
}
