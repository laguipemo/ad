/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lgpm.ad03_jdbc_poolconnection.test;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import lgpm.ad03_jdbc_poolconnection.data.Conexion;
import lgpm.ad03_jdbc_poolconnection.data.Consultora;
import lgpm.ad03_jdbc_poolconnection.data.ProductosJdbc;
import lgpm.ad03_jdbc_poolconnection.domain.Producto;

/**
 *
 * @author lgpmc
 */
public class AD03_Jbc_PoolConnection {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Connection connectionTransational = null;
        List<Producto> productosSelected = new ArrayList<>();
        
        try {
            connectionTransational = Conexion.getConnection();
            if (connectionTransational.getAutoCommit()) {
                connectionTransational.setAutoCommit(false);
            }
            
            Consultora consultoraProductos = new ProductosJdbc(connectionTransational);
            
            // consulta select
            productosSelected = consultoraProductos.select();
            productosSelected.forEach(producto -> System.out.println(producto));
            
            // consulta insert
            //consulta que falla para probar si enta en rollback.
//            Producto prodAInsertar = new Producto("Prueba11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111", 
//                    "Prueba de Inserción", 0.0, "prueba_imagen.jpg", 1);
            //consulta que no falla
//            Producto prodAInsertar = new Producto("Prueba", "Prueba de Inserción", 0.0, "prueba_imagen.jpg", 1);
//            int numFilasInserted = consultoraProductos.insert(prodAInsertar);
//            System.out.println("Se han insertdo " + numFilasInserted + " filas");
            
            // consulta update
//            Producto prodActulizado = new Producto(123, "PruebaActualizada", "Prueba de Actualización", 0.00, "prueba_imagen.jpg", 1);
//            int numFilasUpdated = consultoraProductos.update(prodActulizado);
//            System.out.println("Se han actualizado: " + numFilasUpdated + " filas");
            
            // consulta delete
//            Producto prodAEliminar = new Producto();
//            prodAEliminar.setIdProducto(123);
//            int numFilasDeleted = consultoraProductos.delete(prodAEliminar);
//            System.out.println("Se han borrado: " + numFilasDeleted + " filas");
            
            connectionTransational.commit();
            
        } catch (SQLException sqlEx) {
            System.out.println("*** ERROR *** => " + sqlEx.getMessage());
            System.out.println("Entrando en rollback...");
            try {
                connectionTransational.rollback();
            } catch (SQLException sqlEx1) {
                System.out.println("*** ERROR *** No se pudo hacer rollback => " + sqlEx.getMessage());
            }
        } finally {
            if (connectionTransational != null) {
                Conexion.close(connectionTransational);
            }
        }
        
    }
    
}
