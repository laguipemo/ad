/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lgpm.ad03_jdbc_poolconnection.data;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import lgpm.ad03_jdbc_poolconnection.domain.Producto;

/**
 *
 * @author lgpmc
 */
public class ProductosJdbc implements Consultora {
    
    private Connection connectionTransactional;
    
    private final String SQL_SELECT = "SELECT id_producto, nombre, descripcion, precio, imagen, id_categoria FROM productos LIMIT 5";
    private final String SQL_INSERT = "INSERT INTO productos (nombre, descripcion, precio, imagen, id_categoria) VALUES (?, ?, ?, ?, ?)";
    private final String SQL_UPDATE = "UPDATE productos SET nombre=?, descripcion=?, precio=?, imagen=?, id_categoria=? WHERE id_producto=?";
    private final String SQL_DELETE = "DELETE FROM productos WHERE id_producto=?";

    public ProductosJdbc() {
    }

    public ProductosJdbc(Connection connectionTransactional) {
        this.connectionTransactional = connectionTransactional;
    }
    
    
    @Override
    public List<Producto> select() throws SQLException {
        Connection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;
        List<Producto> productosSelected = new ArrayList<>();
        
        try {
            connection = this.connectionTransactional != null ? 
                    this.connectionTransactional : Conexion.getConnection();
            statement = connection.createStatement();
            resultSet = statement.executeQuery(SQL_SELECT);
            
            while (resultSet.next()) {
                int idProducto = resultSet.getInt("id_producto");
                String nombre = resultSet.getString("nombre");
                String descripcion = resultSet.getString("descripcion");
                double precio = resultSet.getDouble("precio");
                String strImagen = resultSet.getString("imagen");
                int idCategoria = resultSet.getInt("id_categoria");
                
                Producto producto = new Producto(idProducto, nombre, descripcion, precio, strImagen, idCategoria);
                
                productosSelected.add(producto);
            }
            
        } finally {
            Conexion.close(resultSet);
            Conexion.close(statement);
            if (this.connectionTransactional == null) {
                Conexion.close(connection);
            }
        }
        return productosSelected;
    }

    @Override
    public int insert(Producto producto) throws SQLException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        int numFilasInserted = 0;
        
        try {
            connection = this.connectionTransactional != null ? this.connectionTransactional : Conexion.getConnection();
            
            preparedStatement = connection.prepareStatement(SQL_INSERT);
            preparedStatement.setString(1, producto.getNombre());
            preparedStatement.setString(2, producto.getDescripcion());
            preparedStatement.setDouble(3, producto.getPrecio());
            preparedStatement.setString(4, producto.getStrImagen());
            preparedStatement.setInt(5, producto.getIdCategoria());
            
            numFilasInserted = preparedStatement.executeUpdate();
            
        } finally {
            Conexion.close(preparedStatement);
            if (this.connectionTransactional == null) {
                Conexion.close(connection);
            }
        }
        return numFilasInserted;
    }

    @Override
    public int update(Producto producto) throws SQLException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        int numFilasUpdated = 0;
        
        try {
           connection = this.connectionTransactional != null ? this.connectionTransactional : Conexion.getConnection();
           
           preparedStatement = connection.prepareStatement(SQL_UPDATE);
           preparedStatement.setString(1, producto.getNombre());
           preparedStatement.setString(2, producto.getDescripcion());
           preparedStatement.setDouble(3, producto.getPrecio());
           preparedStatement.setString(4, producto.getStrImagen());
           preparedStatement.setInt(5, producto.getIdCategoria());
           preparedStatement.setInt(6, producto.getIdProducto());
           
           numFilasUpdated = preparedStatement.executeUpdate();
           
        } finally {
            Conexion.close(preparedStatement);
            if (this.connectionTransactional == null) {
                Conexion.close(connection);
            }
        }
        return numFilasUpdated;
    }

    @Override
    public int delete(Producto producto) throws SQLException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        int numFilasDeleted = 0;
        
        try {
            connection = this.connectionTransactional != null ? this.connectionTransactional : Conexion.getConnection();
            
            preparedStatement = connection.prepareStatement(SQL_DELETE);
            preparedStatement.setInt(1, producto.getIdProducto());
            
            numFilasDeleted = preparedStatement.executeUpdate();
            
        } finally {
            Conexion.close(preparedStatement);
            if (this.connectionTransactional == null) {
                Conexion.close(connection);
            }
        }
        return numFilasDeleted;
    }
    
}
