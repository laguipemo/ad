/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lgpm.ad03_jdbc_poolconnection.data;

import java.sql.SQLException;
import java.util.List;
import lgpm.ad03_jdbc_poolconnection.domain.Producto;

/**
 *
 * @author lgpmc
 */
public interface Consultora {
    
    public List<Producto> select() throws SQLException;
    
    public int insert(Producto producto) throws SQLException;
    
    public int update(Producto producto) throws SQLException;
    
    public int delete(Producto producto) throws SQLException;
    
}
