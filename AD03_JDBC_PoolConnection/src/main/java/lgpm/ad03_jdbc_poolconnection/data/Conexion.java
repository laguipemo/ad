/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lgpm.ad03_jdbc_poolconnection.data;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.sql.DataSource;
import org.apache.commons.dbcp2.BasicDataSource;

/**
 *
 * @author lgpmc
 */
public class Conexion {
    
    private static final String JDBC_URL = "jdbc:mysql://localhost:3306/restaurante?useSSL=false&serverTimezone=UTC";
    private static final String JDBC_USE = "root";
    private static final String JDBC_PASS = "";
    
    public static DataSource getDataSource() {
        BasicDataSource dataSource = new BasicDataSource();
        dataSource.setUrl(JDBC_URL);
        dataSource.setUsername(JDBC_USE);
        dataSource.setPassword(JDBC_PASS);
        dataSource.setInitialSize(5);
        
        return dataSource;
    }
    
    public static Connection getConnection( ) throws SQLException {
        return getDataSource().getConnection();
    }
    
    public static void close(Connection connection) {
        try {
            connection.close();
        } catch (SQLException sqlEx) {
            System.out.println("*** ERROR No se pudo cerrar la conexión *** => " + sqlEx.getMessage());
        }
    }
    
    public static void close(Statement statement) {
        try {
            statement.close();
        } catch (SQLException sqlEx) {
            System.out.println("*** ERROR No se pudo cerrar el Statement *** => " + sqlEx.getMessage());
        }
    }
    public static void close(ResultSet resultSet) {
        try {
            resultSet.close();
        } catch (SQLException sqlEx) {
            System.out.println("*** ERROR No se pudo cerrar el ResultSet *** => " + sqlEx.getMessage());
        }
    }
            
}
