La Interfaz DatabaseMetaData (de la API java.sql) permite obtener Información  sobre la base de datos en su conjunto.

Algunos métodos de la interfaz DatabaseMetaData devuelven listas de información en forma de objetos  ResultSet, que se 
pueden recorrer en un ciclo para obtener los metadatos. Para ello utilizaremos metodos  getString,  getInt, etc. Consulta 
la documentación de la API JDBC.

Algunos métodos DatabaseMetaData tienen  argumentos que son patrones de cadena. Es posible usar patrones dentro de 
estas cadenas,  "%" representa  cualquier subcadena de 0 o más caracteres, y "_" representa  un  carácter.

Algunos métodos útiles son:

	- getColumns(String catalog, String schemaPattern, String tableNamePattern, String columnNamePattern)

		Devuelve en un ResultSet  la descripción de las columnas de la tabla disponibles en el catálogo especificado. 

	- getDriverName()
	
		Devuelve en un String, el nombre del controlador JDBC.

	- getDriverVersion()

		Devuelve en un String, el número de versión de este controlador JDBC como a String.

	- getExportedKeys(String catalog, String schema, String table)

		Devuelve en un ResultSet  la  descripción de las columnas de clave Foranea que hacen referencia a las columnas 
		de clave principal de la tabla dada.

	- getFunctions(String catalog, String schemaPattern, String functionNamePattern)

		Devuelve en un ResultSet la descripción del sistema y las funciones de usuario disponibles en el catálogo dado.

	- getMaxConnections()
		
		Recupera el número máximo de conexiones simultáneas a esta base de datos que son posibles.

	- getPrimaryKeys(String catalog, String schema, String table)

		Devuelve en un ResultSet  una descripción de las columnas de clave primaria de la tabla dada.

	- getProcedures(String catalog, String schemaPattern, String procedureNamePattern)

		Recupera una descripción de los procedimietos almacenados disponibles en el catálogo dado.

	- getSchemas()

		Devuelve en un ResultSet los nombres de esquema disponibles en esta base de datos.

	- getSQLKeywords()

		Recupera una lista separada por comas de todas las palabras clave de SQL de esta base de datos que NO son también palabras clave de SQL: 2003.

	- getStringFunctions()

		Recupera una lista separada por comas de funciones de cadena disponibles con esta base de datos.

	- supportsAlterTableWithAddColumn()

		Recupera si esta base de datos admite ALTER TABLE con añadir columna. Devuelve un booleano

	- supportsAlterTableWithDropColumn()

		Recupera si esta base de datos es posible borrar una columna con  ALTER TABLE.  Devuelve un booleano.

	- supportsTransactions()

		Recupera si esta base de datos admite transacciones.

Ejemplo de la unidad:

	public class EjemploMetadata {
		public static void main(String[] args) {
			try {
				Connection conexion = DriverManager.getConnection("jdbc:mysql://localhost/ejemplo", "ejemplo", "ejemplo");
				java.sql.DatabaseMetaData dbmd = conexion.getMetaData();// Creamos objeto DatabaseMetaData
				ResultSet resul = null;
				String nombre = dbmd.getDatabaseProductName();
				String driver = dbmd.getDriverName();
				String url = dbmd.getURL();
				String usuario = dbmd.getUserName();
				System.out.println("INFORMACIÓN SOBRE LA BASE DE DATOS:");			
				System.out.printf("Nombre : %s %n", nombre);
				System.out.printf("Driver : %s %n", driver);
				System.out.printf("URL    : %s %n", url);
				System.out.printf("Usuario: %s %n", usuario);
				// Obtener información de las tablas y vistas que hay
				resul = dbmd.getTables(null, "ejemplo", null, null);
				while (resul.next()) {
					String catalogo = resul.getString(1);// columna 1
					String esquema = resul.getString(2); // columna 2
					String tabla = resul.getString(3); // columna 3
					String tipo = resul.getString(4); // columna 4
					System.out.printf("%s - Catalogo: %s, Esquema: %s, Nombre: %s %n", tipo, catalogo, esquema, tabla);
				}
				conexion.close(); // Cerrar conexion
			} catch (SQLException e) {
				System.out.println(e.getMessage());
				System.out.println(e.getErrorCode());
				System.out.println(e.getSQLState());	
			}
		}
	}
