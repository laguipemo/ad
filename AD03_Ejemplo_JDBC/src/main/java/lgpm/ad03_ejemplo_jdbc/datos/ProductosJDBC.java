/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lgpm.ad03_ejemplo_jdbc.datos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import lgpm.ad03_ejemplo_jdbc.dominio.Producto;

/**
 *
 * @author lgpmc
 */
public class ProductosJDBC {
    
    private static final String SQL_SELECT = "SELECT id_producto, nombre, descripcion, precio, imagen, id_categoria FROM productos LIMIT 5";
    private static final String SQL_INSERT = "INSERT INTO productos (nombre, descripcion, precio, imagen, id_categoria) VALUES(?,?,?,?,?)";
    private static final String SQL_UPDATE = "UPDATE productos SET nombre=?, descripcion=?, precio=?, imagen=?, id_categoria=? WHERE id_producto=?";
    private static final String SQL_DELETE = "DELETE FROM productos WHERE id_producto=?";
    
    private Connection connectionTransaccional;
    
    public ProductosJDBC() {
        
    }
    
    public ProductosJDBC(Connection connTransaccional) {
        this.connectionTransaccional = connTransaccional;
    }
    
    
    public List<Producto> select() throws SQLException {
        Connection connection = null;
        PreparedStatement sentencia = null;
        ResultSet resultado = null;
        Producto producto = null;
        List<Producto> listaProductos = new ArrayList<>();
            
        try {
            connection = this.connectionTransaccional != null ? this.connectionTransaccional : MiConexion.getConexion();
            sentencia = connection.prepareStatement(SQL_SELECT);
            resultado = sentencia.executeQuery();
            
            while (resultado.next()) {
                int idProducto = resultado.getInt("id_producto");
                String nombre = resultado.getString("nombre");
                String descripcion = resultado.getString("descripcion");
                double precio = resultado.getDouble("precio");
                String strImagen = resultado.getString("imagen");
                int id_categoria = resultado.getInt("id_categoria");
                
                producto = new Producto(idProducto, nombre, descripcion, precio, strImagen, id_categoria);
                
                listaProductos.add(producto);
            }
            
        } finally {
            MiConexion.close(resultado);
            MiConexion.close(sentencia);
            if (this.connectionTransaccional == null) {
                MiConexion.close(connection);
            }
        }
        
        return listaProductos;
    }
    
    public int insert(Producto producto) throws SQLException {
        Connection connection = null;
        PreparedStatement prepSentencia = null;
        int numFilasInserted = 0;
        
        try {
            connection = this.connectionTransaccional != null ? this.connectionTransaccional : MiConexion.getConexion();
            prepSentencia = connection.prepareStatement(SQL_INSERT);
            prepSentencia.setString(1, producto.getNombre());
            prepSentencia.setString(2, producto.getDescripcion());
            prepSentencia.setDouble(3, producto.getPrecio());
            prepSentencia.setString(4, producto.getStrImagen());
            prepSentencia.setInt(5, producto.getCategoria());
            
            System.out.println("Ejecutando query: " + SQL_INSERT);
            numFilasInserted = prepSentencia.executeUpdate();
            System.out.println("Filas insertadas: " + numFilasInserted);
            
        } finally {
            MiConexion.close(prepSentencia);
            if (this.connectionTransaccional == null) {
                MiConexion.close(connection);
            }
        }
        return numFilasInserted;
    }
    
    public int update(Producto producto) throws SQLException {
        Connection connection = null;
        PreparedStatement prepSentencia = null;
        int numFilasUpdated = 0;
        
        try {
            connection = this.connectionTransaccional != null ? this.connectionTransaccional : MiConexion.getConexion();
            prepSentencia = connection.prepareStatement(SQL_UPDATE);
            prepSentencia.setInt(6, producto.getIdProducto());
            prepSentencia.setString(1, producto.getNombre());
            prepSentencia.setString(2, producto.getDescripcion());
            prepSentencia.setDouble(3, producto.getPrecio());
            prepSentencia.setString(4, producto.getStrImagen());
            prepSentencia.setInt(5, producto.getCategoria());
            
            System.out.println("Ejecutando query: " + SQL_UPDATE);
            numFilasUpdated = prepSentencia.executeUpdate();
            System.out.println("Filas actualizadas: " + numFilasUpdated);
            
        } finally {
            MiConexion.close(prepSentencia);
            if (this.connectionTransaccional == null) {
                MiConexion.close(connection);
            }
        }
        return numFilasUpdated;
    }
    
    public int delete(Producto producto) throws SQLException {
        Connection connection = null;
        PreparedStatement prepSentencia = null;
        int numFilasDeleted = 0;
        
        try {
            connection = this.connectionTransaccional != null ? this.connectionTransaccional : MiConexion.getConexion();
            prepSentencia = connection.prepareStatement(SQL_DELETE);
            prepSentencia.setInt(1, producto.getIdProducto());
            
            System.out.println("Ejecutando query: " + SQL_DELETE);
            numFilasDeleted = prepSentencia.executeUpdate();
            System.out.println("Eliminadas: " + numFilasDeleted + " filas.");
            
        } finally {
            MiConexion.close(prepSentencia);
            if (this.connectionTransaccional == null) {
                MiConexion.close(connection);
            }
        }
        return numFilasDeleted;
    }
    
}
