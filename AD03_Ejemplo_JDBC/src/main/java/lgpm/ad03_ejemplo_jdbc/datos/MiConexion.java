/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lgpm.ad03_ejemplo_jdbc.datos;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author lgpmc
 */
public class MiConexion {
    private static final String JDBC_URL = "jdbc:mysql://localhost:3306/restaurante?useSSL=false&localTimezone=UTC";
    private static final String JDBC_USER = "root";
    private static final String JDBC_PASS = "";
    
    public static Connection getConexion() throws SQLException {
        return DriverManager.getConnection(JDBC_URL, JDBC_USER, JDBC_PASS);
    }
    
    public static void close(Connection conn) {
        try {
            conn.close();
        } catch (SQLException sqlEx) {
            System.out.println(sqlEx.getMessage());
        }
    }
    
    public static void close(Statement statement) {
        try {
            statement.close();
        } catch (SQLException sqlEx) {
            System.out.println(sqlEx.getMessage());
        }
    }
    
    public static void close(ResultSet result) {
        try {
            result.close();
        } catch (SQLException sqlEx) {
            System.out.println(sqlEx.getMessage());
        }
    }
    
}
