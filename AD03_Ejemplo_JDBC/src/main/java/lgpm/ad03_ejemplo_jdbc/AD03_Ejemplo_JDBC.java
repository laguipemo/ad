/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lgpm.ad03_ejemplo_jdbc;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import lgpm.ad03_ejemplo_jdbc.datos.MiConexion;
import lgpm.ad03_ejemplo_jdbc.datos.ProductosJDBC;
import lgpm.ad03_ejemplo_jdbc.dominio.Producto;

/**
 *
 * @author lgpmc
 */
public class AD03_Ejemplo_JDBC {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        //ejemplo_sin_transaccion();
        ejemplo_con_transaccion();
        
    }

    private static void ejemplo_sin_transaccion() {
        // TODO code application logic here
        ProductosJDBC productosJDBC = new ProductosJDBC();
        
        try {
            List<Producto> listaProdutos = productosJDBC.select();
            listaProdutos.forEach(p -> System.out.println(p));
            
            
//            Producto prodAInsertar = new Producto("Prueba", "Prueba de Inserción", 0.0, "prueba_imagen.jpg", 1);
//            int filasInserted = productosJDBC.insert(prodAInsertar);
//            System.out.println("Se han insertado: " + filasInserted + " filas");

//            Producto prodActulizado = new Producto(117, "PruebaActualizada", "Prueba de Actualización", 0.00, "prueba_imagen.jpg", 1);
//            int filasUpdated = productosJDBC.update(prodActulizado);
//            System.out.println("Se han actualizado: " + filasUpdated + " filas");
        
//            Producto prodAEliminar = new Producto();
//            prodAEliminar.setIdProducto(117);
//            int filasDeleted = productosJDBC.delete(prodAEliminar);
//            System.out.println("Se han borrado: " + filasDeleted + " filas");
            
        } catch (SQLException sqlEx) {
            System.out.println(sqlEx.getMessage());
        }
        
    }
    
    private static void ejemplo_con_transaccion() {
        Connection connectionTransaccional = null;
        try {
            connectionTransaccional =  MiConexion.getConexion();
            if (connectionTransaccional.getAutoCommit()) {
                connectionTransaccional.setAutoCommit(false);
            }
            
            ProductosJDBC productosJDBC = new ProductosJDBC(connectionTransaccional);
            //consulta que falla para probar si enta en rollback.
//            Producto prodAInsertar = new Producto("Prueba11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111", 
//                    "Prueba de Inserción", 0.0, "prueba_imagen.jpg", 1);
            //consulta que no falla
            Producto prodAInsertar = new Producto("Prueba", "Prueba de Inserción", 0.0, "prueba_imagen.jpg", 1);
            int filasInserted = productosJDBC.insert(prodAInsertar);
            //System.out.println("Se han insertado: " + filasInserted + " filas");

            Producto prodActulizado = new Producto(118, "PruebaActualizada", "Prueba de Actualización", 0.00, "prueba_imagen.jpg", 1);
            int filasUpdated = productosJDBC.update(prodActulizado);
            //System.out.println("Se han actualizado: " + filasUpdated + " filas");
        
            Producto prodAEliminar = new Producto();
            prodAEliminar.setIdProducto(118);
            int filasDeleted = productosJDBC.delete(prodAEliminar);
            //System.out.println("Se han borrado: " + filasDeleted + " filas");
            
            connectionTransaccional.commit();
            System.out.println("Finalizado commit...");
            
        } catch (SQLException sqlEx) {
            System.out.println(sqlEx.getMessage());
            System.out.println("Inciando Rollback...");
            try {
                connectionTransaccional.rollback();
            } catch (SQLException sqlEx1) {
                System.out.println(sqlEx1.getMessage());
            } 
        } finally {
            if (connectionTransaccional != null) {
                MiConexion.close(connectionTransaccional);
                System.out.println("Conexión transacional cerrada");
            }
        
        }
        
    }
    
}
