/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lgpm.ad03_jdbc_patternsdesign;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import lgpm.ad03_jdbc_patternsdesign.data.ConnectionDA;
import lgpm.ad03_jdbc_patternsdesign.data.Consultora;
import lgpm.ad03_jdbc_patternsdesign.data.ProductosDAO_JDBC;
import lgpm.ad03_jdbc_patternsdesign.domain.ProductoDTO;

/**
 *
 * @author lgpmc
 */
public class AD03_JDBC_PatternsDesign {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        Connection connectionTransactional = null;
        try {
            connectionTransactional = ConnectionDA.getConnection();
            if (connectionTransactional.getAutoCommit()) {
                connectionTransactional.setAutoCommit(false);
            }
            Consultora consultora = new ProductosDAO_JDBC(connectionTransactional);
            
            
            List<ProductoDTO> productsSelected = consultora.select();
            productsSelected.forEach(producto -> System.out.println(producto));
            
            //consulta que falla para probar si enta en rollback.
//            ProductoDTO prodAInsertar = new ProductoDTO("Prueba11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111", 
//                    "Prueba de Inserción", 0.0, "prueba_imagen.jpg", 1);
            //consulta que no falla
            ProductoDTO prodAInsertar = new ProductoDTO("Prueba", "Prueba de Inserción", 0.0, "prueba_imagen.jpg", 1);
            int numRowInserted = consultora.insert(prodAInsertar);
            System.out.println("Se han insertado: " + numRowInserted + " filas");

            ProductoDTO prodActulizado = new ProductoDTO(120, "PruebaActualizada", "Prueba de Actualización", 0.00, "prueba_imagen.jpg", 1);
            int numRowsUpdated = consultora.update(prodActulizado);
            System.out.println("Se han actualizado: " + numRowsUpdated + " filas");
        
            ProductoDTO prodAEliminar = new ProductoDTO();
            prodAEliminar.setIdProducto(120);
            int numRowsDeleted = consultora.delete(prodAEliminar);
            System.out.println("Se han borrado: " + numRowsDeleted + " filas");
            
            connectionTransactional.commit();
            System.out.println("Commit Finalizado");
            
        } catch (SQLException sqlEx) {
            System.out.println("*** ERROR *** => " + sqlEx.getMessage());
            System.out.println("Entrando en collback...");
            try {
                connectionTransactional.rollback();
            } catch (SQLException sqlEx1) {
                System.out.println("*** ERROR *** No se pudo realizar el rollback => " + sqlEx1.getMessage());
            }
        }
        
        
    }
    
}
