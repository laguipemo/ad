/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lgpm.ad03_jdbc_patternsdesign.data;

import java.sql.SQLException;
import java.util.List;
import lgpm.ad03_jdbc_patternsdesign.domain.ProductoDTO;

/**
 *
 * @author lgpmc
 */
public interface Consultora {
    
    public List<ProductoDTO> select() throws SQLException;
    
    public int insert(ProductoDTO producto) throws SQLException;
    
    public int update(ProductoDTO producto) throws SQLException;
    
    public int delete(ProductoDTO producto) throws SQLException;
    
}
