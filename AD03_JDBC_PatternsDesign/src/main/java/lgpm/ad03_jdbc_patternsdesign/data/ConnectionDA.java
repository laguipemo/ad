/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lgpm.ad03_jdbc_patternsdesign.data;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author lgpmc
 */
public class ConnectionDA {
    
    private static final String JDBC_URL = "jdbc:mysql://localhost:3306/restaurante?useSSL=false&serverTimezone=UTC";
    private static final String JDBC_USER = "root";
    private static final String JDBC_PASS = "";
    
    public static Connection getConnection() throws SQLException {
        return DriverManager.getConnection(JDBC_URL, JDBC_USER, JDBC_PASS);
    }
    
    public static void close(Connection connection) {
        try {
            connection.close();
        } catch (SQLException sqlEx) {
            System.out.println("*** ERROR *** => " + sqlEx.getMessage());
        }
    }
    
    public static void close(Statement statement) {
        try {
            statement.close();
        } catch (SQLException sqlEx) {
            System.out.println("*** ERROR *** => " + sqlEx.getMessage());
        }
    }
    
    public static void close(ResultSet resultSet) {
        try {
            resultSet.close();
        } catch (SQLException sqlEx) {
            System.out.println("*** ERROR *** => " + sqlEx.getMessage());
        }
    }
    
}
