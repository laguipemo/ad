/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lgpm.ad03_jdbc_patternsdesign.data;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import lgpm.ad03_jdbc_patternsdesign.domain.ProductoDTO;

/**
 *
 * @author lgpmc
 */
public class ProductosDAO_JDBC implements Consultora{
    
    private Connection connectionTransactional;
    
    private static final String SQL_SELECT = "SELECT id_producto, nombre, descripcion, precio, imagen, id_categoria FROM productos LIMIT 5";
    private static final String SQL_INSERT = "INSERT INTO productos (nombre, descripcion, precio, imagen, id_categoria) VALUES (?, ?, ?, ?, ?)";
    private static final String SQL_UPDATE = "UPDATE productos SET nombre=?, descripcion=?, precio=?, imagen=?, id_categoria=? WHERE id_producto=?";
    private static final String SQL_DELETE = "DELETE FROM productos WHERE id_producto=?";

    public ProductosDAO_JDBC() {
    }

    public ProductosDAO_JDBC(Connection connectionTransactional) {
        this.connectionTransactional = connectionTransactional;
    }
    
    

    @Override
    public List<ProductoDTO> select() throws SQLException {
        Connection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;
        
        List<ProductoDTO> productsSelected = new ArrayList<>();
        
        try {
            connection = this.connectionTransactional != null ? this.connectionTransactional : ConnectionDA.getConnection();
            statement = connection.createStatement();
            resultSet = statement.executeQuery(SQL_SELECT);

            while (resultSet.next()) {
                int idProducto = resultSet.getInt("id_producto");
                String nombre = resultSet.getString("nombre");
                String descripcion = resultSet.getString("descripcion");
                double precio = resultSet.getDouble("precio");
                String strImagen = resultSet.getString("imagen");
                int idCategoria = resultSet.getInt("id_categoria");

                ProductoDTO producto = new ProductoDTO(idProducto, nombre, descripcion, precio, strImagen, idCategoria);

                productsSelected.add(producto);
            }
        } finally {
            ConnectionDA.close(resultSet);
            ConnectionDA.close(statement);
            if (this.connectionTransactional == null) {
                ConnectionDA.close(connection);
            }
        }
        
        return productsSelected;
    }

    @Override
    public int insert(ProductoDTO producto) throws SQLException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        int numRowsInserted = 0;
        
        try {
            connection = this.connectionTransactional != null ? this.connectionTransactional : ConnectionDA.getConnection();

            preparedStatement = connection.prepareStatement(SQL_INSERT);
            preparedStatement.setString(1, producto.getNombre());
            preparedStatement.setString(2, producto.getDescripcion());
            preparedStatement.setDouble(3, producto.getPrecio());
            preparedStatement.setString(4, producto.getStrImagen());
            preparedStatement.setInt(5, producto.getIdCategoria());

            numRowsInserted = preparedStatement.executeUpdate();
            
        } finally {
            ConnectionDA.close(preparedStatement);
            if (this.connectionTransactional == null) {
                ConnectionDA.close(connection);
            }
        }
        return numRowsInserted;
    }

    @Override
    public int update(ProductoDTO producto) throws SQLException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        int numRowsUpdated = 0;
        
        try {
            connection = this.connectionTransactional != null ? this.connectionTransactional : ConnectionDA.getConnection();
            
            preparedStatement = connection.prepareStatement(SQL_UPDATE);
            preparedStatement.setString(1, producto.getNombre());
            preparedStatement.setString(2, producto.getDescripcion());
            preparedStatement.setDouble(3, producto.getPrecio());
            preparedStatement.setString(4, producto.getStrImagen());
            preparedStatement.setInt(5, producto.getIdCategoria());
            preparedStatement.setInt(6, producto.getIdProducto());
            numRowsUpdated = preparedStatement.executeUpdate();
            
        } finally {
            ConnectionDA.close(preparedStatement);
            if (this.connectionTransactional == null) {
                ConnectionDA.close(connection);
            }
        }
        return numRowsUpdated;
    }

    @Override
    public int delete(ProductoDTO producto) throws SQLException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        int numRowsDeleted = 0;
        
        try {
            connection = this.connectionTransactional != null ? this.connectionTransactional : ConnectionDA.getConnection();
           
            preparedStatement = connection.prepareStatement(SQL_DELETE);
            preparedStatement.setInt(1, producto.getIdProducto());
           
            numRowsDeleted = preparedStatement.executeUpdate();
           
        } finally {
            ConnectionDA.close(preparedStatement);
            if (this.connectionTransactional == null) {
                ConnectionDA.close(connection);
            }
        }
        return numRowsDeleted;
    }
    
}
