/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lgpm.ad03_jdbc_patternsdesign.domain;

/**
 *
 * @author lgpmc
 */
public class ProductoDTO {
    private int idProducto;
    private String nombre;
    private String descripcion;
    private double precio;
    private String strImagen;
    private int idCategoria;

    public ProductoDTO() {
    }

    public ProductoDTO(String nombre, String descripcion, double precio, String strImagen, int idCategoria) {
        this.nombre = nombre;
        this.descripcion = descripcion;
        this.precio = precio;
        this.strImagen = strImagen;
        this.idCategoria = idCategoria;
    }

    public ProductoDTO(int idProducto, String nombre, String descripcion, double precio, String strImagen, int idCategoria) {
        this(nombre, descripcion, precio, strImagen, idCategoria);
        this.idProducto = idProducto;
    }

    public int getIdProducto() {
        return idProducto;
    }

    public void setIdProducto(int idProducto) {
        this.idProducto = idProducto;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }

    public String getStrImagen() {
        return strImagen;
    }

    public void setStrImagen(String strImagen) {
        this.strImagen = strImagen;
    }

    public int getIdCategoria() {
        return idCategoria;
    }

    public void setIdCategoria(int idCategoria) {
        this.idCategoria = idCategoria;
    }

    @Override
    public String toString() {
        return "ProductoDTO{" + "idProducto=" + idProducto + ", nombre=" + nombre + ", descripcion=" + descripcion + ", precio=" + precio + ", strImagen=" + strImagen + ", idCategoria=" + idCategoria + '}';
    }
    
    
}
