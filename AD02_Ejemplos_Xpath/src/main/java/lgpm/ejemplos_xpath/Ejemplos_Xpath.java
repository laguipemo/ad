/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lgpm.ejemplos_xpath;

import java.io.File;
import java.io.IOException;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 *
 * @author lgpmc
 */
public class Ejemplos_Xpath {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        ejemplosXPath();
        System.out.println("\n\n" + "*".repeat(40));
        otrosEjemplosXPath();
    }
    
    private static void ejemplosXPath() {
        //Primero tengo que generar el DOM del docuemento teniendo en cuenta que
        //XPath necesita sus nodos para trabajar.
        //Creo factoria de documentos
        DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();
        try {
            //Obtengo un objeto creador de documento del cual obtengo el parse o
            //traductor para leer el documento XML y cargarlo como documento DOM
            DocumentBuilder docBuilder = docBuilderFactory.newDocumentBuilder();
            Document documento = docBuilder.parse(new File("ejemploXPath.xml"));
            
            //Obtengo la factoría de XPath a partir de la cual obtengo un objeto
            // de tipo XPath que me permita trabajar
            XPathFactory xpathFactory = XPathFactory.newInstance();
            XPath xpath = xpathFactory.newXPath();
            
            System.out.println("\n\n" + "*".repeat(40));
            //Para obtener todos los nodos autor pues solo necesito el eje indicando 
            // la ruta adecuada, ya sea de forma absoluta partiendo del nodo raíz, u 
            // otra "relativa" en la que autor solo tiene que cumplir con la condición
            // de ser descendiente
            //String expresion = "/biblioteca/libro/autor";
            //String expresion = "//autor";
            String expresion = "//libro/autor";
            //Convierto la expresión anteririor en un expresión compilade de XPath
            XPathExpression compiledExpresion = xpath.compile(expresion);
            //Evaluo la expresión
            Object resultado = compiledExpresion.evaluate(
                    documento, XPathConstants.NODESET);
            /*
            Lo anteriro se pude resumir de la siguiente manera:
            Object resultado = xpath.evaluate(expresion, documento, XPathConstants.NODESET);
            */
            
            //El resultado obtenido es un objeto que representa la colección de 
            // nodos que satisfacen la expresión compilada. Por ello tenemos que
            // hacer un casting a NodeList sobre el resultado.
            NodeList nodos = (NodeList) resultado;
            // recorro los nodos con un for tradicional llamando cada item de la
            // NodeList
            for (int i = 0; i < nodos.getLength(); i++) {
                // obtengo el nodo con índice i y de el obtengo su nombre o etiqueta
                // y si deseo puedo obtener el texto contedido en esta etiqueta 
                // mediante el método getTextContent()
                System.out.printf("%s: %s %n", 
                        nodos.item(i).getNodeName(), nodos.item(i).getTextContent());
            }
            
            System.out.println("\n" + "*".repeat(40));
            // Para obtener el atributo fecha de nacimiento de los autores entonces
            expresion = "//autor/@fechaNacimiento";
            nodos = (NodeList) xpath.evaluate(
                    expresion, documento, XPathConstants.NODESET);
            for (int i=0; i<nodos.getLength(); i++) {
                System.out.printf("%s: %s %n", 
                        nodos.item(i).getNodeName(), nodos.item(i).getNodeValue());
            }
            
            System.out.println("\n" + "*".repeat(40));
            // Otro ejemplo en el que accedemos a todos los nodos atributos año 
            // descendientes de cualquier nodo, podemos utilizar la expresión
            expresion = "//@año";
            nodos = (NodeList) xpath.evaluate(expresion, documento, XPathConstants.NODESET);
            for (int i=0; i<nodos.getLength(); i++) {
                System.out.printf("%s: %s %n", 
                        nodos.item(i).getNodeName(), nodos.item(i).getNodeValue());
            }
            
            System.out.println("\n" + "*".repeat(40));
            // Utilizando predicados
            //Predicado de tipo [número]
            //expresion = "//libro[1]"; // tomo el primero de los nodos libro descendientes
            //expresion = "//libro[last()]"; // tomo el último de los nodos libro descendientes
            expresion = "//libro[last()-1]"; // tomo el penúltimo de los nodos libro descendientes
            nodos = (NodeList) xpath.evaluate(expresion, documento, XPathConstants.NODESET);
            for (int i=0; i<nodos.getLength(); i++) {
                System.out.printf("%s: %s %n", 
                        nodos.item(i).getNodeName(), nodos.item(i).getTextContent());
            }
            
            System.out.println("\n" + "*".repeat(40));
            //Predicado de comparación
            expresion = "//fechaPublicacion[@año<1970]";
            nodos = (NodeList) xpath.evaluate(expresion, documento, XPathConstants.NODESET);
            for (int i=0; i<nodos.getLength(); i++) {
                System.out.printf("%s: %s=%s %n", 
                        nodos.item(i).getNodeName(), 
                        nodos.item(i).getAttributes().item(0).getNodeName(),
                        nodos.item(i).getAttributes().item(0).getNodeValue());
                // teniendo en cuenta que lo que devuelve getAttributes() es un
                // NamedNodeMap que es una interface utilizada para representar 
                // una colección de nodos que pueden ser accedidos por su nombre
                // NamedNodeMap no hereda de NodeList; NamedNodeMaps no mantiene
                // ningún orden en particular. Esto implica que auque también se 
                // puede acceder a los elementos como hicimos anteriormente, hay
                // que tener presente que el orden no responde al DOM ni a nada.
                // Entonces podemos acceder a atributo con nombre "año" según:
                System.out.printf("%s: %s=%s %n", 
                        nodos.item(i).getNodeName(), 
                        nodos.item(i).getAttributes().getNamedItem("año").getNodeName(),
                        nodos.item(i).getAttributes().getNamedItem("año").getNodeValue());
            }
            
            System.out.println("\n" + "*".repeat(40));
            // Predicado en el que se compara el valor de propio nodo encontrado
            // con el eje de al expresion. Se utiliza "." para referirse el valor
            // del propio nodo buscado. Por ejemplo 
            //expresion = "//@año[.>1970]"; // buscar atributo y comaprar su valor
            expresion = "//autor[.='Mario Vargas Llosa']"; // buscar autor y comaprar su valor
            nodos = (NodeList) xpath.evaluate(expresion, documento, XPathConstants.NODESET);
            for (int i=0; i<nodos.getLength(); i++) {
                // para el ejemplo del atributo
//                System.out.printf("%s: %s %n", 
//                        nodos.item(i).getNodeName(), nodos.item(i).getNodeValue());
                // para el ejemplo del elemento nodo, es necesario otener el valor
                // encerrado con esa etiqueta mediante el método getTextContent()
                System.out.printf("%s: %s %n", 
                        nodos.item(i).getNodeName(), nodos.item(i).getTextContent());
            }
           
            System.out.println("\n" + "*".repeat(40));
            //Predicados con expresiones compuestas
            //Por ejemplos buscar libros cuyo autor sea Mario Vargas Llosa y con 
            //fechaPublicacion en el año 1973
            //expresion = "//libro[autor='Mario Vargas Llosa' and fechaPublicacion/@año='1973']";
            //Ejemplo de libro que hayan sido escritos por Mario Vargas Llosa o 
            //que tengan fechaPublicacion con año igual a 1973
            //expresion = "//libro[autor='Mario Vargas Llosa' or fechaPublicacion/@año='1973']";
            //También se pueden escribir varios predicados seguidos y tales casos
            //cada uno va restringiendo el resultado del anterior como si estuvieran
            //encadenados por la operación logia and. Por ejemplo
            expresion = "//libro[autor='Mario Vargas Llosa'][fechaPublicacion/@año='1973']";
            nodos = (NodeList) xpath.evaluate(expresion, documento, XPathConstants.NODESET);
            for (int i=0; i<nodos.getLength(); i++) {
                if (!nodos.item(i).getTextContent().isBlank()) { //no imprimo lineas null o vacías
                    System.out.printf("%s: %s %n", 
                            nodos.item(i).getNodeName(), nodos.item(i).getTextContent());
                }
            }
            
            System.out.println("\n" + "*".repeat(40));
            //Parte de la expresión dedidaca a la selección de nodos. En una expresión
            //la combinación de eje y predicado(s) trae como resultado la selección
            //de una colección de nodos y la parte de selección de nodos se encarga
            //entonces de seleccionar que nodos se seleccionaran para cada uno de 
            //estos nodos. Por ejemplo:
            //expresion = "//libro/node()"; //para cada libro seleccina sus hijos (titulo, autor y fechaPublicacion)
            //expresion = "//autor/node()"; // para cada autor selecciona sus hijos (En este caso solo nodo text con el nombre)
            expresion = "//libro//node()"; // para cada autor selecciona descendientes(titulo y su hijo texto, autor y su hijo texto etc)
            nodos = (NodeList) xpath.evaluate(expresion, documento, XPathConstants.NODESET);
            for (int i=0; i<nodos.getLength(); i++) {
                if (!nodos.item(i).getTextContent().isBlank()) { // no imprimos lineas null o vacías
                    System.out.printf("%s: %s %n", 
                            nodos.item(i).getNodeName(), nodos.item(i).getTextContent());
                }
            }
            
            System.out.println("\n" + "*".repeat(40));
            //Selección de nodos, específicamente los nodos de tipo test
            //expresion = "//libro/text()"; // libro no tiene nodo texto hijo, solo aparecen linea vacías
            //expresion = "//libro//text()"; // libro si tiene descendientes con nodo texto y los muestra
            expresion = "//autor/text()"; // autor contine nodo texto hijo que es el nombre del autor
            nodos = (NodeList) xpath.evaluate(expresion, documento, XPathConstants.NODESET);
            for (int i=0; i<nodos.getLength(); i++) {
                if (!nodos.item(i).getNodeValue().isBlank()) { // no imprimir lineas nulas o vacías
                    System.out.printf("%s: %s %n", 
                            nodos.item(i).getNodeName(), nodos.item(i).getNodeValue());
                }
            }
            
            
        } catch (ParserConfigurationException parsconfEx) {
            System.out.println(parsconfEx.getMessage());
        } catch (SAXException saxEx) {
            System.out.println(saxEx.getMessage());
        } catch (IOException ioEx) {
            System.out.println(ioEx.getMessage());
        } catch (XPathExpressionException xpathexprEx) {
            System.out.println(xpathexprEx.getMessage());
        }
    }

    private static void otrosEjemplosXPath() throws DOMException {
        // creo una factoría de constructor de documentos
        DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();
        try {
            // obtengo del costructor de documentos que me permitirá leer el fichero xml
            DocumentBuilder docBuilder = docBuilderFactory.newDocumentBuilder();
            // obtengo el documento xml traducido con el parser del contructor de documentos
            Document documento = docBuilder.parse(new File("tvShows.xml"));
            
            // creo una factoria de XPath
            XPathFactory xpathFactory = XPathFactory.newInstance();
            // creo un objeo XPath que nos permitirá navegar por un documento xml
            // a través de sus elementos y parámatros utilizándolos como rutas.
            XPath newXPath = xpathFactory.newXPath();
            
            // Aunque se puede hacer en un paso, primero obtengo un objto que es una
            // expresión de XPath, obtenida de la compilación de una ruta dada
            // (formada por los nodo identificados por su elemento y parámetros que 
            // guían hasta el elemento deseado)
            // Estas rutas son similares a las de los sitemas de archivos.
            // En este emjemplo se utiliza la unión de los nodos obtenidos a partir
            // de dos rutas. La primera de ella sería la de los elementos texto (text())
            // presentes en los elementos con etiqueta real_name que sean hijo de los
            // elementos actor que a su vez sean hijos del los elementos artors de
            // cualquier padre pues se comienza con //.
            XPathExpression xpathExpression = newXPath.compile("//actors/actor/real_name/text() | //actors/actor/character/text()");
            // obtenemos un objeto que contiene o el resultado de la evaluación de 
            // del path anterior en el documento DOM dado y el tipo de resultado que se
            // desea node-set, boolean, número o cadena. En nuestro caso le pasamos
            // el DOM documento y deseamos resultados de tipo XPathConstants.NODESET
            Object resultado = xpathExpression.evaluate(documento, XPathConstants.NODESET);
            // Hacemos un casting al tipo NodeList sobre el objeto resultado obtenido
            NodeList nodos = (NodeList) resultado;
            // recorremos los elementos del NodeList nodos mediante un bucle tradicional
            // obteneniendo el item de la posición i en la colección.
            for (int i = 0; i < nodos.getLength(); i ++) {
                // a modo de ejemplo muestro en pantalla en nobre del nodo y el valor
                System.out.printf("%s: %s %n",
                        nodos.item(i).getParentNode().getNodeName(), 
                        nodos.item(i).getNodeValue());
            }
            
            System.out.println("\n");
            
            // obtener los nodos texto de los hijos del nodo show que tenga cualquier 
            // hijo con un atributo id_code igual a la cadena "show_002"
            xpathExpression = newXPath.compile("//show[*[@id_code='show_002']]/child::node()/text()");
            resultado = xpathExpression.evaluate(documento, XPathConstants.NODESET);
            nodos = (NodeList) resultado;
            for (int i = 0; i < nodos.getLength(); i++) {
                if (!nodos.item(i).getNodeValue().isBlank()) {
                    System.out.println(nodos.item(i).getNodeValue());
                }
            }
            
            System.out.println("\n");
            
            // obtener los nodos texto de lo hijos del nodo show tenga un hijo 
            // release igual a 2006
            xpathExpression = newXPath.compile("//show[release=2006]/child::node()/text()");
            resultado = xpathExpression.evaluate(documento, XPathConstants.NODESET);
            nodos = (NodeList) resultado;
            for (int i = 0; i < nodos.getLength(); i++) {
                if (!nodos.item(i).getNodeValue().isBlank()) {
                    System.out.println(nodos.item(i).getNodeValue());
                }
            }
            
            System.out.println("\n");
            
            // Obtener los nodos de tipo texto de todos los hermanos que sigan a
            // los elementos name que tengan un atributo id_code igual a la cadena
            // 'show_002'
            xpathExpression = newXPath.compile("//name[@id_code='show_002']/following-sibling::*/text()");
            resultado = xpathExpression.evaluate(documento, XPathConstants.NODESET);
            nodos = (NodeList) resultado;
            for (int i = 0; i < nodos.getLength(); i++) {
                if (!nodos.item(i).getNodeValue().isBlank()) { // solo los no vacios
                    System.out.println(nodos.item(i).getNodeValue());
                }
            }
            System.out.println("\n");
            
            // Obtener los nodos de tipo texto de todos los hermanos que antecedan a
            // los elementos network que tengan un atributo country igual a la cadena 
            // 'UK'
            xpathExpression = newXPath.compile("//network[@country='UK']/preceding-sibling::*/text()");
            resultado = xpathExpression.evaluate(documento, XPathConstants.NODESET);
            nodos = (NodeList) resultado;
            for (int i = 0; i < nodos.getLength(); i++) {
                if (!nodos.item(i).getNodeValue().isBlank()) { // solo los no vacios
                    System.out.println(nodos.item(i).getNodeValue());
                }
            }
            System.out.println("\n");
            
            // Obtener los nodos de tipo texto de todos los hijos de los elementos
            // show que tengan cualquier elemento con un atributo id_code igual a
            // la cadena 'show_002', es decir:
            //      "//show[*[@id_code='show_002']]/child::node()/text() "
            // unidos a los nodos de tipo texto que sean hijos de actor que a su
            // seja hijo de actors, pero pertenezcan (hijos) a un nodo show que 
            // cuente con cualquier elemento con atributo id_code igual a la cadena
            // 'show_002', es decir: 
            //  "//show[*[@id_code='show_002']]/actors/actor/child::node()/text()"
            xpathExpression = newXPath.compile(
                    "//show[*[@id_code='show_002']]/child::node()/text() " +
                            "| //show[*[@id_code='show_002']]/actors/actor/child::node()/text()");
            resultado = xpathExpression.evaluate(documento, XPathConstants.NODESET);
            nodos = (NodeList) resultado;
            for (int i = 0; i < nodos.getLength(); i++) {
                if (!nodos.item(i).getNodeValue().isBlank()) {
                    System.out.println(nodos.item(i).getNodeValue());
                }
            }
            System.out.println("\n");
            
            // Obtener todos los atributos href
            xpathExpression = newXPath.compile("//@href");
            resultado = xpathExpression.evaluate(documento, XPathConstants.NODESET);
            nodos = (NodeList) resultado;
            for (int i = 0; i < nodos.getLength(); i++) {
                if (!nodos.item(i).getNodeValue().isBlank()) {
                    System.out.println(nodos.item(i).getNodeValue());
                }
            }
            
        } catch (ParserConfigurationException parsconfEx) {
            System.out.println(parsconfEx.getMessage());
        } catch (SAXException saxEx) {
            System.out.println(saxEx.getMessage());
        } catch (IOException ioEx) {
            System.out.println(ioEx.getMessage());
        } catch (XPathExpressionException xpathexpresEx) {
            System.out.println(xpathexpresEx.getMessage());
        }
    }
    
    
    
}
