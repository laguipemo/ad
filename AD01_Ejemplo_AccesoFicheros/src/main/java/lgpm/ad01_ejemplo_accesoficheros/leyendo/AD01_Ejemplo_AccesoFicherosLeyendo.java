/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lgpm.ad01_ejemplo_accesoficheros.leyendo;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Iterator;
import java.util.stream.Stream;


class LeerFichero {
    private String strFichero;

    public LeerFichero() {
        this("ejemplo.txt");
    }

    public LeerFichero(String strFichero) {
        this.strFichero = strFichero;
    }

    public void lee() {
        try (FileReader entrada = new FileReader(this.strFichero)){
            //lectura caracter a caracter
            int codLetra;
            while ((codLetra = entrada.read()) != -1) {
                char letra = (char) codLetra;
                System.out.print(letra);
            }

        } catch (FileNotFoundException fnfEx) {
            System.out.println(fnfEx.getMessage());
        } catch (IOException ioEx) {
            System.out.println(ioEx.getMessage());
        }
    }
    
    public void leeBuffered() {
        try (BufferedReader bufferLectura = new BufferedReader(
                new FileReader(this.strFichero))) {
            
            /*
            Puedo leer todo el fichero en un stream y luego recorrer
             sus líneas con un iterador 
            */
//            Stream<String> streamLineasFichero = bufferLectura.lines();
//            Iterator<String> iteradorLineas = streamLineasFichero.iterator();
//            while (iteradorLineas.hasNext()) {
//                System.out.println(iteradorLineas.next());
//            }
            
            /*
            Resulta más corto leer linea a linea 
            */
            String linea;
            while ((linea = bufferLectura.readLine()) != null) {
                System.out.println(linea);
            }
            
        } catch (FileNotFoundException fnfEx) {
            System.out.println(fnfEx.getMessage());
        } catch (IOException ioEx) {
            System.out.println(ioEx.getMessage());
        }
    }
}


/**
 *
 * @author lgpmc
 */
public class AD01_Ejemplo_AccesoFicherosLeyendo {
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        LeerFichero leerFichero = new LeerFichero();
//        leerFichero.lee();
        leerFichero.leeBuffered();
    }
    
}
