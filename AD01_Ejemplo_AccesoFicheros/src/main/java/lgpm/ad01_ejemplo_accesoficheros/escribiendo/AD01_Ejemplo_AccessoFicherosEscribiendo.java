/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lgpm.ad01_ejemplo_accesoficheros.escribiendo;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.logging.Level;
import java.util.logging.Logger;


class Escribiendo {
    private String strFichero;
    private String fraseAEscribir = "Estoy escribiendo un fichero de prueba";

    public Escribiendo() {
        this("ejemploEscriSecu.txt");
    }

    public Escribiendo(String strFichero) {
        this.strFichero = strFichero;
    }
    
    public Escribiendo(String strFichero, String fraseAEscribir) {
        this(strFichero);
        this.fraseAEscribir = fraseAEscribir;
    }
    
    public void escribir() {
        try (FileWriter escritura = new FileWriter(this.strFichero, true)){
            escritura.append(fraseAEscribir + "\n");
        } catch (IOException ioEx) {
            System.out.println("Error de IO al escribir en fichero");
            System.out.println(ioEx.getMessage());;
        }
    }
    
    public void escribir(String frase) {
        try (FileWriter escritura = new FileWriter(this.strFichero, true)) {
            escritura.append(frase + "\n");
        } catch (IOException ioEx) {
            System.out.println(ioEx.getMessage());
        }
    }
    
    // para comprobación
    
}

class EscribiendoRandom {
    private String strFichero;
    private int datoIntEjemplo = 1847;

    public EscribiendoRandom() {
        this("ejemploEscriRand.txt");
    }

    public EscribiendoRandom(String strFichero) {
        this.strFichero = strFichero;
    }
    
    public void escribir() {
        try (RandomAccessFile ficheroRandom = new RandomAccessFile(this.strFichero, "rw")){
         // escribir un entero (inicialmente la posicion es 0L)
         ficheroRandom.writeInt(this.datoIntEjemplo);
         // colocar el puntero en la posición 0 para leer el entero entrado
         ficheroRandom.seek(0);
         // leer el entero entrado
         System.out.println("" + ficheroRandom.readInt());
         // regresar el puntero nuevamente a la posición 0
         ficheroRandom.seek(0);
         // escribir el literal entero 200
         ficheroRandom.writeInt(200);
         // regresar el puntero nuevamente a la posición 0 para leer dato entrado
         ficheroRandom.seek(0);
         // leer e imprimir el entero en la posición 0
         System.out.println("" + ficheroRandom.readInt());
         
      } catch (IOException ioEx) {
            System.out.println(ioEx.getMessage());
      }
   }
}

class LeerFichero {
    
    private String strFichero;

    public LeerFichero() {
        this("ejemploEscriSecu.txt");
    }
    
    public LeerFichero(String strFichero) {
        this.strFichero = strFichero;
    }
    
    public void lee() {
        try (FileReader entrada = new FileReader(this.strFichero)){
            //lectura caracter a caracter
            int codLetra;
            while ((codLetra = entrada.read()) != -1) {
                char letra = (char) codLetra;
                System.out.print(letra);
            }

        } catch (FileNotFoundException fnfEx) {
            System.out.println(fnfEx.getMessage());
        } catch (IOException ioEx) {
            System.out.println(ioEx.getMessage());
        }
    }
}

class LeerFicheroRandom {
    
    private String strFichero;

    public LeerFicheroRandom() {
        this("ejemploEscriRand.txt");
    }

    public LeerFicheroRandom(String strFichero) {
        this.strFichero = strFichero;
    }
    
    public void lee() {
        try (RandomAccessFile ficheroRandom = new RandomAccessFile(this.strFichero, "r")) {
            System.out.println(ficheroRandom.readInt());
        } catch (FileNotFoundException fnfEx) {
            System.out.println(fnfEx.getMessage());
        } catch (IOException ioEx) {
            System.out.println(ioEx.getMessage());
        }
    }
    
}

/**
 *
 * @author lgpmc
 */
public class AD01_Ejemplo_AccessoFicherosEscribiendo {
        
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Escribiendo salidaSecu = new Escribiendo();
        salidaSecu.escribir();
        salidaSecu.escribir("Todo ok");
        LeerFichero lectura = new LeerFichero();
        lectura.lee();
        
        EscribiendoRandom salidaRand = new EscribiendoRandom();
        salidaRand.escribir();
        LeerFicheroRandom lecturaRand = new LeerFicheroRandom();
        lecturaRand.lee();
    }
    
}
