-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 14-01-2021 a las 20:22:52
-- Versión del servidor: 10.4.13-MariaDB
-- Versión de PHP: 7.4.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `restaurante`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categorias`
--

CREATE TABLE `categorias` (
  `id_categoria` int(11) NOT NULL,
  `nombre_it` varchar(150) COLLATE utf8_spanish_ci NOT NULL,
  `nombre_es` varchar(150) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `categorias`
--

INSERT INTO `categorias` (`id_categoria`, `nombre_it`, `nombre_es`) VALUES
(1, 'Antipasti', 'Entremeses'),
(2, 'Insalate', 'Ensaladas'),
(3, 'Pizzas', ''),
(4, 'Primi Piatti', 'Pastas'),
(5, 'Secondi Piatti', 'Carnes'),
(6, 'Dolci', 'Postres'),
(7, 'Gelati', 'Helado');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `clientes`
--

CREATE TABLE `clientes` (
  `id_cliente` int(11) NOT NULL,
  `nombre` varchar(150) COLLATE utf8_spanish_ci NOT NULL,
  `telefono` varchar(12) COLLATE utf8_spanish_ci NOT NULL,
  `direccion` varchar(200) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `clientes`
--

INSERT INTO `clientes` (`id_cliente`, `nombre`, `telefono`, `direccion`) VALUES
(1, 'Lázaro Guillermo Pérez Montoto', '669355999', 'Avenida Agustin Romero, 44, 1B, CP 36600. Vilagarcía'),
(2, 'Aitana Pérez Carrión', '666666666', 'Avenida Agustin Romero, 44, 1B, CP 36600. Vilagarcía');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pedidos`
--

CREATE TABLE `pedidos` (
  `id_pedido` int(11) NOT NULL,
  `fecha` date NOT NULL,
  `hora` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `pedidos`
--

INSERT INTO `pedidos` (`id_pedido`, `fecha`, `hora`) VALUES
(1, '2020-08-08', '20:20:00'),
(2, '2020-08-08', '20:30:00'),
(3, '2020-08-10', '14:05:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pedidos_clientes`
--

CREATE TABLE `pedidos_clientes` (
  `id_pedido` int(11) NOT NULL,
  `id_cliente` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `pedidos_clientes`
--

INSERT INTO `pedidos_clientes` (`id_pedido`, `id_cliente`) VALUES
(1, 1),
(2, 2),
(3, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `productos`
--

CREATE TABLE `productos` (
  `id_producto` int(11) NOT NULL,
  `nombre` varchar(150) COLLATE utf8_spanish_ci NOT NULL,
  `descripcion` text COLLATE utf8_spanish_ci NOT NULL,
  `precio` decimal(5,2) NOT NULL,
  `imagen` varchar(200) COLLATE utf8_spanish_ci NOT NULL,
  `id_categoria` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `productos`
--

INSERT INTO `productos` (`id_producto`, `nombre`, `descripcion`, `precio`, `imagen`, `id_categoria`) VALUES
(1, 'PROCCIUTTO E MELONE', 'Jamón serrano con melón', '10.20', 'prosciutto-melone-e-rucola.jpg', 1),
(2, 'ANTIPASTO ALL´ITALINO', 'Selección de embutidos y quesos italiano', '12.50', 'antipasto-italiano.jpg', 1),
(3, 'PARMIGGINA DE MELENZANE', 'Berenjerenas, tomates, mozzarella, ajo, parmesano', '9.80', 'parmiggina-de-melenzane.jpg', 1),
(4, 'CALAMARI FRITTI', 'Calamares fritos', '10.25', 'calamari-fritti.jpg', 1),
(5, 'CALAMARI ALLA GENOVESE', 'Calamares, tomate, ajo, alcaparras, vino blanco, perejil, cebolla', '10.25', 'calamarata-genovese.jpg', 1),
(6, 'AVOCADO E GAMBERETTI', 'Aguacate, gambas peladas, salsa rosa', '11.00', 'avocado-e-gamberetti.jpg', 1),
(7, 'FORMAGGI MISTI', 'Selección de quesos italianos', '11.50', 'formaggi-misti.jpg', 1),
(8, 'COZZE ALLA NAPOLETANA', 'Mejillones, tomate, ajo, vino blanco', '8.80', 'cozze-alla-napoletana.jpg', 1),
(9, 'PROVOLONE ALLA PLANCIA', 'queso especial al plancha', '12.50', 'provolone-alla-plancia.jpg', 1),
(10, 'CARPACCIO SOLOMILLO', 'Marinado con limón, aceite, rúcula, parmasano, champiñones frescos', '12.20', 'carpaccio-solomillo.jpg', 1),
(11, 'CARPACCIO SALMONE', 'Marinado con limón, aceite, caviar y vodka', '12.20', 'carpaccio-salmone-marinato.jpg', 1),
(12, 'CARPACCIO DI BRESAOLA', 'Bresaola cortado muy finamente, marinado en limón y aceite virgen extra, trufa, rúcula y parmesano', '11.40', 'carpaccio-di-bresaola.jpg', 1),
(13, 'PANE E AGLIO', '', '4.00', 'pane-e-aglio.jpg', 1),
(14, 'INSALATA CAPRESE BUFALA', 'Tomate, mozzarella y albahaca', '10.50', 'insalata-caprese.jpg', 2),
(15, 'INSALATA TRICOLORES BUFALA', 'Manzana, aguacate, piña, lechuga, zanahoria, tomate y nata', '11.50', 'insalata-tricolore-bufala.jpg', 2),
(16, 'INSALATA SORRENTINA', 'Lechuga, pasta, pollo, tomate, aguacate, cebolla, pimiento y rúcula', '10.80', '', 2),
(17, 'INSALATA ALLA GRECA', 'Lechuga, radiquio, tomate en trozos, pimiento, berenjeras a la plancha, queso feta y ternera en lonchas, nata', '11.80', '', 2),
(18, 'INSALATA NORDICA', 'Hojas de roble, escarola, salmón ahumado, piñones, tomate cherry, picantones, cebolla crujiente de jamón y vinagreta de vinagre de Módena', '11.80', '', 2),
(19, 'INSALATA ITALICA', 'Mezcla de lechugas con tomates a la plancha, pechuga empanada cortada en lonchas, aguacate, nueces y aliñada con vinagreta de mostaza', '11.80', '', 2),
(20, 'INSALATA TAVOLA DI VEGETALLI', 'Tabla de vegetales a la plancha servidos con picadillo de fruto seco', '11.80', '', 2),
(21, 'PIZZA MARGHERITA', 'Tomate, mozzarella, albahaca', '8.25', '', 3),
(22, 'PIZZA NAPOLETANA', 'Tomate, mozzarella, anchoas, aceitunas, alcaparras', '9.50', '', 3),
(23, 'PIZZA DONATELLO', 'Tomate, mozzarella, jamón york', '9.50', '', 3),
(24, 'PIZZA PROSCIUTTO E FUNGÍ', 'Tomate, mozzarella, jamón york, champiñones', '9.80', '', 3),
(25, 'PIZZA AMERICAN HOT', 'Tomate, mozzarella, pimientos, chorizo, guindilla', '10.20', '', 3),
(26, 'PIZZA CARRETTIERA', 'Tomate, mozzarella, berenjenas, chorizo, champiñones, aceitunas', '10.90', '', 3),
(27, 'PIZZA PUGLIESE', 'Tomate, mozzarella, atún, cebolla', '10.90', '', 3),
(28, 'PIZZA CAPRICCIOSA', 'Tomate, mozzarella, pimientos, champiñones, alcachofas, huevo, aceitunas, jamón york', '11.00', 'pizza-capricciosa.jpg', 3),
(29, 'PIZZA 4 STAGIONI', 'Tomate, mozzarella, jamón york, champiñones alcachofas, anchoas', '11.00', '', 3),
(30, 'PIZZA HAWAIANA', 'Tomate, mozzarella, jamón york, piña', '10.00', '', 3),
(31, 'PIZZA MARE & MONTI', 'Tomate, mozzarella, jamón york, champiñones, mejillones, calamares, gambas, ajo', '13.20', '', 3),
(32, 'PIZZA 4 FORMAGGI', 'Tomate, mozzarella, gorgonzola, pecorino, parmiggiano', '11.00', '', 3),
(33, 'PIZZA PAPALINA', 'Tomate, mozzarella, jamón serrano, huevo', '11.00', '', 3),
(34, 'CALZONE (PIZZA DOBLADA RELLENA', 'Tomate, mozzarella, jamón york, champiñones, pimientos', '10.80', '', 3),
(35, 'CALZONE PARMIGGIANA', 'Tomate, mozzarella, berenjenas, parmesiano', '9.90', '', 3),
(36, 'CALZONE SALMONATA', 'Tomate, mozzarella, salmón ahumado, nata', '12.80', '', 3),
(37, 'PIZZA VEGETARIANA', 'Tomate cherry, mozzarella, brécol, calabacín, berenjenas, espárrago triguero', '10.20', '', 3),
(38, 'PIZZA ROSSINI', 'tomate, mozzarella, jamón york, champiñones, huevo, aceitunas', '10.50', '', 3),
(39, 'PIZZA NUOVA', 'Tomate, mozzarella, beicon, chorizo, huevo', '10.50', '', 3),
(40, 'PIZZA EMILIANA', 'Tomate, mozzarella, gorgonzola, jamón serrano', '11.00', '', 3),
(41, 'PIZZA VULCANO (PIZZA ESPECIAL PARA DOS PERSONAS)', 'Tomate, mozzarella, alcachofas, champiñones, jamón york, chorizo, pimientos, espárragos', '22.00', '', 3),
(42, 'PIZZA AL DUOMO', 'Mozzarella, rúcula, parmesano, jamón serrano', '11.20', '', 3),
(43, 'PIZZA TOSCANA', 'Boletus, alcachofa, ajo, tomate, mozzarella', '11.40', '', 3),
(44, 'PIZZA PRIMAVERA', 'Espárrago triguero, pollo, toamte a la plancha, queso de cabra', '11.50', '', 3),
(45, 'PIZZA PARIGINA', 'Jamón serrano, queso de cabra, miel', '11.50', '', 3),
(46, 'CREP VEGETALES', 'Brécol, tomate cherry, ricota', '10.50', '', 4),
(47, 'CREPES ALLA LOCANDAIA', 'Salsa de tomate, bechamel, huevo pica, jamón, champiñones', '10.50', '', 4),
(48, 'TAGLIATELLE (Cintas al huevo) ALLA DOMUS FLAMINIA', 'Gambas, lagostinos,ajo,perejil,tomate cherry y vino blanco', '13.00', '', 4),
(49, 'TAGLIATELLE (Cintas al huevo) TIMBALO DI TAORMINA', 'Berenjenas, queso, tomate, pesto de albahaca', '11.20', '', 4),
(50, 'TAGLIATELLE (Cintas al huevo) AL PESTO', 'Albahaca, piñones, queso de cabra, parmasano, aceite de oliva', '11.00', '', 4),
(51, 'TAGLIATELLE (Cintas al huevo) SOTTO-BOSCO', 'Trufa de temporada, paté de canard y nata servido con virutas de parmesano', '14.50', '', 4),
(52, 'TORTELLONI (Pasta rellena de queso y espinascas) PASTICCIATI', 'boloñesa, bechamel, mozarella', '11.20', '', 4),
(53, 'LASAGNE DI CARNE', '', '11.50', '', 4),
(54, 'LASAGNE VEGETAL', '', '11.50', '', 4),
(55, 'CANNELLONI ROSSINI', '', '11.50', '', 4),
(56, 'GNOCCHI (Pasta de patata y sémola) AL 4 FORMAGGIO', '', '11.50', '', 4),
(57, 'GNOCCHI (Pasta de patata y sémola) ALLA CAPRESE', 'Tomate, mozzarella, perejil', '11.50', '', 4),
(58, 'RIGATONI (Macarrones) ALLA PUTASNESCA', 'Tomate, anchoa, aceitunas, guindilla, perejil', '10.20', '', 4),
(59, 'RIGATONI (Macarrones) ALLA EMILIA', 'Radiquio, jamón serrano, rúcula, gorgonzola, nata', '11.00', '', 4),
(60, 'RIGATONI (Macarrones) ALLA SICILIANA', 'Tomate, berenjena, cebolla, queso de oveja, albahaca', '11.00', '', 4),
(61, 'RIGATONI (Macarrones) ALLA GIANISA', 'Macarrones en salsa de: jamón serrano, champiñones, mozzarella de búfala, berenjenas y tomates', '11.00', '', 4),
(62, 'SPAGHETTI CARBONARA', 'Nata, panceta, yema de huevo, parmesano', '10.50', '', 4),
(63, 'SPAGHETTI VONGOLE', 'Almejas, tomates, vino blanco, perejil, ajo', '13.00', '', 4),
(64, 'SPAGHETTI MARINARA', 'Ajo, tomate, calamares, almejas, mejillones, gambas, langostinos, vino blanco', '13.00', 'pasta-marinara.jpg', 4),
(65, 'SPAGETTI NERI AL TONNO', 'Tomate, atún, cebolla, aceitunas', '10.50', '', 4),
(66, 'SPAGETTI NERI MARE CHIARO', 'Langostinos, gambas, erizo de mar, vino blanco, tomate en trozos, ajo, nata', '13.00', '', 4),
(67, 'SPAGHETTI INTEGRAL VILLA D´ESTE', 'Espárragos, brécol, tomate cherry, calabacín, berenjena', '11.00', '', 4),
(68, 'RAVIOLI (Pasta rellena de carne) ALLA BOLOGNESE', 'Carne picada, cebolla, zanahoria, apio, tomate', '11.20', '', 4),
(69, 'RAVIOLI (Pasta rellena de carne) GRATINATI', 'Nata, gorgonzola, mozzarella', '11.50', '', 4),
(70, 'RAVIOLI (Pasta rellena de carne) CREMA DI ZUCCA Y TALEGGIO', 'Crema de calabaza y crema de queso taleggio', '11.50', '', 4),
(71, 'PAGLIA E FIENO (Cintas de huevo y espinacas) CARTOCCIO', 'Tomate en trozos, calamares, almejas, ajo, mejillones, langostinos, gambas, vino blanco', '13.00', '', 4),
(72, 'RISOTTI (Arroces) AL SALMONE', 'Salmón ahumado, coñac, nata, tomate', '13.00', '', 4),
(73, 'RISOTTI (Arroces) ALLA MARINARA', 'Tomate en trozos, almejas, mejillones, calamares, gambas, langostinos, vino blanco, ajo, perejil', '13.00', '', 4),
(74, 'RISOTTI (Arroces) ALLA GIUSEPPE VERDI', 'Arroz, mejillones, almejas, espárragos, trozos de tomate y boletus', '13.00', '', 4),
(75, 'FILETTO (Solomillo de ternera) GRIGLIA', 'A la plancha', '9.00', '', 5),
(76, 'FILETTO (Solomillo de ternera) PEPE VERDE', 'Pimienta verde, nata, coñac, demiglace', '19.90', '', 5),
(77, 'FILETTO (Solomillo de ternera) GORGONZOLA', 'Nata, gorgonzola, queso', '19.90', 'filetto-al-gorgonzola.jpg', 5),
(78, 'FILETTO (Solomillo de ternera) FUNGHI PORCINI', 'Setas silvestres, cebolla, coñac, nata, demiglace', '19.90', '', 5),
(79, 'FILETTO (Solomillo de ternera) ROSSINI', 'Coñac, paté de hígado, demiglace, nata', '22.00', '', 5),
(80, 'FILETTO (Solomillo de ternera) FILETTO PUCCINI', 'Solomillo en salsa de vino blanco y vino marsala, bolutus, jamón serrano', '22.00', '', 5),
(81, 'CONTROFILETTO (Entrecot de ternera) AL SAN CARLO', 'Mostaza, nata, coñac', '18.50', '', 5),
(82, 'CONTROFILETTO (Entrecot de ternera) AL CARTOCCIO', 'Setas silvestres, cebolla, pimientos, tomate, vino blanco demiglace', '19.50', '', 5),
(83, 'CONTROFILETTO (Entrecot de ternera) AL PICCOLO', 'Nata, gorgonzola, rúcula', '19.50', '', 5),
(84, 'SCALOPPINE(Escalopes) AL LIMONE', 'En salsa de limón', '15.00', '', 5),
(85, 'SCALOPPINE(Escalopes) AL MARSALA', 'Salsa de vino Marsala', '15.00', '', 5),
(86, 'SCALOPPINE(Escalopes) ALLA ROMANA', 'Vino blanco, jamón serrano, salvia, demiglace', '15.00', '', 5),
(87, 'SCALOPPINE(Escalopes) ALLA PIZZAIOLA', 'Vino blanco, tomate, ajo, aceituna, alcaparras, orégano', '15.00', '', 5),
(88, 'SCALOPPINE(Escalopes) AL FUNGHI', 'Vino blanco, champiñones, ajo, demiglace', '15.00', '', 5),
(89, 'POLLO (Pechuga de pollo) ALLA MILANESE', 'Empanado con huevo y pan rallado, servido con espaguetis con tomate', '12.50', '', 5),
(90, 'POLLO (Pechuga de pollo) ALLA VALDOSTANA', 'Jamón york, queso, nata, vino blanco, demiglace', '13.50', '', 5),
(91, 'POLLO (Pechuga de pollo) ALLA PRINCIPESSA', 'Beicon, espárrago triguero, nata, mozzarella, vino blanco', '13.50', '', 5),
(92, 'TIRAMISÚ', 'Típico postre italiano de crema mascarpone y café', '5.20', '', 6),
(93, 'MOUSSE AL LIMONE', 'Mousse de limón con salsa de fresa', '6.90', '', 6),
(94, 'MOUSSE AL CHIOCCOLATO', 'Mousse de chocolate', '4.90', '', 6),
(95, 'CREAM CARAMEL', 'Flan de huevo', '4.90', '', 6),
(96, 'BANOFFI', 'Tarta de dulce de leche, plátano y nata, sobre base de galleta', '5.20', 'tarta-banoffi.jpg', 6),
(97, 'BLANCA E NERA', 'Tarta de mousse de chocolate con base de bizcocho ligero de almendra y chocolate', '5.20', '', 6),
(98, 'BIGNE', 'Profiteroles rellenos de nata servidos con chocolate caliente', '5.20', '', 6),
(99, 'TARTA EMILIANA', 'Tarta de mousse de turrón y chocolate, fino bizcocho de almendras y canela, almendras garrapiñasdas trituradas, servida con salsa de chocolate y nata', '5.20', '', 6),
(100, 'TARTE DE MANGO', 'Bizcocho de queso de cabra fresco, mermelada de frutas del bosque, frambuesa, mousse de mango, cobertura de chocolate blanco', '5.20', '', 6),
(101, 'TORRONE', '', '4.90', '', 7),
(102, 'CHOCOLATE', '', '4.90', '', 7),
(103, 'FRESA CON YOGURT GRIEGO', '', '4.90', '', 7),
(104, 'HELADO DE NESQUIK', '', '4.90', '', 7),
(105, 'HELADO DE VAINILLA', '', '4.90', '', 7),
(106, 'HELADO DE NATA', '', '4.90', '', 7),
(107, 'HELADO DE LIMÓN', '', '4.90', '', 7),
(108, 'HELADO DE YOGURT GRIEGO CON ARÁNDANOS', '', '4.90', '', 7),
(109, '1 BOLA', '', '2.50', '', 7);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `productos_pedidos`
--

CREATE TABLE `productos_pedidos` (
  `id_producto` int(11) NOT NULL,
  `id_pedido` int(11) NOT NULL,
  `cantidad` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `productos_pedidos`
--

INSERT INTO `productos_pedidos` (`id_producto`, `id_pedido`, `cantidad`) VALUES
(2, 1, 2),
(4, 2, 1),
(5, 2, 1),
(14, 1, 1),
(14, 3, 1),
(28, 3, 1),
(64, 1, 1),
(77, 2, 1),
(96, 3, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `id_usuario` int(11) NOT NULL,
  `usuario` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`id_usuario`, `usuario`, `password`) VALUES
(1, 'admin', '21232f297a57a5a743894a0e4a801fc3'),
(2, 'lazaro', 'e8f56327bd45e83ace7a4734887604ec');

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `view_ped_cli`
-- (Véase abajo para la vista actual)
--
CREATE TABLE `view_ped_cli` (
`id_pedido` int(11)
,`fecha` date
,`hora` time
,`cliente` varchar(150)
,`telefono` varchar(12)
,`direccion` varchar(200)
);

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `view_ped_cli_prod_cant`
-- (Véase abajo para la vista actual)
--
CREATE TABLE `view_ped_cli_prod_cant` (
`id_pedido` int(11)
,`fecha` date
,`hora` time
,`cliente` varchar(150)
,`telefono` varchar(12)
,`direccion` varchar(200)
,`nombre` varchar(150)
,`descripcion` text
,`precio` decimal(5,2)
,`imagen` varchar(200)
,`id_categoria` int(11)
,`cantidad` int(2)
);

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `view_prod_categ`
-- (Véase abajo para la vista actual)
--
CREATE TABLE `view_prod_categ` (
`id_producto` int(11)
,`nombre` varchar(150)
,`descripcion` text
,`precio` decimal(5,2)
,`imagen` varchar(200)
,`id_categoria` int(11)
,`categoria_it` varchar(150)
,`categoria_es` varchar(150)
);

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `view_prod_ped_cantidad`
-- (Véase abajo para la vista actual)
--
CREATE TABLE `view_prod_ped_cantidad` (
`id_pedido` int(11)
,`fecha` date
,`hora` time
,`nombre` varchar(150)
,`descripcion` text
,`precio` decimal(5,2)
,`imagen` varchar(200)
,`id_categoria` int(11)
,`cantidad` int(2)
);

-- --------------------------------------------------------

--
-- Estructura para la vista `view_ped_cli`
--
DROP TABLE IF EXISTS `view_ped_cli`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_ped_cli`  AS  select `ped`.`id_pedido` AS `id_pedido`,`ped`.`fecha` AS `fecha`,`ped`.`hora` AS `hora`,`p_cl`.`cliente` AS `cliente`,`p_cl`.`telefono` AS `telefono`,`p_cl`.`direccion` AS `direccion` from (`pedidos` `ped` join (select `pc`.`id_pedido` AS `id_pedido`,`cl`.`nombre` AS `cliente`,`cl`.`telefono` AS `telefono`,`cl`.`direccion` AS `direccion` from (`pedidos_clientes` `pc` join `clientes` `cl` on(`pc`.`id_cliente` = `cl`.`id_cliente`))) `p_cl` on(`ped`.`id_pedido` = `p_cl`.`id_pedido`)) ;

-- --------------------------------------------------------

--
-- Estructura para la vista `view_ped_cli_prod_cant`
--
DROP TABLE IF EXISTS `view_ped_cli_prod_cant`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_ped_cli_prod_cant`  AS  select `vpc`.`id_pedido` AS `id_pedido`,`vpc`.`fecha` AS `fecha`,`vpc`.`hora` AS `hora`,`vpc`.`cliente` AS `cliente`,`vpc`.`telefono` AS `telefono`,`vpc`.`direccion` AS `direccion`,`vppc`.`nombre` AS `nombre`,`vppc`.`descripcion` AS `descripcion`,`vppc`.`precio` AS `precio`,`vppc`.`imagen` AS `imagen`,`vppc`.`id_categoria` AS `id_categoria`,`vppc`.`cantidad` AS `cantidad` from (`view_ped_cli` `vpc` join `view_prod_ped_cantidad` `vppc` on(`vpc`.`id_pedido` = `vppc`.`id_pedido`)) ;

-- --------------------------------------------------------

--
-- Estructura para la vista `view_prod_categ`
--
DROP TABLE IF EXISTS `view_prod_categ`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_prod_categ`  AS  select `p`.`id_producto` AS `id_producto`,`p`.`nombre` AS `nombre`,`p`.`descripcion` AS `descripcion`,`p`.`precio` AS `precio`,`p`.`imagen` AS `imagen`,`p`.`id_categoria` AS `id_categoria`,`c`.`nombre_it` AS `categoria_it`,`c`.`nombre_es` AS `categoria_es` from (`productos` `p` join `categorias` `c` on(`p`.`id_categoria` = `c`.`id_categoria`)) ;

-- --------------------------------------------------------

--
-- Estructura para la vista `view_prod_ped_cantidad`
--
DROP TABLE IF EXISTS `view_prod_ped_cantidad`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_prod_ped_cantidad`  AS  select `ped`.`id_pedido` AS `id_pedido`,`ped`.`fecha` AS `fecha`,`ped`.`hora` AS `hora`,`p_p`.`nombre` AS `nombre`,`p_p`.`descripcion` AS `descripcion`,`p_p`.`precio` AS `precio`,`p_p`.`imagen` AS `imagen`,`p_p`.`id_categoria` AS `id_categoria`,`p_p`.`cantidad` AS `cantidad` from (`pedidos` `ped` join (select `pp`.`id_pedido` AS `id_pedido`,`pp`.`cantidad` AS `cantidad`,`p`.`nombre` AS `nombre`,`p`.`descripcion` AS `descripcion`,`p`.`precio` AS `precio`,`p`.`imagen` AS `imagen`,`p`.`id_categoria` AS `id_categoria` from (`productos_pedidos` `pp` join `productos` `p` on(`pp`.`id_producto` = `p`.`id_producto`))) `p_p` on(`ped`.`id_pedido` = `p_p`.`id_pedido`)) ;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `categorias`
--
ALTER TABLE `categorias`
  ADD PRIMARY KEY (`id_categoria`);

--
-- Indices de la tabla `clientes`
--
ALTER TABLE `clientes`
  ADD PRIMARY KEY (`id_cliente`);

--
-- Indices de la tabla `pedidos`
--
ALTER TABLE `pedidos`
  ADD PRIMARY KEY (`id_pedido`);

--
-- Indices de la tabla `pedidos_clientes`
--
ALTER TABLE `pedidos_clientes`
  ADD PRIMARY KEY (`id_pedido`,`id_cliente`),
  ADD KEY `id_cliente` (`id_cliente`);

--
-- Indices de la tabla `productos`
--
ALTER TABLE `productos`
  ADD PRIMARY KEY (`id_producto`),
  ADD KEY `PROD_FK` (`id_categoria`);

--
-- Indices de la tabla `productos_pedidos`
--
ALTER TABLE `productos_pedidos`
  ADD PRIMARY KEY (`id_producto`,`id_pedido`),
  ADD KEY `id_pedido` (`id_pedido`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`id_usuario`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `categorias`
--
ALTER TABLE `categorias`
  MODIFY `id_categoria` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT de la tabla `clientes`
--
ALTER TABLE `clientes`
  MODIFY `id_cliente` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `pedidos`
--
ALTER TABLE `pedidos`
  MODIFY `id_pedido` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `productos`
--
ALTER TABLE `productos`
  MODIFY `id_producto` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=123;

--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `id_usuario` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `pedidos_clientes`
--
ALTER TABLE `pedidos_clientes`
  ADD CONSTRAINT `PEDCLI_FK` FOREIGN KEY (`id_pedido`) REFERENCES `pedidos` (`id_pedido`) ON DELETE CASCADE,
  ADD CONSTRAINT `pedidos_clientes_ibfk_1` FOREIGN KEY (`id_cliente`) REFERENCES `clientes` (`id_cliente`) ON DELETE CASCADE;

--
-- Filtros para la tabla `productos`
--
ALTER TABLE `productos`
  ADD CONSTRAINT `PROD_FK` FOREIGN KEY (`id_categoria`) REFERENCES `categorias` (`id_categoria`);

--
-- Filtros para la tabla `productos_pedidos`
--
ALTER TABLE `productos_pedidos`
  ADD CONSTRAINT `PRODPED_FK` FOREIGN KEY (`id_producto`) REFERENCES `productos` (`id_producto`) ON DELETE CASCADE,
  ADD CONSTRAINT `productos_pedidos_ibfk_1` FOREIGN KEY (`id_pedido`) REFERENCES `pedidos` (`id_pedido`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
