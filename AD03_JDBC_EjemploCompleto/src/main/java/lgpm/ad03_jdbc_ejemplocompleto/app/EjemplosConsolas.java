/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lgpm.ad03_jdbc_ejemplocompleto.app;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import lgpm.ad03_jdbc_ejemplocompleto.model.dao.Consultor;
import lgpm.ad03_jdbc_ejemplocompleto.model.dao.DbConnection;
import lgpm.ad03_jdbc_ejemplocompleto.model.dao.ProductConsultor;
import lgpm.ad03_jdbc_ejemplocompleto.model.dto.Product;

/**
 *
 * @author chachy
 */
public class EjemplosConsolas {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Connection connectionTransactional = null;
        try {
            connectionTransactional = DbConnection.getDbConnection();
            if (connectionTransactional.getAutoCommit()) {
                connectionTransactional.setAutoCommit(false);
            }
            
            Consultor productConsultor = new ProductConsultor(connectionTransactional);
            
            // Ejemplo de select
            List<Product> productsSelected = productConsultor.selectProducts();
            System.out.println("\nLista de productos seleccionados (5 primeros)");
            System.out.println("\n" + "=".repeat(40) + "\n");
            productsSelected.forEach(product -> System.out.println(product));
            System.out.println("\n" + "=".repeat(40));
            
            // Ejemplo de insert
            
            // Fallo en el inset nombre muy grande, para probrar entrada en rollback
//            Product productToInsert = new Product(
//                    "Nombre Prueba 1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111", 
//                    "Descripción de prueba", 0.0, 
//                    "pathdelficheroimagen.jpg", 1);
            // Inset sin fallo
            Product productToInsert = new Product(
                    "Nombre Prueba", "Descripción de prueba", 0.0, 
                    "pathdelficheroimagen.jpg", 1);
            int numRowsInserted = productConsultor.insertProduct(productToInsert);
            System.out.println("\nSe insertaron " + numRowsInserted + " lineas");
            System.out.println("\n" + "=".repeat(40));
            
            
            // Ejemplo de Update
            Product productToUpdate = new Product(
                    125, "Nombre Prueba Updated", "Descripción de prueba Updated", 
                    0.0, "pathdelficheroimagen.jpg", 1);
            int numRowsUpdated = productConsultor.updateProduct(productToUpdate);
            System.out.println("\nSe actualizaron " + numRowsUpdated + " lineas");
            System.out.println("\n" + "=".repeat(40));
            
            // Ejemplo de Delete
            Product productToDelete = new Product(
                    125, "Nombre Prueba Updated", "Descripción de prueba Updated", 
                    0.0, "pathdelficheroimagen.jpg", 1);
            int numRowsDeleted = productConsultor.deleteProduct(productToDelete);
            System.out.println("\nSe eliminaron " + numRowsDeleted + " lineas");
            System.out.println("\n" + "=".repeat(40));
            
            System.out.println("Realizando commit de la transacción...");
            connectionTransactional.commit();
            System.out.println("Terminado el commit de la transacción");
            
            if (connectionTransactional != null) {
                DbConnection.close(connectionTransactional);
            }
            
        } catch (SQLException sqlEx) {
            System.out.println("*** ERROR *** => " + sqlEx.getMessage());
            System.out.println("Entramos en rollback...");
            try {
                connectionTransactional.rollback();
                System.out.println("Finalizado rollback");
            } catch (SQLException sqlExRoll) {
                System.out.println("*** ERROR *** Falló el rollback => " + sqlExRoll.getMessage());
            } 
            
        } 
    }
    
}
