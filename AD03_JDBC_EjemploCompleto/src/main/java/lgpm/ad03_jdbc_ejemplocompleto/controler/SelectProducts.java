/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lgpm.ad03_jdbc_ejemplocompleto.controler;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import lgpm.ad03_jdbc_ejemplocompleto.model.dao.Consultor;
import lgpm.ad03_jdbc_ejemplocompleto.model.dao.ProductConsultor;
import lgpm.ad03_jdbc_ejemplocompleto.model.dto.Category;
import lgpm.ad03_jdbc_ejemplocompleto.model.dto.Product;

/**
 *
 * @author chachy
 */
public class SelectProducts {
    
    public static final String SQL_SELECT_PRODUCT = "SELECT id_producto, nombre, descripcion, precio, imagen, id_categoria FROM productos WHERE ";
    public static final String SQL_SELECT_CATEGORY = "SELECT id_categoria, nombre_it, nombre_es FROM categorias WHERE nombre_it=?";
    
    public static List<Product> getProductsSelection(String strCategory, double price) {
        List<Product> listProductsSelected = new ArrayList<>();
        Category category = ProductCategory.getCategoryForName(strCategory);
        String strWhereClause = strCategory.equalsIgnoreCase("Todas") ? 
                "" + "precio >= " + price : 
                "id_categoria = " + category.getIdCategory() + " AND precio >= " + price;
        String strSqlSelectProduct = SQL_SELECT_PRODUCT + strWhereClause;
        
        Consultor consultorProductos = new ProductConsultor();
        try {
            listProductsSelected = consultorProductos.selectProducts(strSqlSelectProduct);
        } catch (SQLException sqlEx) {
            System.out.println("*** ERROR *** => " + sqlEx.getMessage());
        }
        
        return listProductsSelected;
    }
    
    
}
