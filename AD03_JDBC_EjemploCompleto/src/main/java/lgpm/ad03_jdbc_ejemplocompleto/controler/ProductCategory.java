/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lgpm.ad03_jdbc_ejemplocompleto.controler;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import lgpm.ad03_jdbc_ejemplocompleto.model.dao.DbConnection;
import lgpm.ad03_jdbc_ejemplocompleto.model.dto.Category;

/**
 *
 * @author chachy
 */
public class ProductCategory {
    private static final String SQL_SELECT_CATEGORIES = "SELECT id_categoria, nombre_it, nombre_es FROM categorias";
    
    
    public static List<Category> getListCategories() {
        List<Category> listCategories = new ArrayList<>();
        Connection dbConnection = null;
        try {
            dbConnection = DbConnection.getDbConnection();
            Statement sqlStatement = dbConnection.createStatement();
            ResultSet categoriesResultSet = sqlStatement.executeQuery(SQL_SELECT_CATEGORIES);
            
            while(categoriesResultSet.next()) {
                int idCategory = categoriesResultSet.getInt("id_categoria");
                String nameIt = categoriesResultSet.getString("nombre_it");
                String nameEs = categoriesResultSet.getString("nombre_es");
                
                Category category = new Category(idCategory, nameIt, nameEs);
                
                listCategories.add(category);
            }
            DbConnection.close(categoriesResultSet);
            DbConnection.close(sqlStatement);
            DbConnection.close(dbConnection);
            
        } catch (SQLException sqlEx) {
            System.out.println("*** ERROR *** => " + sqlEx.getMessage());
        }
        
        return listCategories;
    }
    
    public static Category getCategoryForId(int idCategory) {
        Category categoryToReturn = null;
        for (Category category : getListCategories()) {
            if (category.getIdCategory() == idCategory) {
                categoryToReturn = category;
                break;
            }
        }
        return categoryToReturn;
    }
    
    public static Category getCategoryForName(String name) {
        Category categoryToReturn = null;
        for (Category category : getListCategories()) {
            if (category.getNameIt().equalsIgnoreCase(name) || 
                    category.getNameEs().equalsIgnoreCase(name)) {
                categoryToReturn = category;
                break;
            }
        }
        return categoryToReturn;
    }
}
