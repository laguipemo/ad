/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lgpm.ad03_jdbc_ejemplocompleto.model.dao;

import java.sql.SQLException;
import java.util.List;
import lgpm.ad03_jdbc_ejemplocompleto.model.dto.Product;

/**
 *
 * @author chachy
 */
public interface Consultor {
    
    
    public List<Product> selectProducts() throws SQLException;
    public List<Product> selectProducts(String sqlSelect) throws SQLException;
    
    public int insertProduct(Product product) throws SQLException;
    public int insertProduct(String sqlInsert, Product product) throws SQLException;
    
    public int updateProduct(Product product) throws SQLException;
    public int updateProduct(String sqlUpdate, Product product) throws SQLException;
    
    public  int deleteProduct(Product product) throws SQLException;
    public  int deleteProduct(String sqlDelete, Product product) throws SQLException;
    
}
