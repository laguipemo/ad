/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lgpm.ad03_jdbc_ejemplocompleto.model.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author chachy
 */
public class DbConnection {
    private static final String JDBC_URL = "jdbc:mysql://localhost:3306/restaurante?useSSL=false&serverTimezone=UTC";
    private static final String JDBC_USER = "root";
    private static final String JDBC_PASS = "";

    public static Connection getDbConnection() throws SQLException {
        return DriverManager.getConnection(JDBC_URL, JDBC_USER, JDBC_PASS);
    }
    
    public static void close(Connection dbConnection) throws SQLException {
        dbConnection.close();
    }
    public static void close(Statement sqlStatement) throws SQLException {
        sqlStatement.close();
    }
    public static void close(ResultSet resultSet) throws SQLException {
        resultSet.close();
    }
    
    
}
