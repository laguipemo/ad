/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lgpm.ad03_jdbc_ejemplocompleto.model.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import lgpm.ad03_jdbc_ejemplocompleto.model.dto.Product;

/**
 *
 * @author chachy
 */
public class ProductConsultor implements Consultor {
    
    private static final String SQL_SELECT = 
            "SELECT id_producto, nombre, descripcion, precio, imagen, id_categoria " +
            "FROM productos LIMIT 5";
    
    private static final String SQL_INSERT = 
            "INSERT INTO productos " +
            "(nombre, descripcion, precio, imagen, id_categoria) " +
            "VALUES (?, ?, ?, ?, ?)";
    
    private static final String SQL_UPDATE = 
            "UPDATE productos " +
            "SET nombre=?, descripcion=?, precio=?, imagen=?, id_categoria=? " + 
            "WHERE id_producto=?";
    
    private static final String SQL_DELETE = 
            "DELETE FROM productos WHERE id_producto=?";
    
    private Connection connectionTransactional;

    
    public ProductConsultor() {
    }
    
    public ProductConsultor(Connection connetionTransactional) {
        this.connectionTransactional = connectionTransactional;
    }

    
    @Override
    public List<Product> selectProducts() throws SQLException {
        return selectProducts(SQL_SELECT);
    }

    @Override
    public List<Product> selectProducts(String sqlSelect) throws SQLException {
        List<Product> productsSelected = new ArrayList<>();
        
        Connection dbConnection = this.connectionTransactional != null ? 
                this.connectionTransactional : DbConnection.getDbConnection();
        
        Statement sqlStatement = dbConnection.createStatement();
        ResultSet resultSet = sqlStatement.executeQuery(sqlSelect);
        
        while (resultSet.next()) {
            int idProduct = resultSet.getInt("id_producto");
            String name = resultSet.getString("nombre");
            String description = resultSet.getString("descripcion");
            double price = resultSet.getDouble("precio");
            String strImage = resultSet.getString("imagen");
            int idCategory = resultSet.getInt("id_categoria");
            
            Product product = new Product(
                    idProduct, name, description, price, strImage, idCategory);
            productsSelected.add(product);
            
        }
        DbConnection.close(resultSet);
        DbConnection.close(sqlStatement);
        if (this.connectionTransactional == null) {
            DbConnection.close(dbConnection);
        } 
        
        return productsSelected;
    }

    @Override
    public int insertProduct(Product product) throws SQLException {
        return insertProduct(SQL_INSERT, product);
    }

    @Override
    public int insertProduct(String sqlInsert, Product product) throws SQLException {
        int numRowsInserted = 0;
        
        Connection dbConnection = this.connectionTransactional != null ? 
                this.connectionTransactional : DbConnection.getDbConnection();
        
        PreparedStatement sqlPreparedStatement = dbConnection.prepareStatement(sqlInsert);
        sqlPreparedStatement.setString(1, product.getName());
        sqlPreparedStatement.setString(2, product.getDescription());
        sqlPreparedStatement.setDouble(3, product.getPrice());
        sqlPreparedStatement.setString(4, product.getStrImage());
        sqlPreparedStatement.setInt(5, product.getIdCategory());
        
        numRowsInserted = sqlPreparedStatement.executeUpdate();
        
        DbConnection.close(sqlPreparedStatement);
        if (this.connectionTransactional == null) {
            DbConnection.close(dbConnection);
        }
        
        return numRowsInserted;
    }

    @Override
    public int updateProduct(Product product) throws SQLException {
        return updateProduct(SQL_UPDATE, product);
    }

    @Override
    public int updateProduct(String sqlUpdate, Product product) throws SQLException {
        int numRowsUpdated = 0;
        
        Connection dbConnection = this.connectionTransactional != null ? 
                this.connectionTransactional : DbConnection.getDbConnection();
        
        PreparedStatement sqlPreparedStatement = dbConnection.prepareStatement(sqlUpdate);
        sqlPreparedStatement.setString(1, product.getName());
        sqlPreparedStatement.setString(2, product.getDescription());
        sqlPreparedStatement.setDouble(3, product.getPrice());
        sqlPreparedStatement.setString(4, product.getStrImage());
        sqlPreparedStatement.setInt(5, product.getIdCategory());
        sqlPreparedStatement.setInt(6, product.getIdProduct());
        
        numRowsUpdated = sqlPreparedStatement.executeUpdate();
        
        DbConnection.close(sqlPreparedStatement);
        if (this.connectionTransactional == null) {
            DbConnection.close(dbConnection);
        }
        
        return numRowsUpdated;
    }

    @Override
    public int deleteProduct(Product product) throws SQLException {
        return deleteProduct(SQL_DELETE, product);
    }

    @Override
    public int deleteProduct(String sqlDelete, Product product) throws SQLException {
        int numRowsDeleted = 0;
        
        Connection dbConnection = this.connectionTransactional != null ? 
                this.connectionTransactional : DbConnection.getDbConnection();
        
        PreparedStatement sqlPreparedStatement = dbConnection.prepareStatement(sqlDelete);
        sqlPreparedStatement.setInt(1, product.getIdProduct());
        
        numRowsDeleted = sqlPreparedStatement.executeUpdate();
        
        DbConnection.close(sqlPreparedStatement);
        if (this.connectionTransactional == null) {
            DbConnection.close(dbConnection);
        }
        
        return numRowsDeleted;
    }
    
}
