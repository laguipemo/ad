/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lgpm.ad03_jdbc_ejemplocompleto.model.dto;

/**
 *
 * @author chachy
 */
public class Category {
    private int idCategory;
    private String nameIt;
    private String nameEs;

    public Category() {
    }

    public Category(String nameIt, String nameEs) {
        this.nameIt = nameIt;
        this.nameEs = nameEs;
    }

    
    public Category(int idCategory, String nameIt, String nameEs) {
        this(nameIt, nameEs);
        this.idCategory = idCategory;
    }

    public int getIdCategory() {
        return idCategory;
    }

    public void setIdCategory(int idCategory) {
        this.idCategory = idCategory;
    }

    public String getNameIt() {
        return nameIt;
    }

    public void setNameIt(String nameIt) {
        this.nameIt = nameIt;
    }

    public String getNameEs() {
        return nameEs;
    }

    public void setNameEs(String nameEs) {
        this.nameEs = nameEs;
    }

    @Override
    public String toString() {
        return "Category{" + "idCategory=" + idCategory + ", nameIt=" + nameIt + ", nameEs=" + nameEs + '}';
    }
    
    
}
