/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lgpm.ad03_jdbc_ejemplocompleto.model.dto;

/**
 *
 * @author chachy
 */
public class Product {
    private int idProduct;
    private String name;
    private String description;
    private double price;
    private String strImage;
    private int idCategory;

    public Product() {
    }

    public Product(String name, String description, double price, String strImage, int idCategory) {
        this.name = name;
        this.description = description;
        this.price = price;
        this.strImage = strImage;
        this.idCategory = idCategory;
    }

    public Product(int idProduct, String name, String description, double price, String strImage, int idCategory) {
        this(name, description, price, strImage, idCategory);
        this.idProduct = idProduct;
    }

    public int getIdProduct() {
        return idProduct;
    }

    public void setIdProduct(int idProduct) {
        this.idProduct = idProduct;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getStrImage() {
        return strImage;
    }

    public void setStrImage(String strImage) {
        this.strImage = strImage;
    }

    public int getIdCategory() {
        return idCategory;
    }

    public void setIdCategory(int idCategory) {
        this.idCategory = idCategory;
    }

    @Override
    public String toString() {
        return "Product{" + "idProduct=" + idProduct + ", name=" + name + 
                ", description=" + description + ", price=" + price + 
                ", strImage=" + strImage + ", idCategory=" + idCategory + '}';
    }
    
    
    
}
