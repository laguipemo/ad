/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lgpm.ad02_tarea_a02;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;

/**
 *
 * @author lgpmc
 */
public class CrearFichero {
    private static final int LARGO_NOMBRE = 15; // 15 caracteres -> 30bytes
    private static final int LARGO_LOCALIDAD = 25; // 25 caracteres -> 50bytes
    private static final long BYTES_REGISTRO = 88L; 
    
    private final String strFile;
    
    public CrearFichero() {
        this("Departamentos.dat");
    }

    public CrearFichero(String strFile) {
        this.strFile = strFile;
        
        int[] numerosDptos = {3, 2, 1, 5, 4};
        String[] nombresDptos = {"INFORMÁTICA", 
                                 "INGLÉS", 
                                 "MATEMÁTICAS", 
                                 "FÍSICA", 
                                 "QUÍMICA"};
        String[] localidadesDptos = {"FACULTAD INFORMÁTICA", 
                                     "FACULTAD LENGUAS", 
                                     "FACULTAD MATEMÁTICAS", 
                                     "FACULTAD FÍSICA", 
                                     "FACULTAD QUÍMICA"};
        this.introducirDatos(numerosDptos, nombresDptos, localidadesDptos);
    }

    

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        CrearFichero crearFichero = null;
        //comprobar si el usuario un nombre para el fichero
        if (args.length < 1) {
            crearFichero = new CrearFichero();
        } else { //recoger el nombre del fichero en la primera posición de args
            String strFile = args[0];
            crearFichero = new CrearFichero(strFile);
        }
        //comprobar la creación
        if (crearFichero != null) {
            crearFichero.leerFichero();
        }
    }

    private void introducirDatos(int[] numerosDptos, String[] nombresDptos, String[] localidadesDptos) {
        try (RandomAccessFile randomFile = new RandomAccessFile(this.strFile, "rw")) {
            randomFile.seek(0); //posiciono en inicio fichero
            for (int i = 0; i < numerosDptos.length; i++) {
                randomFile.writeInt(i + 1); //indice del registro
                randomFile.writeInt(numerosDptos[i]); //número del departamento
                //nombre del departamento
                StringBuffer bufferNombre = new StringBuffer(nombresDptos[i]);
                bufferNombre.setLength(LARGO_NOMBRE);
                randomFile.writeChars(bufferNombre.toString());
                //localidad del departamento
                StringBuffer bufferLocalidad = new StringBuffer(localidadesDptos[i]);
                bufferLocalidad.setLength(LARGO_LOCALIDAD);
                randomFile.writeChars(bufferLocalidad.toString());
            }
        } catch (FileNotFoundException fnfEx) {
            fnfEx.printStackTrace();
        } catch (IOException ioEx) {
            ioEx.printStackTrace();
        }
    }

    private void leerFichero() {
        System.out.println("\n" + "=".repeat(60));
        System.out.println("Comprobación, mostrando contenido del fichero creado");
        System.out.println("=".repeat(60) + "\n");
        
        try (RandomAccessFile randomFile = new RandomAccessFile(this.strFile, "r")){
            long posicion = 0;
            char[] nombre = new char[LARGO_NOMBRE];
            char[] localidad = new char[LARGO_LOCALIDAD];
            while (posicion < randomFile.length()) {
                randomFile.seek(posicion);
                System.out.println("Indice: " + randomFile.readInt());
                System.out.println("Numero Departamento: " + randomFile.readInt());
                for (int i = 0; i < LARGO_NOMBRE; i++) {
                    nombre[i] = randomFile.readChar();
                }
                System.out.println("Nombre Departamento: " + new String(nombre));
                for (int i=0; i < LARGO_LOCALIDAD; i++) {
                    localidad[i] = randomFile.readChar();
                }
                System.out.println("Nombre Localidad: " + new String(localidad));
                posicion += BYTES_REGISTRO;
                System.out.println("");
            }
        } catch (FileNotFoundException fnfEx) {
            fnfEx.printStackTrace();
        } catch (IOException ioEx) {
            ioEx.printStackTrace();
        }
        
    }
    
}
