/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lgpm.ad02_tarea_a01;

import java.io.File;

/**
 *
 * @author lgpmc
 */
public class EliminarDirectorio {
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        //comprobar si el usuario pasó el nombre del directorio
        if (args.length < 1) {
            System.out.println("Se necesita el direcorio a borrar");
            System.exit(0);
        }
        //recoger el nombre del directorio en la primera posición de args
        String strDir = args[0];
        File directorio = new File(strDir);
        //comprobar si existe el objeto tipo File con el nombre pasado
        if (!directorio.exists()) {
            System.out.println("El directorio no existe");
            System.exit(0);
        }
        //comprobar si es un directorio o no
        if (!directorio.isDirectory()) {
            System.out.println("El nombre indicado no corresponde a un directorio");
            System.exit(0);
        }
        //Si el directori está vacío se borra diectamente
        if (directorio.list().length < 1) {
            try {
                if (directorio.delete()) {
                    System.out.println("El direcorio se ha eliminado satisfactoriamente");
                    System.exit(0);
                } else {
                    System.out.println("El directorio no se pudo eliminar");
                    System.exit(0);
                }
            } catch (SecurityException secEx) {
                System.out.println("No tiene permisos para borrar el directorio");
            }
        }
        //el directio no está vacío y hay que vaciarlo recursivamente antes de borrarlo
        try {
            if (borrarDirRecursivo(directorio)) {
                System.out.println("El directorio se ha borrado con éxito");
            } else {
                System.out.println("No se pudo borrar el directorio");
            }
        } catch (SecurityException secEx) {
            System.out.println("No tiene permisos para borrar el directorio o alguno de sus elementos");
        }
        
        
    }

    private static boolean borrarDirRecursivo(File directorio) {
        if (directorio.list() != null) { //el directorio no esta vacío
            //llamar recursivamente la función para cada elemento del directorio
            for(File elemento : directorio.listFiles()) {
                borrarDirRecursivo(elemento);
            }
        }
        //el directorio está vacío lo borro retornando resultado
        return directorio.delete();
    }
    
}
