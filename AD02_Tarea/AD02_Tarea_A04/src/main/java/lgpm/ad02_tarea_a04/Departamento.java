/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lgpm.ad02_tarea_a04;

/**
 *
 * @author lgpmc
 */
public class Departamento {
    private int numeroDpto;
    private String nombreDpto;
    private String localidadDpto;

    public Departamento() {
    }

    public Departamento(int numeroDpto, String nombreDpto, String localidadDpto) {
        this.numeroDpto = numeroDpto;
        this.nombreDpto = nombreDpto;
        this.localidadDpto = localidadDpto;
    }

    public int getNumeroDpto() {
        return numeroDpto;
    }

    public void setNumeroDpto(int numeroDpto) {
        this.numeroDpto = numeroDpto;
    }

    public String getNombreDpto() {
        return nombreDpto;
    }

    public void setNombreDpto(String nombreDpto) {
        this.nombreDpto = nombreDpto;
    }

    public String getLocalidadDpto() {
        return localidadDpto;
    }

    public void setLocalidadDpto(String localidadDpto) {
        this.localidadDpto = localidadDpto;
    }
    
    
}
