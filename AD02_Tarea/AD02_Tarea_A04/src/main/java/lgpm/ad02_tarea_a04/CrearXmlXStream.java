/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lgpm.ad02_tarea_a04;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.RandomAccessFile;

/**
 *
 * @author lgpmc
 */
public class CrearXmlXStream {

    private static final int LARGO_NOMBRE = 15; // 15 caracteres -> 30bytes
    private static final int LARGO_LOCALIDAD = 25; // 25 caracteres -> 50bytes
    private static final long LONG_REGISTRO = 88L;

    private String strFile;
    private ListaDepartamentos listaDptos = new ListaDepartamentos();
    private XStream xstream;

    public CrearXmlXStream() {
        this("Departamentos.dat");
    }

    public CrearXmlXStream(String strFile) {
        this.strFile = strFile;
        initListDepartamentos();
        initXstream();
    }

    private void initListDepartamentos() {
        //leeo los datos del fichero y pueblo la lista de listaDptos
        int numeroDpto;
        String nombreDpto;
        String localidadDpto;
        try (RandomAccessFile randomFile = new RandomAccessFile(this.strFile, "r")) {
            long posicion = 0;
            while (posicion < randomFile.length()) {
                randomFile.seek(posicion);
                if (randomFile.readInt() == -1) { // registro borrado lógico
                    posicion += LONG_REGISTRO;
                }
                // numero del departamento
                numeroDpto = randomFile.readInt();
                // nombre del departamento
                char[] charsNombre = new char[LARGO_NOMBRE];
                for (int i=0; i<LARGO_NOMBRE; i++) {
                    charsNombre[i] = randomFile.readChar();
                }
                nombreDpto = new String(charsNombre).trim();
                // localidad del departamento
                char[] charsLocalidad = new char[LARGO_LOCALIDAD];
                for (int i=0; i<LARGO_LOCALIDAD; i++) {
                    charsLocalidad[i] = randomFile.readChar();
                }
                localidadDpto = new String(charsLocalidad).trim();
                // añado un nuevo objeto tipo Departamento con los datos leidos
                listaDptos.addDepartamento(new Departamento(numeroDpto, nombreDpto, localidadDpto));
                
                posicion += LONG_REGISTRO;
            }
        } catch (FileNotFoundException fnfEx) {
            fnfEx.printStackTrace();
        } catch (IOException ioEx) {
            ioEx.printStackTrace();
        }
    }

    private void initXstream() {
        try {
            xstream = new XStream(new DomDriver("UTF-8"));
            //creo los alias para que las etiquetas del xml sean más intuitivas
            //El ejemplar es de la clase ListaDepartamentos y lo llamaré listaDptos
            xstream.alias("departamentos", ListaDepartamentos.class);
            //Los elementos son de la clase Departamento y los llamaré departamento  
            xstream.alias("departamento", Departamento.class);
            //creo alias para los campos de cada departamento
            //elemento de tipo campo numeroDpto de la clase Departamento que llamaré numero
            xstream.aliasField("numero", Departamento.class, "numeroDpto");
            //elemento de tipo campo nombreDpto de la clase Departamento que llamaré nombre
            xstream.aliasField("nombre", Departamento.class, "nombreDpto");
            //elemento de tipo campo localidadDpto de la clase Departamento que llamaré localidad
            xstream.aliasField("localidad", Departamento.class, "localidadDpto");
            //añado los elementos de la clase Departamento almacenados en el campo listaDptos de la
            //clase ListaDepartamentos, como hijos del ejemplar sin utilizar el nombre del campo
            //como etiqueta envoltoria
            xstream.addImplicitCollection(ListaDepartamentos.class, "listaDepartamentos");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    
    private void create() {
        //guardar en fichero
        String strFileXml = this.strFile.split("\\.")[0] + ".xml";
        try {
            this.xstream.toXML(listaDptos, new FileOutputStream(strFileXml));
        } catch (FileNotFoundException fnfEx) {
            fnfEx.printStackTrace();
        }
    }
    
    private void show() {
        //mostrar por salida estándar
        xstream.toXML(listaDptos, System.out);
            
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        CrearXmlXStream xml = null;
        //comprobar si el usuario un nombre para el fichero
        if (args.length < 1) {
            xml = new CrearXmlXStream();
        } else { //recoger el nombre del fichero en la primera posición de args
            String strFile = args[0];
            xml = new CrearXmlXStream(strFile);
        }
        //comprobar la creación
        if (xml != null) {
            xml.create();
            //comprobación, mostrar por consola el contenido del fichero
            xml.show();
        }
    }
}
