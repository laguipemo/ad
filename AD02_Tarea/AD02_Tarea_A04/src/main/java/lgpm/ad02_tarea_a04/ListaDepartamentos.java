/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lgpm.ad02_tarea_a04;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author lgpmc
 */
public class ListaDepartamentos {
    
    private List<Departamento> listaDepartamentos = new ArrayList<>();

    public ListaDepartamentos() {
    }

    public List<Departamento> getListaDepartamentos() {
        return listaDepartamentos;
    }

    public void setListaDepartamentos(List<Departamento> listaDepartamentos) {
        this.listaDepartamentos = listaDepartamentos;
    }
    
    public void addDepartamento(Departamento departamento) {
        listaDepartamentos.add(departamento);
    }
    
}
