/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lgpm.ad02_tarea_a03;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;

/**
 *
 * @author lgpmc
 */
public class EliminarDepartamento {
    private static final int LARGO_NOMBRE = 15; // 15 caracteres -> 30bytes
    private static final int LARGO_LOCALIDAD = 25; // 25 caracteres -> 50bytes
    private static final long BYTES_REGISTRO = 88L;
    
    private final String strFile;
    private final int numeroDpto;

    public EliminarDepartamento(int numeroDpto) {
        this("Departamentos.dat", numeroDpto);
    }

    public EliminarDepartamento(String strFile, int numeroDpto) {
        this.strFile = strFile;
        this.numeroDpto = numeroDpto;
        eleminarDepartamento();
        
    }

    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        EliminarDepartamento eliminarDepartamento = null;
        //comprobar si el usuario introdujo algún parámetro
        if (args.length < 1) {
            System.out.println("Se necesita al menos el número del Departamento a eliminar");
            System.exit(0);
        } else if (args.length == 1){ //introduce un único parámeto, número dpto
            try {
                int numeroDpto = Integer.parseInt(args[0]);
                if (!(new File("Departamentos.dat").exists())) {
                    System.out.println("No existe el fichero Departamentos.dat");
                    System.exit(0);
                }
                eliminarDepartamento = new EliminarDepartamento(numeroDpto);
            } catch (NumberFormatException nfEx) {
                System.out.println("Un parámetro único, tiene que ser Int > 0");
            }
        } else {
            String strFile = args[0];
            if (!(new File(strFile).exists())) {
                System.out.println("No existe el fichero " + strFile);
                System.exit(0);
            }
            try {
                int numeroDpto = Integer.parseInt(args[1]);
                eliminarDepartamento = new EliminarDepartamento(strFile, numeroDpto);
            } catch (NumberFormatException nfEx) {
                System.out.println("El segundo parámetro, tiene que ser Int > 0");
            }
        }
        
    }

    private void eleminarDepartamento() {
        int numeroRegistros;
        try (RandomAccessFile randomFile = new RandomAccessFile(this.strFile, "rw")) {
            
            //busco departamamento con número deseado
            long posicion = buscarDptoNumero(randomFile, this.numeroDpto);
            if (posicion == -1) { //departamento no existe
                System.out.println("No existe ningún Departamento con ese número");
                numeroRegistros = getNumeroRegistros(randomFile);
                System.out.println("Existen " + numeroRegistros + " departamentos");
                System.exit(0);
            }
            randomFile.seek(posicion);
            randomFile.writeInt(-1); // bandera borrado lógico
//            randomFile.writeInt(0);
//            randomFile.writeChars(" ".repeat(LARGO_NOMBRE));
//            randomFile.writeChars(" ".repeat(LARGO_LOCALIDAD));
            
            numeroRegistros = getNumeroRegistros(randomFile);
            System.out.println("Existen " + numeroRegistros + " departamentos");
            
        } catch (FileNotFoundException fnfEx) {
            fnfEx.printStackTrace();
        } catch (IOException ioEx) {
            ioEx.printStackTrace();
        }
        
    }

    private long buscarDptoNumero(RandomAccessFile randomFile, int numeroDpto) {
        long pos = 0;
        try {
            while (pos < randomFile.length()) {
                randomFile.seek(pos);
                if (randomFile.readInt() == -1) { //dpto borrado lógicamente
                    pos += BYTES_REGISTRO;
                    continue;
                } 
                if (randomFile.readInt() == numeroDpto) { // dpto buscado
                    return pos;
                }
                pos += BYTES_REGISTRO; //avanzo al proximo registro
            }
        } catch (IOException ioEx) {
            ioEx.printStackTrace();
        }
        return -1; // no se encontró departamento con ese número
    }

    private int getNumeroRegistros(RandomAccessFile randomFile) {
        long pos = 0;
        int cont = 0;
        try {
            while (pos < randomFile.length()) {
                randomFile.seek(pos);
                if (randomFile.readInt() != -1) { // dpto no borrado
                    cont++;
                }
                pos += BYTES_REGISTRO; // avanzo al proximo registro
            }
        } catch (IOException ioEx) {
            ioEx.printStackTrace();
        }
        return cont;
    }
    
}
