/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aplicacion;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.math.BigDecimal;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeConstants;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import jaxb.albaran.Articulos;
import jaxb.albaran.Articulos.Articulo;
import jaxb.albaran.Direccion;
import jaxb.albaran.PedidoType;

/**
 *
 * @author lgpmc
 */
public class AD02_ModificaAlbaranPed {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        try {
            JAXBContext contextoAlbaran = JAXBContext.newInstance("jaxb.albaran");
            Unmarshaller unmashAlbaran = contextoAlbaran.createUnmarshaller();
            JAXBElement jaxbElementAlbaran = (JAXBElement) unmashAlbaran.unmarshal(
                    new FileInputStream(new File("mi_albaran.xml")));
            
            PedidoType pedidoType = (PedidoType) jaxbElementAlbaran.getValue();
            
            Direccion direccionFacturacion = pedidoType.getFacturarA();
            // leer contido del fichero xml
            leerContenido(direccionFacturacion, pedidoType);
            //cambiar datos de facturaci�n
            changeDireccionFacturacion(direccionFacturacion);
            
            Articulo articulo = createArticuloPedido("Almax", "111-AL", 1, new BigDecimal("1.35"), 2020, 12, 4);
            
            pedidoType.getArticulos().getArticulo().add(articulo);
            
            Marshaller marshAlbaran = contextoAlbaran.createMarshaller();
            marshAlbaran.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
            
            marshAlbaran.marshal(jaxbElementAlbaran, System.out);
            marshAlbaran.marshal(jaxbElementAlbaran, 
                    new FileOutputStream(new File("mi_albaran2.xml"), false));
            
        } catch (JAXBException jaxbEx) {
            System.out.println(jaxbEx.getMessage());
        } catch (FileNotFoundException fnfEx) {
            System.out.println(fnfEx.getMessage());
        } catch (DatatypeConfigurationException datatypeconfEx) {
            System.out.println(datatypeconfEx.getMessage());
        }
                
    }

    private static Articulo createArticuloPedido(
            String nombreProducto, String codProducto, int cantidad, BigDecimal precio, 
            int year, int month, int day) throws DatatypeConfigurationException {
        Articulo articulo = new Articulo();
        articulo.setNombreProducto(nombreProducto);
        articulo.setCodigo(codProducto);
        articulo.setCantidad(cantidad);
        articulo.setPrecio(precio);
        XMLGregorianCalendar fechaEnvio = DatatypeFactory.newInstance()
                .newXMLGregorianCalendarDate(year, month, day, DatatypeConstants.FIELD_UNDEFINED);
        articulo.setFechaEnvio(fechaEnvio);
        return articulo;
    }

    private static void changeDireccionFacturacion(Direccion direccionFacturacion) {
        //modificar direcci�n de facturaci�n
        direccionFacturacion.setNombre("Jos� Javier");
        direccionFacturacion.setCalle("Zafiro 3");
        direccionFacturacion.setCiudad("Molina");
        direccionFacturacion.setProvincia("Murcia");
        direccionFacturacion.setCodigoPostal(new BigDecimal("30500"));
    }

    private static void leerContenido(Direccion direccionFacturacion, PedidoType pedidoType) {
        String nombreFarmacia = direccionFacturacion.getNombre();
        String calle = direccionFacturacion.getCalle();
        String ciudad = direccionFacturacion.getCiudad();
        String provincia = direccionFacturacion.getProvincia();
        BigDecimal codPostal = direccionFacturacion.getCodigoPostal();
        
        System.out.printf(
                "Farmacia: %s%n Calle: %s%n Ciudad: %s%n Provincia: %s%n Codigo Postal: %s%n",
                nombreFarmacia, calle, ciudad, provincia, codPostal.toString());
        
        String comentarioPedido = pedidoType.getComentario();
        System.out.printf("%s%n", comentarioPedido);
        
        Articulos articulosAlbaran = pedidoType.getArticulos();
        for (Articulo articulo: articulosAlbaran.getArticulo()) {
            System.out.println("Estoy aqu�");
            String nombreProducto = articulo.getNombreProducto();
            String codProducto = articulo.getCodigo();
            int cantidad = articulo.getCantidad();
            BigDecimal precio = articulo.getPrecio();
            String comentario = articulo.getComentario();
            XMLGregorianCalendar fechaEnvio = articulo.getFechaEnvio();
            
            System.out.printf(
                    "Producto: %s%n C�digo: %s%n Cantidad: %d%n Precio: %.2f%n Comentario: %s%n Fecha envio: %s%n",
                    nombreProducto, codProducto, cantidad, precio, comentario, fechaEnvio.toString());
        }
    }
    
}
