/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lgpm.ad02_ejemplo_sax_reader;

import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.DefaultHandler;

/**
 *
 * @author lgpmc
 */
public class AD02_Ejemplo_SAX_Reader {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // creamos una instancia de la fábrica o factoría de traductores (parsers)
        // de SAX.
        SAXParserFactory saxParserFactory = SAXParserFactory.newInstance();
        try {
            // creamos nuevo traductor (parser) de SAX capturando errores
            SAXParser saxParser = saxParserFactory.newSAXParser();
            // a partir del parser obtenemos un lector SAX
            XMLReader xmlReader = saxParser.getXMLReader();
            // creamos una instacia del manejador de contendidos (ContentHandler)
            // que se encargará de manejar los eventos en los que se se basa SAX.
            // Esto es una instancia de una clase creada a tal efecto y que extiende
            // de la clase abstracta DefaultHandler
            ManejadorContenido manejadorContenido = new ManejadorContenido();
            // Asignamos el manejador de contendido definido anteriormente a nuestro 
            // lector SAX.
            xmlReader.setContentHandler(manejadorContenido);
            /* 
             A través del método parse, del lector le indicamos el fichero a leer
             existe una sobrecarga de este método, por lo que podemos utilizar la
             firma sencilla que consite en paser un String con el path del fichero
             a leer:
                xmlReader.parse("tvShows.xml");
             o la firma que utiliza un InputSource() que a su vez puede tomar un
             un String con el path del fichero:
                InputSource ficheroXML = new InputSource("tvShows.xml");
             o un InputStream o un Reader, es decir, un flujo de bytes asociado 
             al fichero:
                InputSource ficheroXML = new InputSource(new FileInputStream("tvShows.xml"));
             o un flujo de careacteres asociado al fichero:   
                InputSource ficheroXML = new InputSource(new FileReader("tvShows.xml"));
             También tenemos la variante más sencilla del InputSource en la que 
             se le pasa un String con el path del fichero:
            */
            InputSource ficheroXML = new InputSource("tvShows.xml");
            xmlReader.parse(ficheroXML);
        } catch (ParserConfigurationException parsconfEx) {
            System.out.println(parsconfEx.getMessage());
        } catch (SAXException saxEx) {
            System.out.println(saxEx.getMessage());
        } catch (IOException ioEx) {
            System.out.println(ioEx.getMessage());
        }
    }
}

class ManejadorContenido extends DefaultHandler {
    public ManejadorContenido() {
        super();
    }
    
    @Override
    public void startDocument() {
        // Imprimo a modo de comprobación que estmos al inicio del documento
        System.out.println("Comienza el Documento");
    }
    
    @Override
    public void endDocument() {
        // Imprimo a modo de comprobación que estamo al final del documento
        System.out.println("Finaliza el Documento");
    }
    
    /**
     * Método que maneja el evento lanzado cuando termina de leer un elemento del XML
     * @param uri - uri utilizaca como espacio de nombres en el XML o null
     * @param localName - nombre local sin prefijo si no se utilizó uri o null
     * @param qName - etiqueta del elemento (qualified name) sin prefijo o null
     */
    @Override
    public void endElement(String uri, String localName, String qName) {
        // ejemplo, mostrar los valores de los parámetros
//        System.out.printf("Comienza el elemento: Uri%s - localName:%s - qName:%s %n", uri, localName, qName);
        // Ejemplo, mostrar solo la etique del elemento que se está leyendo
        System.out.printf("Finaliza el elemento: %s %n", qName);
    }
    
    /**
     * Método que maneja el evento lanzado cuando se lee un elemnto de XML
     * @param uri - uri utilizaca como espacio de nombres en el XML o null
     * @param localName - nombre local sin prefijo si no se utilizó uri o null
     * @param qName - etiqueta del elemento (qualified name) sin prefijo o null
     * @param attributes - colección de atributos del elemento que se está leyendo
     */
    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) {
        // Ejemplo para mostrar valores de los parámetros
//        System.out.printf("Comienza el elemento: Uri:%s - localName:%s - qName:%s - Num attributes:%s %n", uri, localName, qName, attributes.getLength());
        // Ejemplo indicar el nombre del elemento que se está leyendo
        System.out.printf("Comienza el elemento: %s %n", qName);
        // Indicar el número de atributos definidos para este elemento
        System.out.printf("\t%s tiene %d atributos: %n", qName, attributes.getLength());
        // Si el elemento tiene atributos, mostrar su(s) nombre(s) y valor(es)
        if (attributes.getLength() != 0) {
            for (int i = 0; i < attributes.getLength(); i++) {
                System.out.printf("\t\t%s = %s %n", attributes.getQName(i), attributes.getValue(i));
            }
        }
    }

    /**
     * Método que maneja el evento lanzado cuando se lee el texto conteneido en
     * un elemento, es decir, entre las etiquetas de apertura y cierre del elemento.
     * @param ch - arreglo con los caracteres correspondientes al texto
     * @param start - posición de inicio en el arreglo (defecto 0)
     * @param length - el número de careacteres a utilizar del arreglo (defeto todo)
     */
    @Override
    public void characters(char[] ch, int start, int length) {
        // creo un string con el arreglo de carecters desde la posición inicial
        // y tomando un número de caracteres determinado. Especificamente mostraré
        // todo el texto
        String caracteres = new String(ch, start, length);
        // solo imprimo el texto si no está vacío o no está formado exclusivamente
        // por espacios en blanco.
        if (!caracteres.isBlank()) {
            System.out.println("\t"  + caracteres);
        }
    }
}