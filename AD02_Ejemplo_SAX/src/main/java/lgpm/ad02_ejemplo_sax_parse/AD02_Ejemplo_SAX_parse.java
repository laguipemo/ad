/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lgpm.ad02_ejemplo_sax_parse;

import java.io.File;
import java.io.IOException;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

/**
 *
 * @author lgpmc
 */
public class AD02_Ejemplo_SAX_parse {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        SAXParserFactory saxParserFactory = SAXParserFactory.newInstance();
        try {
            SAXParser saxParser = saxParserFactory.newSAXParser();
            ManejadorContenido manejadorContenido = new ManejadorContenido();
            // utilizamos la firma parse(File f, DefaultHandler dh)
            saxParser.parse(new File("tvShows.xml"), manejadorContenido);
            
        } catch (ParserConfigurationException parsconfEx) {
            System.out.println(parsconfEx.getMessage());
        } catch (SAXException saxEx) {
            System.out.println(saxEx.getMessage());
        } catch (IOException ioEx) {
            System.out.println(ioEx.getMessage());
        }
    }

    private static class ManejadorContenido extends DefaultHandler {

        public ManejadorContenido() {
            super();
        }

        @Override
        public void characters(char[] ch, int start, int length) throws SAXException {
            //super.characters(ch, start, length); //To change body of generated methods, choose Tools | Templates.
            String contenidoTexto = new String(ch, start, length);
            if (!contenidoTexto.isBlank()) {
                System.out.println("\tContendido:");
                System.out.println("\t\t" + contenidoTexto);
            }
        }

        @Override
        public void endElement(String uri, String localName, String qName) throws SAXException {
            //super.endElement(uri, localName, qName); //To change body of generated methods, choose Tools | Templates.
            System.out.println("Finalizado: " + qName);
        }

        @Override
        public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
            //super.startElement(uri, localName, qName, attributes); //To change body of generated methods, choose Tools | Templates.
            System.out.println("Comienza: " + qName);
            System.out.printf("\t%s tiene %s atributos:%n", qName, attributes.getLength());
            if (attributes.getLength() != 0) {
                for (int i=0; i<attributes.getLength(); i++) {
                    System.out.printf("\t\t%s: %s %n", attributes.getQName(i), attributes.getValue(i));
                }
            }
        }

        @Override
        public void endDocument() throws SAXException {
            //super.endDocument(); //To change body of generated methods, choose Tools | Templates.
            System.out.println("Finaliza el documento");
        }

        @Override
        public void startDocument() throws SAXException {
            //super.startDocument(); //To change body of generated methods, choose Tools | Templates.
            System.out.println("Comienza e documento");
        }
    }
}

