/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lgpm.ad02_ejemplo_jaxb_manual.jaxbalbaran;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 *
 * @author lgpmc
 */
@XmlType(propOrder = {"nombre", "calle", "ciudad", "provincia", "codPostal"})
public class Direccion {
    
    private String nombre;
    private String calle;
    private String ciudad;
    private String provincia;
    private BigDecimal codPostal;
    
    public Direccion() {
    }

    @XmlElement(name = "nombre")
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @XmlElement(name = "calle")
    public String getCalle() {
        return calle;
    }

    public void setCalle(String calle) {
        this.calle = calle;
    }

    @XmlElement(name = "ciudad")
    public String getCiudad() {
        return ciudad;
    }

    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }

    @XmlElement(name = "provincia")
    public String getProvincia() {
        return provincia;
    }

    public void setProvincia(String provincia) {
        this.provincia = provincia;
    }

    @XmlElement(name = "codigoPostal")
    public BigDecimal getCodPostal() {
        return codPostal;
    }

    public void setCodPostal(BigDecimal codPostal) {
        this.codPostal = codPostal;
    }

    
}
