/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lgpm.ad02_ejemplo_jaxb_manual.jaxbalbaran;


import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;

/**
 *
 * @author lgpmc
 */
@XmlType(propOrder = {"codigo", "nombreProducto", "cantidad", "precio", "comentario", "fechaEnvio"})
public class Articulo {
    
    private String codigo;
    private String nombreProducto;
    private int cantidad;
    private BigDecimal precio;
    private String comentario;
    private XMLGregorianCalendar fechaEnvio;

    public Articulo() {
    }

    @XmlAttribute(name = "codigo")
    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    @XmlElement(name = "nombreProducto")
    public String getNombreProducto() {
        return nombreProducto;
    }

    public void setNombreProducto(String nombreProducto) {
        this.nombreProducto = nombreProducto;
    }

    @XmlElement(name = "cantidad")
    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    @XmlElement(name = "precio")
    public BigDecimal getPrecio() {
        return precio;
    }

    public void setPrecio(BigDecimal precio) {
        this.precio = precio;
    }

    public String getComentario() {
        return comentario;
    }

    @XmlElement(name = "comentario")
    public void setComentario(String comentario) {
        this.comentario = comentario;
    }

    public XMLGregorianCalendar getFechaEnvio() {
        return fechaEnvio;
    }

    @XmlElement(name = "fechaEnvio")
    public void setFechaEnvio(XMLGregorianCalendar fechaEnvio) {
        this.fechaEnvio = fechaEnvio;
    }
    
    
}
