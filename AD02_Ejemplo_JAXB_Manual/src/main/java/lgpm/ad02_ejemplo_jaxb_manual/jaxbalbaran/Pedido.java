/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lgpm.ad02_ejemplo_jaxb_manual.jaxbalbaran;


import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 *
 * @author lgpmc
 */
@XmlRootElement(name="pedido")
@XmlType(propOrder = {"facturarA", "comentario", "articulos"})
public class Pedido {
    
    private Direccion facturarA;
    private String comentario;
    private List<Articulo> articulos = new ArrayList<>();

    public Pedido() {
    }

    @XmlElement(name = "facturarA")
    public Direccion getFacturarA() {
        return facturarA;
    }

    public void setFacturarA(Direccion facturarA) {
        this.facturarA = facturarA;
    }

    @XmlElement(name = "comentario")
    public String getComentario() {
        return comentario;
    }

    public void setComentario(String comentario) {
        this.comentario = comentario;
    }

    @XmlElementWrapper(name = "articulos")
    @XmlElement(name = "articulo")
    public List<Articulo> getArticulos() {
        return articulos;
    }

    public void setArticulos(List<Articulo> articulos) {
        this.articulos = articulos;
    }

        
    
}
