/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lgpm.ad02_ejemplo_jaxb_manual;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.math.BigDecimal;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeConstants;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import lgpm.ad02_ejemplo_jaxb_manual.jaxbalbaran.Articulo;
import lgpm.ad02_ejemplo_jaxb_manual.jaxbalbaran.Direccion;
import lgpm.ad02_ejemplo_jaxb_manual.jaxbalbaran.Pedido;

/**
 *
 * @author lgpmc
 */
public class AD02_ManejandoJAXB_Manual {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        try {
            // Como se está llevando a cabo manualmente, la instancia no se construye
            // pasándole el paquete con el schema .xsd. En este caso se le pasa la
            // clase que mapea el ejemplar (elemento raiz) del .xml.
            JAXBContext contextAlbaran = JAXBContext.newInstance(Pedido.class);
            // Creo el Unmarshaller que me permite leer de datos XML a árbol de
            // objetos java
            Unmarshaller unmashAlbaran = contextAlbaran.createUnmarshaller();
            // Hago la lectura del ejemplar del fichero .xml mediante el método
            // unmarshal() pasándole un flujo o stream desde el fichero. Como el
            // método devuelve un objeto genérico, tengo que hacer un casting 
            // al tipo Pedido que es la clase que corresponde al ejemplar.
            Pedido pedido = (Pedido) unmashAlbaran.unmarshal(
                    new FileInputStream(new File("mi_albaran.xml")));
            
            Direccion direccionFacturacion = pedido.getFacturarA();
            leerContenido(direccionFacturacion, pedido);
            
            //Cambiar los datos de facturación
            changeDireccionFacturacion(direccionFacturacion);
            
            //Cambia el comentario del pedido
            pedido.setComentario("Líneas pedidas por Daya");
            
            // Crear un nuevo artículo para añadirlo al pedido.
            Articulo articulo = createArticuloPedido(
                    "Losartan", "002-AA", 1, new BigDecimal("1.05"), 
                    "Para la tensión", 2020, 11, 17);
            
            pedido.getArticulos().add(articulo);
            
            Marshaller marshAlbaran = contextAlbaran.createMarshaller();
            marshAlbaran.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
            marshAlbaran.marshal(pedido, System.out);
            marshAlbaran.marshal(pedido, new FileOutputStream(new File("mi_albaran2.xml")));
                    
        } catch (JAXBException jaxbEx) {
            System.out.println(jaxbEx.getMessage());
        } catch (FileNotFoundException fnfEx) {
            System.out.println(fnfEx.getMessage());
        } catch (DatatypeConfigurationException datatconfEx) {
            System.out.println(datatconfEx.getMessage());
        }
        
    }
    
    private static void changeDireccionFacturacion(Direccion direccionFacturacion) {
        //Cambia los datos de la facturación (facturarA)
        direccionFacturacion.setNombre("Farmacia José Teijeiro Vega");
        direccionFacturacion.setCalle("Avenida de Cambados 60");
        direccionFacturacion.setCiudad("Vilagarcía de Arousa");
        direccionFacturacion.setProvincia("Pontevedra");
        direccionFacturacion.setCodPostal(new BigDecimal("36601"));
    }
    
    private static Articulo createArticuloPedido(
            String nombreProducto, String codProducto, int cantidad, BigDecimal precio, 
            String comentario, int year, int month, int day) throws DatatypeConfigurationException {
        // Crear un artículo y añadirlo a la lista de artículos del pedido
        Articulo articulo = new Articulo();
        articulo.setNombreProducto(nombreProducto);
        articulo.setCodigo(codProducto);
        articulo.setCantidad(cantidad);
        articulo.setPrecio(precio);
        articulo.setComentario(comentario);
        XMLGregorianCalendar fechaEnvio = DatatypeFactory.newInstance()
                .newXMLGregorianCalendarDate(year, month, day,  DatatypeConstants.FIELD_UNDEFINED);
        articulo.setFechaEnvio(fechaEnvio);
        return articulo;
    }

    private static void leerContenido(Direccion direccionFacturacion, Pedido pedido) {
        String nombreFarmacia = direccionFacturacion.getNombre();
        String calle = direccionFacturacion.getCalle();
        String ciudad = direccionFacturacion.getCiudad();
        String provincia = direccionFacturacion.getProvincia();
        BigDecimal codPostal = direccionFacturacion.getCodPostal();
        
        System.out.printf(
                "Farmacia: %s%n Calle: %s%n Ciudad: %s%n Provincia: %s%n Codigo Postal: %s%n",
                nombreFarmacia, calle, ciudad, provincia, codPostal.toString());
        
        String comentarioPedido = pedido.getComentario();
        System.out.printf("%s%n%s%n%s%n", "=".repeat(40), comentarioPedido, "=".repeat(40));
        
        
        for (Articulo articulo: pedido.getArticulos()) {
            String nombreProducto = articulo.getNombreProducto();
            String codProducto = articulo.getCodigo();
            int cantidad = articulo.getCantidad();
            BigDecimal precio = articulo.getPrecio();
            String comentario = articulo.getComentario();
            XMLGregorianCalendar fechaEnvio = articulo.getFechaEnvio();
            System.out.printf(
                    "Producto (Código): %s (%s)%n Cantidad: %d%n Precio: %.2f%n Comentario: %s%n Fecha Envio: %s%n",
                    nombreProducto, codProducto, cantidad, precio, comentario, fechaEnvio.toString());
        }
    }
    
}
