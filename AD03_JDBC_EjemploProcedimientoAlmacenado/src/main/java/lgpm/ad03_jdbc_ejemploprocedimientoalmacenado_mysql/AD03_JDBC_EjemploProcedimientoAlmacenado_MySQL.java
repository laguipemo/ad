/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lgpm.ad03_jdbc_ejemploprocedimientoalmacenado_mysql;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 *
 * @author lgpmc
 */
public class AD03_JDBC_EjemploProcedimientoAlmacenado_MySQL {
    //Declaro e inicializo como constantes las variables que almacenan la url,
    //el usuario y la contraseña, necesarias para crar la conexión a la BBDD
    private static final String JDBC_URL = "jdbc:mysql://localhost:3306/test?useSSL=false&serverTimezone=UTC";
    private static final String JDBC_USER = "root";
    private static final String JDBC_PASS = "";

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // Declaro referencia a la conexión y al callableStatement inicializandolas
        //a null
        Connection connection = null;
        CallableStatement callableStatement = null;
        try {
            //creo conexión meidante método getConnection() de la clase DriverManager
            connection = DriverManager.getConnection(JDBC_URL, JDBC_USER, JDBC_PASS);
            //creo el objeto de tipo CallableStatement por medio del método 
            //prepareCall del objeto conexión al cual le pasamos una cadena que
            //representa la llamada para metrizada al procedimiento almacendo.
            callableStatement = connection.prepareCall("{call insertarCliente(?, ?, ?)}");
            
            //Asigno los valores de los parámetros requeridos por el procedimiento
            //que llamaremos, mediante los repectivos método setXXXX() del obejeto
            //callableStatement creado
            callableStatement.setInt("cod_cliente", 777);
            callableStatement.setString("nombre", "Alberto Pérez");
            callableStatement.setString("telefono", "950151617");
            
            //ejecuto la llamada al procedimiento mediante el método execute()
            //del objeto CallebleStatement que creamos le asignamos los valores
            //de los parámetros
            callableStatement.execute();
            
            //cerramos los recursos callableStatement y connection
            if (callableStatement != null) {
                callableStatement.close();
            }
            if (connection != null) {
                connection.close();
            }
            
        } catch (SQLException sqlEx) {
            System.out.println(sqlEx.getMessage());
        }
    }
}
