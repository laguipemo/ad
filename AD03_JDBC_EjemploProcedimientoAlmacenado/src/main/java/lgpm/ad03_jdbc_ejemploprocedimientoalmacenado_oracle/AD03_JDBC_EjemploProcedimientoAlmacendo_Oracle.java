/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lgpm.ad03_jdbc_ejemploprocedimientoalmacenado_oracle;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 *
 * @author lgpmc
 */
public class AD03_JDBC_EjemploProcedimientoAlmacendo_Oracle {
    //Declaro e inicializo como String constantes la url, el usuario y la contraseña
    //necesarias para crear y establecer la conexi´n a la BBDD
    private static final String JDBC_URL_ORACLE = "jdbc:oracle:thin:@localhost";
    private static final String JDBC_USER = "laguipemo";
    private static final String JDBC_PASS = "321montoto";
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        //Declaro e inicializo a null objetos de tipo Connection y CallableStatement
        Connection connection = null;
        CallableStatement callableStatement = null;
        
        try {
            // Creo conexión mediente el método getConnection de la interfaz 
            // DriverManager, pasándole como parámetros los String que definen
            // la url, el usuario y la contraseña. Este objeto lo asigno a la 
            // referencia crada anteriormente.
            connection = DriverManager.getConnection(
                    JDBC_URL_ORACLE, JDBC_USER, JDBC_PASS);
            
            // Creo una sentencia de llamada al procedimiento almacenado deseado
            // (adicionarEquipo que fue previamente creado en Oracle SQL 
            // Developer). Este objeto CallableStatement se crea por medio del
            // método prepareCall() de la conexión creada y al que se le pasa
            // un string que representa la llamada parametrizada al procedimiento:
            callableStatement = connection.prepareCall("{call adicionarEquipo(?, ?)}");
            
            // Asigno al objeto callable, el valor de cada uno de los parámetros
            // que necesita para realizar su función. Esto se puede hacer mediante
            // el crrespondiente método setXXXX()
            callableStatement.setString("nombre", "Movistar");
            callableStatement.setString("nacionalidad", "ESPAGNE");
            
            // realizo la llamada o ejecuto el procesimiento por medio de método
            // execute()
            callableStatement.execute();
            
            //cierro recursos
            if (callableStatement != null) {
                callableStatement.close();
            }
            if (connection != null) {
                connection.close();
            }
            
        } catch (SQLException sqlEx) {
            System.out.println(sqlEx.getMessage());
        }
    }
}
