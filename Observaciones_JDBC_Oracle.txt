Drivers de Oracle
JDBC Thin Driver, utiliza sockets de Java para conectarse directamente a Oracle. Proporciona su propia versión TCP/IP 
del protocolo SQL * Net de Oracle. Debido a que es 100% Java, este controlador es independiente de la plataforma y también
puede ejecutarse desde un navegador web.Los  formatos de URL:

	SID: (ya no se recomienda para uso de Oracle)
	
		jdbc: oracle: thin: [<user> / <password>] @ <host> [: <port>]: <SID>

	Servicios:
	
		jdbc: oracle: thin: [<user> / <password>] @ // <host> [: <port>] / <service>

	TNSNames:

		jdbc: oracle: thin: [<user> / <password>] @ <TNSName>

JDBC OCI (Oracle Call Interfaces ) funciona a través de SQL * Net . Los controladores OCI de JDBC le permiten llamar al 
OCI directamente desde Java, lo que proporciona un alto grado de compatibilidad con una versión específica de Oracle. Debido
a que utilizan métodos nativos, son específicos de la plataforma.

	jdbc: oracle: oci: @myhost: 1521: orcl
	
JDBC KPRB driver (default connection)


El controlador JDBC KPRB de Oracle se utiliza principalmente para escribir procedimientos almacenados Java y JSP (JavaServer 
Pages). Utiliza la sesión de base de datos  actual y, por lo tanto, no requiere un nombre de usuario, contraseña o URL de 
base de datos adicional.Se puede obtener un identificador de la conexión predeterminada o actual (controlador KPRB) llamando 
al método OracleDriver.defaultConnection().

En la unidad se nos indica que podemo encontrar la documentación de estos tres tipos de drivers es la pagina :
http://www.orafaq.com/wiki/JDBC 

En el ejemplo me conecto a la base de datos XE de oracle 11g instalada en el módulo de BBDD de primer año de DAM. Trabajaré
con el workspace: LAGUIPEMO, user: LAGUIPEMO@GMAIL.COM, password: 321montoto. El de formato de URL utiliado es el de Servicios
y su contenido dependerá de la firma del utilizada en el método getConnection() de la clase DriverManager. Tendriamos 3 
opciones:

	- URL que cuenta con toda la información y fima getConnection() por defecto:
	
		String url = "jdbc:oracle:thin:laguipemo/321montoto@localhost:1521:xe";
		
		getConnection(url);
		
	- URL que no incluye ni el usuario de ni la contraseña, información que hay que paserle luego en el método getConnection:
	
		String url = "jdbc:oracle:thin:@localhost:1521:xe"
		
		getConnection(url, "laguipemo", "321montoto");
		
	- URL que no incluye user/pasword ni toda aquella información que se considera opcional si se utilizan los valor por defecto
	  de la instalación de Oracle (el puerto 1521, la base XE):
	  
		String url = "jdbc:oracle:thin:@localhost";
		
		getConnection(url, "laguipemo", "321montoto");
		
La metodología a seguir es como la de MySQL. Desde el JDK 1.6 ya no es necesario registrar el driver con el tradiconal 
Class.forName() pues los drivers ya se registran automáticamente, solo se requiere que estén en el classpath de la JVM.
Por lo tanto los pasos generales a seguir son:

	- Crear una conexión a la BBDD mediante la interfaz DriverManager y en específico su método getConnection() con alguna de
	  sus firmas válidas.
	  
	- Crear un objeto de tipo Statement (createStatement()), PreparedStatement (prepareStatement()) o CallableStatement 
	  (prepareCall()según necesitemos:
	  
			- una sentencia de consulta o modificación normal, 
			
			- una sentencia que se escriba una vez y quede precompilada de forma que se pueda utilizar múltiples veces 
			  pasándole los parámetros con los datos necesarios para hacer consultas o actulizaciones (uso de parametrización) 
			  
			- ejecutar es un procedimiento almacenado;
		  
	  respectivamente.
	  
	- Dependiendo de la sentencia utilizada necesitaremos un objeto ResultSet donde guardar el resultado de la consulta hecha
	  por medio del método executeQuerry(), una variable de tipo entero para almacenar el número de filas modificadas en la 
	  BBDD si es una sentencia de actualización ejecutada mediante executeUpdate(), etc.
	  
	- Manejar los resultados obtenidos.

En todo ello puede tener lugar una excepsión de tipo SQLException que tenemos que capturar.

En el ejemplo que comento he creado dos paquetes, uno para realizar una consulta a la base de datos, guardar el resultado en
un ResultSet y luego recorrerlo con un bucle while haciendo uso del método next() del ResultSet y el otro para realizar 
senticias DML (Data Manipulation Language), es decir, INSERT, UPDATE y DELETE.

	Código del proyecto AD03_JDBC_EjemploOracle:
	
		Código clase AD03_JDBC_EjemploOracle_Consulta del paquete lgpm.ad03_jdbc_ejemplooracle.sqlconsulta:
		
			package lgpm.ad03_jdbc_ejemplooracle.sqlconsulta;

			import java.sql.Connection;
			import java.sql.DriverManager;
			import java.sql.ResultSet;
			import java.sql.SQLException;
			import java.sql.Statement;

			/**
			 *
			 * @author lgpmc
			 */
			public class AD03_JDBC_EjemploOracleSQL_Consulta {
				//Toda la información está en el url
			//    private static final String URL_JDBC_ORACLE = "jdbc:oracle:thin:laguipemo/321montoto@localhost:1521:XE";
				// El usuario y la contraseña se omiten porque se pasarán al constructor de la conexión
			//    private static final String URL_JDBC_ORACLE = "jdbc:oracle:thin:@localhost:1521:XE";
				// El puerto es opcional a menos de que no se esté utilizando el tipico de oracle (1521)
				private static final String URL_JDBC_ORACLE = "jdbc:oracle:thin:@localhost"; 
				/**
				 * @param args the command line arguments
				 */
				public static void main(String[] args) {
					// Declaro una referencia a objetos de tipo Connection, Statement y ResultSet
					Connection connection = null;
					Statement statement = null;
					ResultSet resultSet = null;
					try {
						// Inicializo con una conexión obtenida a partir de la clase estática
						// DriverManager y su métod getConnection()
			//            connection = DriverManager.getConnection(URL_JDBC_ORACLE); // user/password en la url
						connection = DriverManager.getConnection(URL_JDBC_ORACLE, "laguipemo", "321montoto");
						// Creo un objeto de tipo Statement para la conexión creada a partir
						// de su método createStatement()
						statement = connection.createStatement();
						// Consulta sql a realizar: obtener todas la clumnas de todas la filas
						// de la tabla equipos.
						String sqlQuerry = "SELECT * FROM equipos WHERE NACIONALIDAD='ESPAGNE'";
						// Ejecuto una consulta mediante el método executeQuery() del objeto
						// Statement creado y su resultda lo almaceno en un ResultSet
						resultSet = statement.executeQuery(sqlQuerry);
						// Recorro el ResulSet valiéndome de su método next(), que entrega la
						// siguente fila del resulSet. Para el recorrido utilizo un ciclo while  
						while (resultSet.next()) {
							System.out.println(
									"Nombre Equipo: " + resultSet.getString("NOM_EQUIP") + 
											", Nacionalidad: " + resultSet.getString("NACIONALIDAD"));
						}
						// Cerrar ResultSet, Statement y Connection
						if (resultSet != null) {
							resultSet.close();
						}
						if (statement != null) {
							statement.close();
						}
						if (connection != null) {
							connection.close();
						}
						
					} catch (SQLException sqlEx) {
						System.out.println(sqlEx.getMessage());
					}
				}
			}

		Código clase AD03_JDBC_EjemploOracle_Actualizacion del paquete lgpm.ad03_jdbc_ejemplooracle.sqlactualizacion:

			package lgpm.ad03_jdbc_ejemplooracle.sqlactualizacion;

			import java.sql.Connection;
			import java.sql.DriverManager;
			import java.sql.SQLException;
			import java.sql.Statement;

			/**
			 *
			 * @author lgpmc
			 */
			public class AD03_JDBC_EjemploOracleSQL_Actualizacion {
				//Defino constante estática con la url de la conexión
				
				// Versión de la url en la que se indica toda la informacion necesaria para
				// la conexión, incluyento valores opciones o que se sulen añadir después 
				// en el constuctor de la conexión
				private static final String URL_JDBC_ORACLE = "jdbc:oracle:thin:laguipemo/321montoto@localhost:1521:xe";
				

				/**
				 * @param args the command line arguments
				 */
				public static void main(String[] args) {
					// Declaro e inicializo Connection y Statement a null
					Connection connection = null;
					Statement statement = null;
					int numFilasActualizadas = 0;
					
					try {
						//Obtengo una conexión por medio del método statico getConnection() 
						//de la clase DriverManager, utilizando la firma en la que se requiere
						//solo la url que contiene toda la información
						connection = DriverManager.getConnection(URL_JDBC_ORACLE);
						//creo un statement por medio del método createStatement() de la 
						//conexión creada y lo asigno a la referencia de tipo Statement crada
						//al inicio.
						statement = connection.createStatement();
						
						//Ejemplo actualización de la BBDD por inserción de un registro
						String sqlInsert = "INSERT INTO equipos VALUES('Movistar', 'ESPAGNE')";
						//Ejemplo actualización de la BBDD por actualización de un registro
						String sqlUpdate = "UPDATE equipos SET NOM_EQUIP='Movistar TEAM' WHERE NOM_EQUIP='Movistar'";
						//Ejemplo actualización de la BBDD por borrado de un registro
						String sqlDelete = "DELETE FROM equipos WHERE NOM_EQUIP='Movistar TEAM'";

						//ejecuto la sentencia sqlInsert definida anteriormente
			//            numFilasActualizadas = statement.executeUpdate(sqlInsert);
						
						//ejecuto la sentencia sqlUpdate definida anteriormente
			//            numFilasActualizadas = statement.executeUpdate(sqlUpdate);
						
						//ejecuto la sentencia sqlDelete definida anteriormente
						numFilasActualizadas = statement.executeUpdate(sqlDelete);
						
						//Muestro el número de filas o registros de la BBDD que se vieron 
						//afectados por la actualización 
						System.out.println(
								"Se actualizaron " + numFilasActualizadas + 
										" filas de la Base de Datos.");
						
						// Cierro Statement y Connection
						if (statement != null) {
							statement.close();
						}
						if (connection != null) {
							connection.close();
						}
					} catch (SQLException sqlEx) {
						System.out.println(sqlEx.getMessage());
					}
					 
				}
			}
			


En la unidad nos recomiendan la página: https://users.dcc.uchile.cl/~lmateu/CC60H/Trabajos/jfernand/ donde hay una 
explicación sencilla de JDBC en Oracle y donde se incluyen varios ejemplos simples:

	driver_ver.java : Despliega la versión del driver. 
			https://users.dcc.uchile.cl/~lmateu/CC60H/Trabajos/jfernand/driver_ver.java
	driver_check.java Chequea si corren los drivers correctamente. 
			https://users.dcc.uchile.cl/~lmateu/CC60H/Trabajos/jfernand/driver_check.java

	tab_sel.java : Select a una tabla. 
			https://users.dcc.uchile.cl/~lmateu/CC60H/Trabajos/jfernand/tab_sel.java
	tab_upd.java : Modifica registros en una tabla. 
			https://users.dcc.uchile.cl/~lmateu/CC60H/Trabajos/jfernand/tab_upd.java
	tab_del.java : Borra registros de una tabla.
			https://users.dcc.uchile.cl/~lmateu/CC60H/Trabajos/jfernand/tab_del.java
	tab_ins.java Inserta registros hacia una tabla.
			https://users.dcc.uchile.cl/~lmateu/CC60H/Trabajos/jfernand/tab_ins.java
	tabprep_upd.java Como usar una sentencia de preparación con un "update".
			https://users.dcc.uchile.cl/~lmateu/CC60H/Trabajos/jfernand/tabprep_upd.java
	
	Otros ejemplos:

	call_sp.java Llamado a un procedimiento de Oracle.
			https://users.dcc.uchile.cl/~lmateu/CC60H/Trabajos/jfernand/call_sp.java
	call_sf.java Llama a una función de Oracle.
			https://users.dcc.uchile.cl/~lmateu/CC60H/Trabajos/jfernand/call_sf.java
	excep_1.java Manejo de excepciones (1).
			https://users.dcc.uchile.cl/~lmateu/CC60H/Trabajos/jfernand/excep_1.java
	excep_2.java Manejo de excepciones (2).
			https://users.dcc.uchile.cl/~lmateu/CC60H/Trabajos/jfernand/excep_2.java
	meta_data1.java Meta-datos. Cómo manejar un "select *".
			https://users.dcc.uchile.cl/~lmateu/CC60H/Trabajos/jfernand/meta_data1.java
	meta_data2.java Más métodos con meta-datos , cómo obtener los nombres de los campos.
			https://users.dcc.uchile.cl/~lmateu/CC60H/Trabajos/jfernand/meta_data2.java
	meta_data3.java Más métodos con meta-datos (*).
			https://users.dcc.uchile.cl/~lmateu/CC60H/Trabajos/jfernand/meta_data3.java
	meta_data4.java Detalles sobre las palabras claves no estándares de la BD.
			https://users.dcc.uchile.cl/~lmateu/CC60H/Trabajos/jfernand/meta_data4.java
	meta_data5.java Cómo encontrar el largo máximo de un nombre de un campo.
			https://users.dcc.uchile.cl/~lmateu/CC60H/Trabajos/jfernand/meta_data5.java
	meta_data6.java Tipos de tablas permitidos.
			https://users.dcc.uchile.cl/~lmateu/CC60H/Trabajos/jfernand/meta_data6.java
	tab_preupdtran.java Cómo setear una transacción.
			https://users.dcc.uchile.cl/~lmateu/CC60H/Trabajos/jfernand/tab_preupdtran.java
	tab_preupdrb.java Cómo ejecutar un "rollback".
			https://users.dcc.uchile.cl/~lmateu/CC60H/Trabajos/jfernand/tab_preupdrb.java

  