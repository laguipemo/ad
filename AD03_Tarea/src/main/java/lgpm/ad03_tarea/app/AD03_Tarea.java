/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lgpm.ad03_tarea.app;

import java.sql.SQLException;
import java.util.List;
import lgpm.ad03_tarea.model.dao.DepartamentoConsultor;
import lgpm.ad03_tarea.model.dao.EmpleadoConsultor;
import lgpm.ad03_tarea.model.dao.IConsultor;
import lgpm.ad03_tarea.model.dao.SalarioConsultor;
import lgpm.ad03_tarea.view.VentanaEmpleado;
import lgpm.ad03_tarea.view.VentanaPrincipal;
import lgpm.ad03_tarea.model.dto.Departamento;
import lgpm.ad03_tarea.model.dto.Empleado;
import lgpm.ad03_tarea.model.dto.Salario;

/**
 *
 * @author lgpmc
 */
public class AD03_Tarea {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
//        IConsultor departamentoConsultor = new DepartamentoConsultor("MySQL", "root", "");
//        IConsultor empleadoConsultor = new EmpleadoConsultor("MySQL", "root", "");
//        IConsultor salarioConsultor = new SalarioConsultor("MySQL", "root", "");
//        
//        try {
//            // Ejemplo de select Departamentos
//            List<Departamento> departamentosSelected = departamentoConsultor.selectElements();
//            System.out.println("\nLista de Departamentos seleccionados");
//            System.out.println("\n" + "=".repeat(40) + "\n");
//            departamentosSelected.forEach(departamento -> System.out.println(departamento));
//            System.out.println("\n" + "=".repeat(40));
//            
//            // Ejemplo de select Epmpleados
//            List<Empleado> empleadosSelected = empleadoConsultor.selectElements(10);
//            System.out.println("\nLista de Empleados seleccionados (10 primeros)");
//            System.out.println("\n" + "=".repeat(40) + "\n");
//            empleadosSelected.forEach(empleado -> System.out.println(empleado));
//            System.out.println("\n" + "=".repeat(40));
//            
//            // Ejemplo de select Salarios
//            List<Salario> salariosSelected = salarioConsultor.selectElements(3);
//            System.out.println("\nLista de Salarios seleccionados (3 primeros)");
//            System.out.println("\n" + "=".repeat(40) + "\n");
//            salariosSelected.forEach(salario -> System.out.println(salario));
//            System.out.println("\n" + "=".repeat(40));
//            
//        } catch (SQLException sqlEx) {
//            sqlEx.printStackTrace();
//        }

        VentanaPrincipal ventanaPrincipal = new VentanaPrincipal();
        ventanaPrincipal.setLocationRelativeTo(null);
        ventanaPrincipal.setVisible(true);
    }
    
}
