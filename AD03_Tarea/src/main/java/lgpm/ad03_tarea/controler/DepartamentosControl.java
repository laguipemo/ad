/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lgpm.ad03_tarea.controler;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;
import lgpm.ad03_tarea.model.dao.DepartamentoConsultor;
import lgpm.ad03_tarea.model.dto.Departamento;

/**
 *
 * @author lgpmc
 */
public class DepartamentosControl {
    private static final String JDBC_DEPT = "SELECT deptno, dname, loc FROM dept";
    
    public static List<Departamento> getListDepartamentos(String dataBase, String usuario, String pass) {
        List<Departamento> listaDepartamentos = new ArrayList<>();
        DepartamentoConsultor departamentoConsultor = new DepartamentoConsultor(dataBase, usuario, pass);
        try {
            listaDepartamentos = departamentoConsultor.selectElements();
        } catch (SQLException sqlEx) {
            String titulo = "Error en la conxión";
            String texto = sqlEx.getMessage();
            JOptionPane.showMessageDialog(null,texto,titulo,JOptionPane.ERROR_MESSAGE);
        }
        
        return listaDepartamentos;
    }
        
    public static List<Integer> getListNumDepartamentos(
            String dataBase, String usuario, String pass) {
        
        List<Integer> listaNumerosDept = new ArrayList<>();
        
        DepartamentoConsultor empleadoConsultor = new DepartamentoConsultor(dataBase, usuario, pass);
        try {
            for (Departamento departamento: empleadoConsultor.selectElements()) {
                listaNumerosDept.add(departamento.getDeptNum());
            }
                    
        } catch (SQLException sqlEx) {
            String titulo = "Error en la conxión";
            String texto = sqlEx.getMessage();
            JOptionPane.showMessageDialog(null,texto,titulo,JOptionPane.ERROR_MESSAGE);
        }
        return listaNumerosDept;
    }
    
    
    public static Departamento getDepartamentoWithNum(String dataBase, String usuario, String pass, int deptNo) {
        
        String strWhereClause = " WHERE DEPTNO=" + deptNo;
        String sqlSelect = JDBC_DEPT + strWhereClause;
        
        DepartamentoConsultor departamentoConsultor = new DepartamentoConsultor(dataBase, usuario, pass);
        Departamento departamento = null;
        try {
            departamento = departamentoConsultor.selectElements(sqlSelect).get(0);
        } catch (SQLException sqlEx) {
            String titulo = "Error en la conxión";
            String texto = sqlEx.getMessage();
            JOptionPane.showMessageDialog(null,texto,titulo,JOptionPane.ERROR_MESSAGE);
        }
        return departamento;
    }
    
    public static int getDepartamentoNumFrom(String dataBase, String usuario, String pass, String strDeptName) {
        int deptNo = 0;
        for (Departamento departamento: getListDepartamentos(dataBase, usuario, pass)) {
            if (departamento.getDeptName().equalsIgnoreCase(strDeptName)) {
                deptNo = departamento.getDeptNum();
                break;
            }
        }
        return deptNo;
    }
    
}
