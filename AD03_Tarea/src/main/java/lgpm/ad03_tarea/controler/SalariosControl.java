/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lgpm.ad03_tarea.controler;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;
import lgpm.ad03_tarea.model.dao.SalarioConsultor;
import lgpm.ad03_tarea.model.dto.Salario;

/**
 *
 * @author lgpmc
 */
public class SalariosControl {
    private static final String JDBC_SAL = "SELECT grade, losal, hisal FROM salgrade";
    
    public static List<Salario> getListSalarios(String dataBase, String usuario, String pass) {
        List<Salario> listaSalarios = new ArrayList<>();
        SalarioConsultor salarioConsultor = new SalarioConsultor(dataBase, usuario, pass);
        try {
            listaSalarios = salarioConsultor.selectElements();
        } catch (SQLException sqlEx) {
            String titulo = "Error en la conxión";
            String texto = sqlEx.getMessage();
            JOptionPane.showMessageDialog(null,texto,titulo,JOptionPane.ERROR_MESSAGE);
        }
        
        return listaSalarios;
    }
        
    public static List<Integer> getListNumSalarios(
            String dataBase, String usuario, String pass) {
        
        List<Integer> listaNumerosSal = new ArrayList<>();
        
        SalarioConsultor salarioConsultor = new SalarioConsultor(dataBase, usuario, pass);
        try {
            for (Salario salario: salarioConsultor.selectElements()) {
                listaNumerosSal.add(salario.getSalGrade());
            }
                    
        } catch (SQLException sqlEx) {
            String titulo = "Error en la conxión";
            String texto = sqlEx.getMessage();
            JOptionPane.showMessageDialog(null,texto,titulo,JOptionPane.ERROR_MESSAGE);
        }
        return listaNumerosSal;
    }
    
    
    public static Salario getSalarioWithNum(
            String dataBase, String usuario, String pass, int grade) {
        
        String strWhereClause = " WHERE GRADE=" + grade;
        String sqlSelect = JDBC_SAL + strWhereClause;
        
        SalarioConsultor salarioConsultor = new SalarioConsultor(dataBase, usuario, pass);
        Salario salario = null;
        try {
            salario = salarioConsultor.selectElements(sqlSelect).get(0);
        } catch (SQLException sqlEx) {
            String titulo = "Error en la conxión";
            String texto = sqlEx.getMessage();
            JOptionPane.showMessageDialog(null,texto,titulo,JOptionPane.ERROR_MESSAGE);
        }
        return salario;
    }
    
}
