/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lgpm.ad03_tarea.controler;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import lgpm.ad03_tarea.model.dao.DBConnection;
import lgpm.ad03_tarea.model.dao.EmpleadoConsultor;
import lgpm.ad03_tarea.model.dto.Empleado;

/**
 *
 * @author lgpmc
 */
public class EmpleadosControl {
    private static final String JDBC_EMP_FILTRO = 
            "SELECT empno, ename, job, mgr, hiredate, sal, comm, deptno " + 
            "FROM emp";
    
    
    public static List<Empleado> getListEmpleadosFiltro(
            String dataBase, String usuario, String pass, String strDeptNo, String strJob) {
        
        String strWhereClause;
        if (!strDeptNo.equalsIgnoreCase("Todos") && !strJob.equalsIgnoreCase("Todas")) {
            strWhereClause = " WHERE DEPTNO="+ strDeptNo + " AND JOB LIKE '%"+ strJob + "%'";
        } else if (strDeptNo.equalsIgnoreCase("Todos") && !strJob.equalsIgnoreCase("Todas")) {
            strWhereClause = " WHERE JOB LIKE '%"+ strJob + "%'";
        } else if (!strDeptNo.equalsIgnoreCase("Todos") && strJob.equalsIgnoreCase("Todas")) {
            strWhereClause = " WHERE DEPTNO="+ strDeptNo;
        } else {
            strWhereClause = "";
        }
        
        List<Empleado> listaEmpleadosFiltro = new ArrayList<>();
       
        EmpleadoConsultor empleadoConsultor = new EmpleadoConsultor(dataBase, usuario, pass);
        try {
            listaEmpleadosFiltro = empleadoConsultor.selectElements(
                    JDBC_EMP_FILTRO + strWhereClause);
        } catch (SQLException sqlEx) {
            String titulo = "Error en la conxión";
            String texto = sqlEx.getMessage();
            JOptionPane.showMessageDialog(null,texto,titulo,JOptionPane.ERROR_MESSAGE);
        }
        return listaEmpleadosFiltro;
    }
    
    public static List<Integer> getListNumEmpleados(
            String dataBase, String usuario, String pass) {
        
        List<Integer> listaNumerosEmp = new ArrayList<>();
        
        EmpleadoConsultor empleadoConsultor = new EmpleadoConsultor(dataBase, usuario, pass);
        try {
            for (Empleado empleado: empleadoConsultor.selectElements()) {
                listaNumerosEmp.add(empleado.getEmpNum());
            }
                    
        } catch (SQLException sqlEx) {
            String titulo = "Error en la conxión";
            String texto = sqlEx.getMessage();
            JOptionPane.showMessageDialog(null,texto,titulo,JOptionPane.ERROR_MESSAGE);
        }
        return listaNumerosEmp;
    }
    
        
    public static Empleado getEmpleadoWithNum(
            String dataBase, String usuario, String pass, int empNo) {
        
        String strWhereClause = " WHERE EMPNO=" + empNo;
        String sqlSelect = JDBC_EMP_FILTRO + strWhereClause;
        
        EmpleadoConsultor empleadoConsultor = new EmpleadoConsultor(dataBase, usuario, pass);
        
        Empleado empleado = null;
        try {
            empleado = empleadoConsultor.selectElements(sqlSelect).get(0);
        } catch (SQLException sqlEx) {
            String titulo = "Error en la conxión";
            String texto = sqlEx.getMessage();
            JOptionPane.showMessageDialog(null,texto,titulo,JOptionPane.ERROR_MESSAGE);
        }
        return empleado;
    }
    
    public static int updateAllEmpSalInPercent(
            String dataBase, String usuario, String pass, double percent) {
        
        int numOfSalIncreases = 0;
        
        EmpleadoConsultor empleadoConsultor = new EmpleadoConsultor(
                dataBase, usuario, pass);
        try {
            for (Empleado empleado: 
                    empleadoConsultor.selectElements(JDBC_EMP_FILTRO)) {
                //recupero salario y lo varío en un porciento dado
                double sal = empleado.getEmpSal();
                empleado.setEmpSal(sal + (sal * percent / 100));
                //actualizo datos empleado
                try {
                    numOfSalIncreases += empleadoConsultor.updateElement(empleado);
                } catch (SQLException sqlEx) {
                    String titulo = "Error en la conxión";
                    String texto = sqlEx.getMessage();
                    JOptionPane.showMessageDialog(null,texto,titulo,JOptionPane.ERROR_MESSAGE);
                }
            }
            
        } catch (SQLException sqlEx) {
            String titulo = "Error en la conxión";
            String texto = sqlEx.getMessage();
            JOptionPane.showMessageDialog(null,texto,titulo,JOptionPane.ERROR_MESSAGE);
        }
        
        return numOfSalIncreases;
    }
}
