/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lgpm.ad03_tarea.controler;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import lgpm.ad03_tarea.model.dao.DBConnection;
import lgpm.ad03_tarea.model.dao.EmpleadoConsultor;

/**
 *
 * @author lgpmc
 */
public class JobsControl {
    
    private static final String JDBC_JOBS_DISTINCT = "SELECT DISTINCT job FROM emp";
    
    public static List<String> getListJobsDistinct(String dataBase) {
        List<String> listaJobsDistinct = new ArrayList<>();
        Connection dbConnection = null;
        Statement sqlStatement = null;
        ResultSet resultSet = null;
        try {
            dbConnection = DBConnection.getConnection(dataBase);
            sqlStatement = dbConnection.createStatement();
            resultSet = sqlStatement.executeQuery(JDBC_JOBS_DISTINCT);
            while (resultSet.next()) {
                listaJobsDistinct.add(resultSet.getString("JOB"));
            }
            
            DBConnection.close(resultSet);
            DBConnection.close(sqlStatement);
            DBConnection.close(dbConnection);
        } catch (SQLException sqlEx) {
            String titulo = "Error en la conxión";
            String texto = sqlEx.getMessage();
            JOptionPane.showMessageDialog(null,texto,titulo,JOptionPane.ERROR_MESSAGE);
        }
        
        return listaJobsDistinct;
    }
}
