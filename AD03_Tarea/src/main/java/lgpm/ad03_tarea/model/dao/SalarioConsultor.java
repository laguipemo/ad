/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lgpm.ad03_tarea.model.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import lgpm.ad03_tarea.model.dto.Salario;

/**
 *
 * @author lgpmc
 */
public class SalarioConsultor implements IConsultor<Salario> {
    
    private static final String SQL_SELECT = 
            "SELECT grade, losal, hisal FROM salgrade";
    private static final String SQL_SELECT_LIMIT = 
            "SELECT grade, losal, hisal FROM salgrade LIMIT ?";
    private static final String SQL_SELECT_ROWNUM = 
            "SELECT grade, losal, hisal FROM salgrade WHERE ROWNUM <=?";
    
    private static final String SQL_INSERT = 
            "INSERT INTO salgrade (losal, hisal) VALUES (?, ?)";
    
    private static final String SQL_UPDATE = 
            "UPDATE salgrade SET losal=?, hisal=?  WHERE grade=?";
    
    private static final String SQL_DELETE = 
            "DELETE FROM salgrade WHERE grade=?";

    private String dataBase;
    private String usuario;
    private String pass;
    
    
    public SalarioConsultor() {
        this("ORACLE", "AD03_TAREA", "AD03_TAREA");
    }

    public SalarioConsultor(String dataBase, String usuario, String pass) {
        if (dataBase.toUpperCase().contains("MYSQL")) {
            this.dataBase = "MYSQL";
        } else {
            this.dataBase = "ORACLE";
        } 
        this.usuario = usuario;
        this.pass = pass;
    }

    @Override
    public List<Salario> selectElements() throws SQLException {
        return selectElements(SQL_SELECT);
    }

    @Override
    public List<Salario> selectElements(int limit) throws SQLException {
        return selectElements(SQL_SELECT, limit);
    }

    @Override
    public List<Salario> selectElements(String sqlSelect) throws SQLException {
        Connection dbConnection = DBConnection.getConnection(dataBase, usuario, pass);
        
        Statement sqlStatement = dbConnection.createStatement();
        
        ResultSet resultSet = sqlStatement.executeQuery(sqlSelect);
        
        List<Salario> salariosSelected = new ArrayList<>();
        readSalariososSelected(resultSet, salariosSelected);
        
        DBConnection.close(resultSet);
        DBConnection.close(sqlStatement);
        DBConnection.close(dbConnection);
        
        return salariosSelected;
    }

    @Override
    public List<Salario> selectElements(String sqlSelect, int limit) throws SQLException {
        
        if (dataBase.toUpperCase().equals("MYSQL")) {
            sqlSelect = SQL_SELECT_LIMIT;
        } else {
            sqlSelect = SQL_SELECT_ROWNUM;
        }
        
        Connection dbConnection = DBConnection.getConnection(dataBase, usuario, pass);
       
        PreparedStatement sqlPreparedStatement = dbConnection.prepareStatement(sqlSelect);
        sqlPreparedStatement.setInt(1, limit);
        
        ResultSet resultSet = sqlPreparedStatement.executeQuery();
        
        List<Salario> salariosSelected = new ArrayList<>();
        readSalariososSelected(resultSet, salariosSelected);
        
        DBConnection.close(resultSet);
        DBConnection.close(sqlPreparedStatement);
        DBConnection.close(dbConnection);
        
        return salariosSelected;
    }
    
    private void readSalariososSelected(
            ResultSet resultSet, List<Salario> salariosSelected) throws SQLException {
        
        while (resultSet.next()) {
            int salGrade = resultSet.getInt("GRADE");
            Double salLo = resultSet.getDouble("LOSAL");
            Double salHi = resultSet.getDouble("HISAL");
            Salario salario = new Salario(salGrade, salLo, salHi);
            salariosSelected.add(salario);
        }
    }

    @Override
    public int insertElement(Salario salario) throws SQLException {
        return insertElement(SQL_INSERT, salario);
    }

    @Override
    public int insertElement(String sqlInsert, Salario salario) throws SQLException {
        int numRowInserted = 0;
        
        Connection dbConnection = DBConnection.getConnection(dataBase, usuario, pass);
        
        PreparedStatement sqlPreparedStatement = dbConnection.prepareStatement(sqlInsert);
        prepareSalarioData(sqlPreparedStatement, salario);
        
        numRowInserted = sqlPreparedStatement.executeUpdate();
        
        DBConnection.close(sqlPreparedStatement);
        DBConnection.close(dbConnection);
        
        return numRowInserted;
    }

    @Override
    public int updateElement(Salario salario) throws SQLException {
        return updateElement(SQL_UPDATE, salario);
    }

    @Override
    public int updateElement(String sqlUpdate, Salario salario) throws SQLException {
        int numRowUpdated = 0;
        
        Connection dbConnection = DBConnection.getConnection(dataBase, usuario, pass);
        
        PreparedStatement sqlPreparedStatement = dbConnection.prepareStatement(sqlUpdate);
        prepareSalarioData(sqlPreparedStatement, salario);
        sqlPreparedStatement.setInt(3, salario.getSalGrade());
        
        numRowUpdated = sqlPreparedStatement.executeUpdate();
        
        DBConnection.close(sqlPreparedStatement);
        DBConnection.close(dbConnection);
        
        return numRowUpdated;
    }
    
    private void prepareSalarioData(
            PreparedStatement sqlPreparedStatement, Salario salario) throws SQLException {
        
        sqlPreparedStatement.setDouble(1, salario.getSalLo());
        sqlPreparedStatement.setDouble(2, salario.getSalHi());
        
    }
    
    @Override
    public int deleteElement(int grade) throws SQLException {
        int numRowDelete = 0;
        
        Connection dbConnection = DBConnection.getConnection(dataBase, usuario, pass);
        
        PreparedStatement sqlPreparedStatement = dbConnection.prepareStatement(SQL_DELETE);
        sqlPreparedStatement.setInt(1, grade);
        numRowDelete = sqlPreparedStatement.executeUpdate();
        
        DBConnection.close(sqlPreparedStatement);
        DBConnection.close(dbConnection);
        
        return numRowDelete;
    }
    
    @Override
    public int deleteElement(Salario salario) throws SQLException {
        return deleteElement(SQL_DELETE, salario);
    }

    @Override
    public int deleteElement(String sqlDelete, Salario salario) throws SQLException {
        int numRowDelete = 0;
        
        Connection dbConnection = DBConnection.getConnection(dataBase, usuario, pass);
        
        PreparedStatement sqlPreparedStatement = dbConnection.prepareStatement(sqlDelete);
        sqlPreparedStatement.setInt(1, salario.getSalGrade());
        
        numRowDelete = sqlPreparedStatement.executeUpdate();
        
        DBConnection.close(sqlPreparedStatement);
        DBConnection.close(dbConnection);
        
        return numRowDelete;
    }
    
    
    
    
}
