/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lgpm.ad03_tarea.model.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
//import oracle.jdbc.OracleDriver;

/**
 *
 * @author lgpmc
 */
public class DBConnection {
    
    private static final String JDBC_URL_ORACLE = "jdbc:oracle:thin:@localhost:1521:xe";
    private static final String JDBC_USER_ORACLE = "AD03_TAREA";
    private static final String JDBC_PASS_ORACLE = "AD03_TAREA";
    private static final String JDBC_URL_MYSQL = 
            "jdbc:mysql://localhost:3306/AD03_TAREA?useSSL=false&severTimezone=UTC";
    private static final String JDBC_USER_MYSQL = "root";
    private static final String JDBC_PASS_MYSQL = "";
    

    public static Connection getOracleConnection() throws SQLException {
//        DriverManager.registerDriver(new OracleDriver());
        return DriverManager.getConnection(
                JDBC_URL_ORACLE, JDBC_USER_ORACLE, JDBC_PASS_ORACLE);
    }
    
    public static Connection getMySQLConnection() throws SQLException {
        return DriverManager.getConnection(
                JDBC_URL_MYSQL, JDBC_USER_MYSQL, JDBC_PASS_MYSQL);
    }
    
    public static Connection getConnection(String dataBase) throws SQLException {
        String jdbc_user  = dataBase.toUpperCase().contains("MYSQL")? 
                JDBC_USER_MYSQL : JDBC_USER_ORACLE;
        String jdbc_pass  = dataBase.toUpperCase().contains("MYSQL")? 
                JDBC_PASS_MYSQL : JDBC_PASS_ORACLE;
        
        return getConnection(dataBase, jdbc_user, jdbc_pass);
    }
    
    public static Connection getConnection(String dataBase, String usuario, String pass) throws SQLException {
        
        if (dataBase.toUpperCase().contains("ORACLE")) {
//            DriverManager.registerDriver(new OracleDriver());
        }
        String jdbc_url  = dataBase.toUpperCase().contains("MYSQL")? 
                JDBC_URL_MYSQL : JDBC_URL_ORACLE;
        String jdbc_user  = usuario;
        String jdbc_pass  = pass;
        
        return DriverManager.getConnection(jdbc_url, jdbc_user, jdbc_pass);
    }
    
    public static void close(Connection dbConnection) throws SQLException {
        dbConnection.close();
    }
    
    public static void close(Statement sqlStatement) throws SQLException {
        sqlStatement.close();
    }
    
    public static void close(ResultSet resultSet) throws SQLException {
        resultSet.close();
    }
}
