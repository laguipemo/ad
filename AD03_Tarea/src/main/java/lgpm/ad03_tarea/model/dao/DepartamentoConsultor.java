/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lgpm.ad03_tarea.model.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import lgpm.ad03_tarea.model.dto.Departamento;

/**
 *
 * @author lgpmc
 */
public class DepartamentoConsultor implements IConsultor<Departamento> {
    
    private static final String SQL_SELECT = "SELECT deptno, dname, loc FROM dept";
    private static final String SQL_SELECT_LIMIT = 
            "SELECT deptno, dname, loc FROM dept LIMIT ?";
    private static final String SQL_SELECT_ROWNUM = 
            "SELECT deptno, dname, loc FROM dept WHERE ROWNUM <=?";
    
    private static final String SQL_INSERT = 
            "INSERT INTO dept (deptno, dname, loc) VALUES (?, ?, ?)";
    
    private static final String SQL_UPDATE = 
            "UPDATE dept " + 
            "SET dname=?, loc=? " + 
            "WHERE deptno=?";
    
    private static final String SQL_DELETE = 
            "DELETE FROM dept WHERE deptno=?";

    private String dataBase;
    private String usuario;
    private String pass;

    
    
    public DepartamentoConsultor() {
        this("ORACLE", "AD03_TAREA", "AD03_TAREA");
    }

    public DepartamentoConsultor(String dataBase, String usuario, String pass) {
        if (dataBase.toUpperCase().contains("MYSQL")) {
            this.dataBase = "MYSQL";
        } else {
            this.dataBase = "ORACLE";
        } 
        this.usuario = usuario;
        this.pass = pass;
    }

    @Override
    public List<Departamento> selectElements() throws SQLException {
        return selectElements(SQL_SELECT);
    }

    @Override
    public List<Departamento> selectElements(int limit) throws SQLException {
        return selectElements(SQL_SELECT, limit);
    }

    @Override
    public List<Departamento> selectElements(String sqlSelect) throws SQLException {
        
        Connection dbConnection = DBConnection.getConnection(dataBase, usuario, pass);
        
        Statement sqlStatement = dbConnection.createStatement();
        
        ResultSet resultSet = sqlStatement.executeQuery(sqlSelect);
        
        List<Departamento> departamentosSelected = new ArrayList<>();
        readDepartamentosSelected(resultSet, departamentosSelected);
        
        DBConnection.close(resultSet);
        DBConnection.close(sqlStatement);
        DBConnection.close(dbConnection);
        
        return departamentosSelected;
    }

    @Override
    public List<Departamento> selectElements(String sqlSelect, int limit) throws SQLException {
        
        if (dataBase.toUpperCase().equals("MYSQL")) {
            sqlSelect = SQL_SELECT_LIMIT;
        } else {
            sqlSelect = SQL_SELECT_ROWNUM;
        }
        
        Connection dbConnection = DBConnection.getConnection(dataBase, usuario, pass);
       
        PreparedStatement sqlPreparedStatement = dbConnection.prepareStatement(sqlSelect);
        sqlPreparedStatement.setInt(1, limit);
        
        ResultSet resultSet = sqlPreparedStatement.executeQuery();
        
        List<Departamento> departamentosSelected = new ArrayList<>();
        readDepartamentosSelected(resultSet, departamentosSelected);
        
        DBConnection.close(resultSet);
        DBConnection.close(sqlPreparedStatement);
        DBConnection.close(dbConnection);
        
        return departamentosSelected;
    }
    
    private void readDepartamentosSelected(
            ResultSet resultSet, 
            List<Departamento> departamentosSelected) throws SQLException {
        
        while (resultSet.next()) {
            int deptNo = resultSet.getInt("DEPTNO");
            String deptName = resultSet.getString("DNAME");
            String deptLoc = resultSet.getString("LOC");
            
            Departamento departamento = new Departamento(deptNo, deptName, deptLoc);
            departamentosSelected.add(departamento);
        }
    }

    @Override
    public int insertElement(Departamento departamento) throws SQLException {
        return insertElement(SQL_INSERT, departamento);
    }

    @Override
    public int insertElement(String sqlInsert, Departamento departamento) throws SQLException {
        int numRowInserted = 0;
        
        Connection dbConnection = DBConnection.getConnection(dataBase, usuario, pass);
        
        PreparedStatement sqlPreparedStatement = dbConnection.prepareStatement(sqlInsert);
        sqlPreparedStatement.setInt(1, departamento.getDeptNum());
        sqlPreparedStatement.setString(2, departamento.getDeptName());
        sqlPreparedStatement.setString(3, departamento.getDeptLoc());
        
        numRowInserted = sqlPreparedStatement.executeUpdate();
        
        DBConnection.close(sqlPreparedStatement);
        DBConnection.close(dbConnection);
        
        return numRowInserted;
    }

    @Override
    public int updateElement(Departamento departamento) throws SQLException {
        return updateElement(SQL_UPDATE, departamento);
    }

    @Override
    public int updateElement(String sqlUpdate, Departamento departamento) throws SQLException {
        int numRowUpdated = 0;
        
        Connection dbConnection = DBConnection.getConnection(dataBase, usuario, pass);
        
        PreparedStatement sqlPreparedStatement = dbConnection.prepareStatement(sqlUpdate);
        sqlPreparedStatement.setString(1, departamento.getDeptName());
        sqlPreparedStatement.setString(2, departamento.getDeptLoc());
        sqlPreparedStatement.setInt(3, departamento.getDeptNum());
        
        numRowUpdated = sqlPreparedStatement.executeUpdate();
        
        DBConnection.close(sqlPreparedStatement);
        DBConnection.close(dbConnection);
        
        return numRowUpdated;
    }
    
    @Override
    public int deleteElement(int deptNo) throws SQLException {
        int numRowDelete = 0;
        
        Connection dbConnection = DBConnection.getConnection(dataBase, usuario, pass);
        
        PreparedStatement sqlPreparedStatement = dbConnection.prepareStatement(SQL_DELETE);
        sqlPreparedStatement.setInt(1, deptNo);
        numRowDelete = sqlPreparedStatement.executeUpdate();
        
        DBConnection.close(sqlPreparedStatement);
        DBConnection.close(dbConnection);
        
        return numRowDelete;
    }

    @Override
    public int deleteElement(Departamento departamento) throws SQLException {
        return deleteElement(SQL_DELETE, departamento);
    }

    @Override
    public int deleteElement(String sqlDelete, Departamento departamento) throws SQLException {
        int numRowDelete = 0;
        
        Connection dbConnection = DBConnection.getConnection(dataBase, usuario, pass);
        
        PreparedStatement sqlPreparedStatement = dbConnection.prepareStatement(sqlDelete);
        sqlPreparedStatement.setInt(1, departamento.getDeptNum());
        
        numRowDelete = sqlPreparedStatement.executeUpdate();
        
        DBConnection.close(sqlPreparedStatement);
        DBConnection.close(dbConnection);
        
        return numRowDelete;
    }
    
    
    
    
    
}
