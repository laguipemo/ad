/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lgpm.ad03_tarea.model.dao;

import java.sql.SQLException;
import java.util.List;

/**
 *
 * @author lgpmc
 * @param <T>
 */
public interface IConsultor<T> {
    
    public List<T> selectElements() throws SQLException;
    public List<T> selectElements(int limit) throws SQLException;
    public List<T> selectElements(String sqlSelect) throws SQLException;
    public List<T> selectElements(String sqlSelect, int limit) throws SQLException;
    
    public int insertElement(T element) throws SQLException;
    public int insertElement(String sqlInsert, T element) throws SQLException;
    
    public int updateElement(T element) throws SQLException;
    public int updateElement(String sqlUpdate, T element) throws SQLException;
    
    public int deleteElement(int number) throws SQLException;
    public int deleteElement(T element) throws SQLException;
    public int deleteElement(String sqlDelete, T element) throws SQLException;
}
