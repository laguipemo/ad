/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lgpm.ad03_tarea.model.dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import lgpm.ad03_tarea.model.dto.Empleado;

/**
 *
 * @author lgpmc
 */
public class EmpleadoConsultor implements IConsultor<Empleado> {
    
    private static final String SQL_SELECT = 
            "SELECT empno, ename, job, mgr, hiredate, sal, comm, deptno " + 
            "FROM emp";
    private static final String SQL_SELECT_LIMIT = 
            "SELECT empno, ename, job, mgr, hiredate, sal, comm, deptno " + 
            "FROM emp LIMIT ?";
    private static final String SQL_SELECT_ROWNUM = 
            "SELECT empno, ename, job, mgr, hiredate, sal, comm, deptno " + 
            "FROM emp WHERE ROWNUM <=?";
    
    private static final String SQL_INSERT = 
            "INSERT INTO emp " + 
            "(empno, ename, job, mgr, hiredate, sal, comm, deptno) " + 
            "VALUES (?, ?, ?, ?, ?, ?, ?, ?)";
    
    private static final String SQL_UPDATE = 
            "UPDATE emp " + 
            "SET ename=?, job=?, mgr=?, hiredate=?, sal=?, comm=?, deptno=? " + 
            "WHERE empno=?";
    
    private static final String SQL_DELETE = 
            "DELETE FROM emp WHERE empno=?";

    private String dataBase;
    private String usuario;
    private String pass;
    
    
    
    public EmpleadoConsultor() {
        this("ORACLE", "AD03_TAREA", "AD03_TAREA");
    }
    
    public EmpleadoConsultor(String dataBase, String usuario, String pass) {
        if (dataBase.toUpperCase().contains("MYSQL")) {
            this.dataBase = "MYSQL";
        } else {
            this.dataBase = "ORACLE";
        } 
        this.usuario = usuario;
        this.pass = pass;
    }

    
    @Override
    public List<Empleado> selectElements() throws SQLException {
        return selectElements(SQL_SELECT);
    }
    
    @Override
    public List<Empleado> selectElements(int limit) throws SQLException {
        return selectElements(SQL_SELECT, limit);
    }

    @Override
    public List<Empleado> selectElements(String sqlSelect) throws SQLException {
        
        Connection dbConnection = DBConnection.getConnection(dataBase, usuario, pass);
        
        Statement sqlStatement = dbConnection.createStatement();
        
        ResultSet resultSet = sqlStatement.executeQuery(sqlSelect);
        
        List<Empleado> empleadosSelected = new ArrayList<>();
        readEmpleadosSelected(resultSet, empleadosSelected);
        
        DBConnection.close(resultSet);
        DBConnection.close(sqlStatement);
        DBConnection.close(dbConnection);
        
        return empleadosSelected;
    }
    
    @Override
    public List<Empleado> selectElements(String sqlSelect, int limit) throws SQLException {
        
        if (dataBase.toUpperCase().equals("MYSQL")) {
            sqlSelect = SQL_SELECT_LIMIT;
        } else {
            sqlSelect = SQL_SELECT_ROWNUM;
        }
        
        Connection dbConnection = DBConnection.getConnection(dataBase, usuario, pass);
       
        PreparedStatement sqlPreparedStatement = dbConnection.prepareStatement(sqlSelect);
        sqlPreparedStatement.setInt(1, limit);
        
        ResultSet resultSet = sqlPreparedStatement.executeQuery();
        
        List<Empleado> empleadosSelected = new ArrayList<>();
        readEmpleadosSelected(resultSet, empleadosSelected);
        
        DBConnection.close(resultSet);
        DBConnection.close(sqlPreparedStatement);
        DBConnection.close(dbConnection);
        
        return empleadosSelected;
    }

    private void readEmpleadosSelected(
            ResultSet resultSet, List<Empleado> empleadosSelected) throws SQLException {
        
        while (resultSet.next()) {
            int empNum = resultSet.getInt("EMPNO");
            String empName = resultSet.getString("ENAME");
            String empJob = resultSet.getString("JOB");
            int empMgr = resultSet.getInt("MGR");
            LocalDate empHireDate = resultSet.getDate("HIREDATE").toLocalDate();
            double empSal = resultSet.getDouble("SAL");
            double empComm = resultSet.getDouble("COMM");
            int empDeptNo = resultSet.getInt("DEPTNO");
            Empleado empleado = new Empleado(
                    empNum, empName, empJob, empMgr, empHireDate, empSal, empComm, empDeptNo);
            empleadosSelected.add(empleado);
        }
    }
    
    @Override
    public int insertElement(Empleado empleado) throws SQLException {
        return insertElement(SQL_INSERT, empleado);
    }

    @Override
    public int insertElement(String sqlInsert, Empleado empleado) throws SQLException {
        int numRowInserted = 0;
        
        Connection dbConnection = DBConnection.getConnection(dataBase, usuario, pass);
        
        PreparedStatement sqlPreparedStatement = dbConnection.prepareStatement(sqlInsert);
        sqlPreparedStatement.setInt(1, empleado.getEmpNum());
        sqlPreparedStatement.setString(2, empleado.getEmpName());
        sqlPreparedStatement.setString(3, empleado.getEmpJob());
        sqlPreparedStatement.setInt(4, empleado.getEmpMgr());
        sqlPreparedStatement.setDate(5, Date.valueOf(empleado.getEmpHireDate()));
        sqlPreparedStatement.setDouble(6, empleado.getEmpSal());
        sqlPreparedStatement.setDouble(7, empleado.getEmpComm());
        sqlPreparedStatement.setInt(8, empleado.getEmpDeptNo());
        
        numRowInserted = sqlPreparedStatement.executeUpdate();
        
        DBConnection.close(sqlPreparedStatement);
        DBConnection.close(dbConnection);
        
        return numRowInserted;
    }

    @Override
    public int updateElement(Empleado empleado) throws SQLException {
        return updateElement(SQL_UPDATE, empleado);
    }

    @Override
    public int updateElement(String sqlUpdate, Empleado empleado) throws SQLException {
        int numRowUpdated = 0;
        
        Connection dbConnection = DBConnection.getConnection(dataBase, usuario, pass);
        
        PreparedStatement sqlPreparedStatement = dbConnection.prepareStatement(sqlUpdate);
        sqlPreparedStatement.setString(1, empleado.getEmpName());
        sqlPreparedStatement.setString(2, empleado.getEmpJob());
        sqlPreparedStatement.setInt(3, empleado.getEmpMgr());
        sqlPreparedStatement.setDate(4, Date.valueOf(empleado.getEmpHireDate()));
        sqlPreparedStatement.setDouble(5, empleado.getEmpSal());
        sqlPreparedStatement.setDouble(6, empleado.getEmpComm());
        sqlPreparedStatement.setInt(7, empleado.getEmpDeptNo());
        sqlPreparedStatement.setInt(8, empleado.getEmpNum());
        
        numRowUpdated = sqlPreparedStatement.executeUpdate();
        
        DBConnection.close(sqlPreparedStatement);
        DBConnection.close(dbConnection);
        
        return numRowUpdated;
    }
    
    @Override
    public int deleteElement(int empNo) throws SQLException {
        int numRowDelete = 0;
        
        Connection dbConnection = DBConnection.getConnection(dataBase, usuario, pass);
        
        PreparedStatement sqlPreparedStatement = dbConnection.prepareStatement(SQL_DELETE);
        sqlPreparedStatement.setInt(1, empNo);
        numRowDelete = sqlPreparedStatement.executeUpdate();
        
        DBConnection.close(sqlPreparedStatement);
        DBConnection.close(dbConnection);
        
        return numRowDelete;
    }
    
    @Override
    public int deleteElement(Empleado empleado) throws SQLException {
        return deleteElement(SQL_DELETE, empleado);
    }

    @Override
    public int deleteElement(String sqlDelete, Empleado empleado) throws SQLException {
        int numRowDelete = 0;
        
        Connection dbConnection = DBConnection.getConnection(dataBase, usuario, pass);
        
        PreparedStatement sqlPreparedStatement = dbConnection.prepareStatement(sqlDelete);
        sqlPreparedStatement.setInt(1, empleado.getEmpNum());
        
        numRowDelete = sqlPreparedStatement.executeUpdate();
        
        DBConnection.close(sqlPreparedStatement);
        DBConnection.close(dbConnection);
        
        return numRowDelete;
    }

}
