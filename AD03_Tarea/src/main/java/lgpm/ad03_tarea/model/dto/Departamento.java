/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lgpm.ad03_tarea.model.dto;

/**
 *
 * @author lgpmc
 */
public class Departamento {
    
    private int deptNum;
    private String deptName;
    private String deptLoc;

    public Departamento() {
    }

    public Departamento(int deptNo, String deptName, String deptLoc) {
        this.deptNum = deptNo;
        this.deptName = deptName;
        this.deptLoc = deptLoc;
    }

    public int getDeptNum() {
        return deptNum;
    }

    public void setDeptNum(int deptNum) {
        this.deptNum = deptNum;
    }

    public String getDeptName() {
        return deptName;
    }

    public void setDeptName(String deptName) {
        this.deptName = deptName;
    }

    public String getDeptLoc() {
        return deptLoc;
    }

    public void setDeptLoc(String deptLoc) {
        this.deptLoc = deptLoc;
    }

    @Override
    public String toString() {
        return "Departamento{" + "deptNo=" + deptNum + ", deptName=" + deptName + 
                ", deptLoc=" + deptLoc + '}';
    }
    
    
}
