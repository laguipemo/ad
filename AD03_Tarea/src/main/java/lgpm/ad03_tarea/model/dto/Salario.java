/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lgpm.ad03_tarea.model.dto;

/**
 *
 * @author lgpmc
 */
public class Salario {
    
    private int salGrade;
    private double salLo;
    private double salHi;

    public Salario() {
    }

    public Salario(int salGrade, double salLo, double salHi) {
        this.salGrade = salGrade;
        this.salLo = salLo;
        this.salHi = salHi;
    }

    public int getSalGrade() {
        return salGrade;
    }

    public void setSalGrade(int salGrade) {
        this.salGrade = salGrade;
    }

    public double getSalLo() {
        return salLo;
    }

    public void setSalLo(double salLo) {
        this.salLo = salLo;
    }

    public double getSalHi() {
        return salHi;
    }

    public void setSalHi(double salHi) {
        this.salHi = salHi;
    }

    @Override
    public String toString() {
        return "Salarios{" + "salGrade=" + salGrade + ", salLo=" + salLo + 
                ", salHi=" + salHi + '}';
    }
    
    
}
