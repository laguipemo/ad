/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lgpm.ad03_tarea.model.dto;

import java.time.LocalDate;

/**
 *
 * @author lgpmc
 */
public class Empleado {
    
    private int empNum;
    private String empName;
    private String empJob;
    private int empMgr;
    private LocalDate empHireDate;
    private double empSal;
    private double empComm;
    private int empDeptNo;

    public Empleado() {
    }
    
    
    public Empleado(int empNum, String empName, String empJob, int empMgr, LocalDate empHireDate, double empSal, double empComm, int empDeptNo) {
        this.empNum = empNum;
        this.empName = empName;
        this.empJob = empJob;
        this.empMgr = empMgr;
        this.empHireDate = empHireDate;
        this.empSal = empSal;
        this.empComm = empComm;
        this.empDeptNo = empDeptNo;
    }

    public int getEmpNum() {
        return empNum;
    }

    public void setEmpNum(int empNum) {
        this.empNum = empNum;
    }

    public String getEmpName() {
        return empName;
    }

    public void setEmpName(String empName) {
        this.empName = empName;
    }

    public String getEmpJob() {
        return empJob;
    }

    public void setEmpJob(String empJob) {
        this.empJob = empJob;
    }

    public int getEmpMgr() {
        return empMgr;
    }

    public void setEmpMgr(int empMgr) {
        this.empMgr = empMgr;
    }

    public LocalDate getEmpHireDate() {
        return empHireDate;
    }

    public void setEmpHireDate(LocalDate empHireDate) {
        this.empHireDate = empHireDate;
    }

    public double getEmpSal() {
        return empSal;
    }

    public void setEmpSal(double empSal) {
        this.empSal = empSal;
    }

    public double getEmpComm() {
        return empComm;
    }

    public void setEmpComm(double empComm) {
        this.empComm = empComm;
    }

    public int getEmpDeptNo() {
        return empDeptNo;
    }

    public void setEmpDeptNo(int empDeptNo) {
        this.empDeptNo = empDeptNo;
    }

    @Override
    public String toString() {
        return "Empleado{" + "empNumber=" + empNum + ", empName=" + empName + 
                ", empJob=" + empJob + ", empMgr=" + empMgr + ", empHireDate=" + 
                empHireDate + ", empSal=" + empSal + ", empComm=" + empComm + 
                ", empDeptNo=" + empDeptNo + '}';
    }
    
}
