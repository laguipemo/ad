/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lgpm.ad03_introduccion_jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author lgpmc
 */
public class AD03_Introduccion_JDBC {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        String url = "jdbc:mysql://localhost:3306/restaurante?useSSL=false&serverTimezone=UTC";
        
        try {
            Connection conexion = DriverManager.getConnection(url, "root", "");
            Statement sentencia = conexion.createStatement();
            String sql = "SELECT * FROM productos LIMIT 3";
            ResultSet resultadoQuery = sentencia.executeQuery(sql);
            
            while (resultadoQuery.next()) {
                System.out.print("Id Producto: " + resultadoQuery.getInt(1));
                System.out.print(" Nombre: " + resultadoQuery.getString(2));
//                System.out.print(" Descripción: " + resultadoQuery.getString(3));
                System.out.println(" Precio: " + resultadoQuery.getDouble(4));
            }
            resultadoQuery.close();
            sentencia.close();
            conexion.close();
            
        } catch (SQLException sqlEx) {
            System.out.println(sqlEx.getMessage());
        }
    }
    
}
