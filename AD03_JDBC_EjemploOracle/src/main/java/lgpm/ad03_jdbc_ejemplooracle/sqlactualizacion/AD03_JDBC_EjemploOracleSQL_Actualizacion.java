/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lgpm.ad03_jdbc_ejemplooracle.sqlactualizacion;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author lgpmc
 */
public class AD03_JDBC_EjemploOracleSQL_Actualizacion {
    //Defino constante estática con la url de la conexión
    
    // Versión de la url en la que se indica toda la informacion necesaria para
    // la conexión, incluyento valores opciones o que se sulen añadir después 
    // en el constuctor de la conexión
    private static final String URL_JDBC_ORACLE = "jdbc:oracle:thin:laguipemo/321montoto@localhost:1521:xe";
    

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // Declaro e inicializo Connection y Statement a null
        Connection connection = null;
        Statement statement = null;
        int numFilasActualizadas = 0;
        
        try {
            //Obtengo una conexión por medio del método statico getConnection() 
            //de la clase DriverManager, utilizando la firma en la que se requiere
            //solo la url que contiene toda la información
            connection = DriverManager.getConnection(URL_JDBC_ORACLE);
            //creo un statement por medio del método createStatement() de la 
            //conexión creada y lo asigno a la referencia de tipo Statement crada
            //al inicio.
            statement = connection.createStatement();
            
            //Ejemplo actualización de la BBDD por inserción de un registro
            String sqlInsert = "INSERT INTO equipos VALUES('Movistar', 'ESPAGNE')";
            //Ejemplo actualización de la BBDD por actualización de un registro
            String sqlUpdate = "UPDATE equipos SET NOM_EQUIP='Movistar TEAM' WHERE NOM_EQUIP='Movistar'";
            //Ejemplo actualización de la BBDD por borrado de un registro
            String sqlDelete = "DELETE FROM equipos WHERE NOM_EQUIP='Movistar TEAM'";

            //ejecuto la sentencia sqlInsert definida anteriormente
//            numFilasActualizadas = statement.executeUpdate(sqlInsert);
            
            //ejecuto la sentencia sqlUpdate definida anteriormente
//            numFilasActualizadas = statement.executeUpdate(sqlUpdate);
            
            //ejecuto la sentencia sqlDelete definida anteriormente
            numFilasActualizadas = statement.executeUpdate(sqlDelete);
            
            //Muestro el número de filas o registros de la BBDD que se vieron 
            //afectados por la actualización 
            System.out.println(
                    "Se actualizaron " + numFilasActualizadas + 
                            " filas de la Base de Datos.");
            
            // Cierro Statement y Connection
            if (statement != null) {
                statement.close();
            }
            if (connection != null) {
                connection.close();
            }
        } catch (SQLException sqlEx) {
            System.out.println(sqlEx.getMessage());
        }
         
    }
}
