/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lgpm.ad03_jdbc_ejemplooracle.sqlconsulta;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author lgpmc
 */
public class AD03_JDBC_EjemploOracleSQL_Consulta {
    //Toda la información está en el url
//    private static final String URL_JDBC_ORACLE = "jdbc:oracle:thin:laguipemo/321montoto@localhost:1521:XE";
    // El usuario y la contraseña se omiten porque se pasarán al constructor de la conexión
//    private static final String URL_JDBC_ORACLE = "jdbc:oracle:thin:@localhost:1521:XE";
    // El puerto es opcional a menos de que no se esté utilizando el tipico de oracle (1521)
    private static final String URL_JDBC_ORACLE = "jdbc:oracle:thin:@localhost"; 
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // Declaro una referencia a objetos de tipo Connection, Statement y ResultSet
        Connection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;
        try {
            // Inicializo con una conexión obtenida a partir de la clase estática
            // DriverManager y su métod getConnection()
//            connection = DriverManager.getConnection(URL_JDBC_ORACLE); // user/password en la url
            connection = DriverManager.getConnection(URL_JDBC_ORACLE, "laguipemo", "321montoto");
            // Creo un objeto de tipo Statement para la conexión creada a partir
            // de su método createStatement()
            statement = connection.createStatement();
            // Consulta sql a realizar: obtener todas la clumnas de todas la filas
            // de la tabla equipos.
            String sqlQuerry = "SELECT * FROM equipos WHERE NACIONALIDAD='ESPAGNE'";
            // Ejecuto una consulta mediante el método executeQuery() del objeto
            // Statement creado y su resultda lo almaceno en un ResultSet
            resultSet = statement.executeQuery(sqlQuerry);
            // Recorro el ResulSet valiéndome de su método next(), que entrega la
            // siguente fila del resulSet. Para el recorrido utilizo un ciclo while  
            while (resultSet.next()) {
                System.out.println(
                        "Nombre Equipo: " + resultSet.getString("NOM_EQUIP") + 
                                ", Nacionalidad: " + resultSet.getString("NACIONALIDAD"));
            }
            // Cerrar ResultSet, Statement y Connection
            if (resultSet != null) {
                resultSet.close();
            }
            if (statement != null) {
                statement.close();
            }
            if (connection != null) {
                connection.close();
            }
            
        } catch (SQLException sqlEx) {
            System.out.println(sqlEx.getMessage());
        }
    }
}
