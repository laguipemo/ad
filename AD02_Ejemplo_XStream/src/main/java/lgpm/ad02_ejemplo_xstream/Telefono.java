/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lgpm.ad02_ejemplo_xstream;

import java.io.Serializable;

/**
 *
 * @author lgpmc
 */
public class Telefono implements Serializable{
    //atributos o campos que definen o caractrerizan el estado de los objeos de esta clase
    private int codigo;
    private String numTelf;

    //constructor por defecto con el los atributos quedan inicializados a sus valore
    //por defecto.
    public Telefono() {
    }

    //constructor qeu admite lo valores para inicializar los atributos de esta clase
    public Telefono(int codigo, String numTelf) {
        this.codigo = codigo;
        this.numTelf = numTelf;
    }

    //getters and setter para cada uno de los atributos de la clase
    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getNumTelf() {
        return numTelf;
    }

    public void setNumTelf(String numTelf) {
        this.numTelf = numTelf;
    }

    //sobreescritura del método toString para mostrar una representación textual
    //de los objetos de esta clase.
    @Override
    public String toString() {
        return "Telefono{" + "codigo=" + codigo + ", numTelf=" + numTelf + '}';
    }
    
}
