/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lgpm.ad02_ejemplo_xstream;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author lgpmc
 */
public class Datos {
    private static final int LARGO_NOMBRE = 20; //20 char -> 40 bytes
    private static final int LARGO_APELLIDO = 20; // 20 char -> 40 bytes
//    private static final int LARGO_CODIGO_TELEFONO = 32;
    private static final int LARGO_NUMERO = 12; //12 char -> 24 bytes
    private static final long LARGO_REGISTRO = 140; // 4+40+40+4+24+4+24

    private String strFileDatos;
    private String strTipoDatos;

    public Datos() {
    }

    public Datos(String strFileDatos) {
        this.strFileDatos = strFileDatos;
//        this.strTipoDatos = strTipoDatos;
    }

    public List<Persona> generaListaPersonasEjemplo(){

        Persona persona1 = new Persona();
        persona1.setNombre("Lázaro Guillermo");
        persona1.setApellido("Pérez Montoto");
        Telefono telfPersona1 = new Telefono();
        telfPersona1.setCodigo(34);
        telfPersona1.setNumTelf("669-355-999");
        Telefono faxPersona1 = new Telefono();
        faxPersona1.setCodigo(34);
        faxPersona1.setNumTelf("986-508-993");
        persona1.setTelefono(telfPersona1);
        persona1.setFax(faxPersona1);

        Persona persona2 = new Persona();
        persona2.setNombre("Dayamí");
        persona2.setApellido("Carrión Recio");
        Telefono telfPersona2 = new Telefono();
        telfPersona2.setCodigo(34);
        telfPersona2.setNumTelf("616-741-078");
        Telefono faxPersona2 = new Telefono();
        faxPersona2.setCodigo(34);
        faxPersona2.setNumTelf("986-503-942");
        persona2.setTelefono(telfPersona2);
        persona2.setFax(faxPersona2);

        List<Persona> lista = new ArrayList<Persona>(List.of(persona1, persona2));

        return lista;
    }

    public void createFileListaObjetosPersonas() {

        List<Persona> lista = generaListaPersonasEjemplo();
        try (ObjectOutputStream outLista = new ObjectOutputStream(
             new FileOutputStream(new File(strFileDatos), false))){
             outLista.writeObject(lista);
        } catch (FileNotFoundException fnfEx) {
            System.out.println(fnfEx.getMessage());
        } catch (IOException ioEx) {
            System.out.println(ioEx.getMessage());
        }
    }

    public List<Persona> leerFileListaObjetosPersonas() {
        List<Persona> lista = null;
        try (ObjectInputStream inLista = new ObjectInputStream(
                    new FileInputStream(new File(strFileDatos)))) {
            lista = (List<Persona>) inLista.readObject();
        } catch (FileNotFoundException fnfEx) {
            System.out.println(fnfEx.getMessage());
        } catch (IOException ioEx) {
            System.out.println(ioEx.getMessage());
        } catch (ClassNotFoundException clssnotfEx) {
            System.out.println(clssnotfEx.getMessage());
        }

        return lista;
    }

    public void createFileDatosPersonas() {
        try (RandomAccessFile randAccFile = new RandomAccessFile(
                new File(strFileDatos), "rw")) {
            int codPersona = 0;
            long posicion = 0L;
            for (Persona persona: generaListaPersonasEjemplo()) {
                randAccFile.seek(posicion);
                //Escribo código de la persona en el fichero
                randAccFile.writeInt(codPersona);
                //Recupero y escribo nombre persona
                String nombre = persona.getNombre();
                StringBuffer nomBuff = new StringBuffer(nombre);
                nomBuff.setLength(LARGO_NOMBRE);
                randAccFile.writeChars(nomBuff.toString());
                //Recupero y escribo apellido persona
                String apellido = persona.getApellido();
                StringBuffer apelliBuff = new StringBuffer(apellido);
                apelliBuff.setLength(LARGO_APELLIDO);
                randAccFile.writeChars(apelliBuff.toString());
                //Recupero y escribo codigo telefono
                int codTelef = persona.getTelefono().getCodigo();
                randAccFile.writeInt(codTelef);
                //Recupero y escribo telefono
                String telefono = persona.getTelefono().getNumTelf();
                StringBuffer telefBuff = new StringBuffer(telefono);
                telefBuff.setLength(LARGO_NUMERO);
                randAccFile.writeChars(telefBuff.toString());
                //recupero y escribo codigo fax
                int codFax = persona.getFax().getCodigo();
                randAccFile.writeInt(codFax);
                //Recupero y escribo fax
                String fax = persona.getFax().getNumTelf();
                StringBuffer faxBuff = new StringBuffer(fax);
                faxBuff.setLength(LARGO_NUMERO);
                randAccFile.writeChars(faxBuff.toString());

                codPersona += 1;
                posicion += LARGO_REGISTRO;
            }        

        } catch (FileNotFoundException fnfEx) {
            System.out.println(fnfEx.getMessage());
        } catch (IOException ioEx) {
            System.out.println(ioEx.getMessage());
        }
    }


    public List<Persona> leerFileDatosPersonas() {
        List<Persona> lista = new ArrayList<>();

        long posicion = 0;
        try (RandomAccessFile randAccFile = new RandomAccessFile(
                new File(strFileDatos), "r")) {
            while (posicion < randAccFile.length()) {
                randAccFile.seek(posicion);
                int codPersona = randAccFile.readInt();
                if (codPersona != -1) { // no ha sufrido borrado lógico
                    //obtengo el nombre
                    char[] charsNombre = new char[LARGO_NOMBRE];
                    for (int i=0; i<LARGO_NOMBRE; i++) {
                        charsNombre[i] = randAccFile.readChar();
                    }
                    String nombre = new String(charsNombre).trim();

                    //obtengo el apellido
                    char[] charsApelli = new char[LARGO_APELLIDO];
                    for (int i=0; i<LARGO_APELLIDO; i++) {
                        charsApelli[i] = randAccFile.readChar();
                    }
                    String apellido = new String(charsApelli).trim();

                    //obtengo el código del teléfono
                    int codTelef = randAccFile.readInt();

                    //obtengo el teléfono
                    char[] charsTelef = new char[LARGO_NUMERO];
                    for (int i=0; i<LARGO_NUMERO; i++) {
                        charsTelef[i] = randAccFile.readChar();
                    }
                    String telefono = new String(charsTelef).trim();

                    //obtengo el código del fax
                    int codFax = randAccFile.readInt();

                    //obtengo el fax
                    char[] charsFax = new char[LARGO_NUMERO];
                    for (int i=0; i<LARGO_NUMERO; i++) {
                        charsFax[i] = randAccFile.readChar();
                    }
                    String fax = new String(charsFax).trim();

                    lista.add(new Persona(
                            nombre, apellido, new Telefono(codTelef, telefono), 
                            new Telefono(codFax, fax)));
                }
                posicion += LARGO_REGISTRO;
            }

        } catch (FileNotFoundException fnfEx) {
            System.out.println(fnfEx.getMessage());
        } catch (IOException ioEx) {
            System.out.println(ioEx.getMessage());
        }

        return lista;
    } 
}
