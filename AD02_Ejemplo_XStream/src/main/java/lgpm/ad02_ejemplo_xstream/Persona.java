/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lgpm.ad02_ejemplo_xstream;

import java.io.Serializable;

/**
 *
 * @author lgpmc
 */
public class Persona implements Serializable{
    //campos o atributos que caracterizan o definen el estado de un objeto persona
    private String nombre;
    private String apellido;
    private Telefono telefono;
    private Telefono fax;

    //constructor por defecto (los atributos o campos se inicializan a sus valores
    //por defecto: null (Objetos, Strings)
    //             0 datos primitivos numéricos no decimales
    //             0.0 datos primitivos float, double
    //             false datos primitivos boolean
    public Persona() {
    }

    //constructor que admite dos cadenas de textos que serán el nombre y el apellido
    //los demás atributos quedan unicializados a sus valores por defeto
    public Persona(String nombre, String apellido) {
        this.nombre = nombre;
        this.apellido = apellido;
    }

    //constructor que admite dos cadenas con el mimso significado que el anterior
    //y además acepta un objeto de tipo Telefono. El atributo que falta (fax) se 
    //se inicializa a null que sería su valor por defecto al ser un objeto.
    public Persona(String nombre, String apellido, Telefono telefono) {
        this(nombre, apellido);
        this.telefono = telefono;
    }
    
    //constructor que admite un valor de inicialización para cada uno de los atributos
    //de esta clase.
    public Persona(String nombre, String apellido, Telefono telefono, Telefono fax) {
        this(nombre, apellido, telefono);
        this.fax = fax;
    }

    //getters y setters para cada uno de los atributos
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public Telefono getTelefono() {
        return telefono;
    }

    public void setTelefono(Telefono telefono) {
        this.telefono = telefono;
    }

    public Telefono getFax() {
        return fax;
    }

    public void setFax(Telefono fax) {
        this.fax = fax;
    }

    //sobreescritura del metodo toString para mostrar una respresentación en
    //forma de texto de los objetos de esta clase.
    @Override
    public String toString() {
        return "Persona{" + "nombre=" + nombre + ", apellido=" + apellido + ", telefono=" + telefono + ", fax=" + fax + '}';
    }
    
}
