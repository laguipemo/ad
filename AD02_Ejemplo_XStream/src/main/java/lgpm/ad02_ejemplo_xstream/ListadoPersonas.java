/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lgpm.ad02_ejemplo_xstream;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author lgpmc
 */
public class ListadoPersonas {
    // campo o atributo que almacena los objetos persona y que en la serialización
    // a un xml sería el ejemplar o elemento raíz
    private List<Persona> listadoPersonas = new ArrayList<>();

    //constructor por defecto
    public ListadoPersonas() {
    }

    //constructor que inicializa el listado de parsonas con una lista dada
    public ListadoPersonas(List<Persona> listadoPersonas) {
        this.listadoPersonas = listadoPersonas;
    }
    
    // getter que nos permite obtener la lista de personas
    public List<Persona> getListadoPersonas() {
        return listadoPersonas;
    }

    // setter que permite asignar una lista de personas al atributo listadoPersonas
    public void setListadoPersonas(List<Persona> listadoPersonas) {
        this.listadoPersonas = listadoPersonas;
    }
    
    // método que nos permite añidir un objeto de tipo persona a la lista.
    public void addPersona (Persona persona) {
        this.listadoPersonas.add(persona);
    }

    @Override
    public String toString() {
        return "ListadoPersonas{" + "listadoPersonas=" + listadoPersonas + '}';
    }
    
    
    
}
