/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lgpm.ad02_ejemplo_xstream;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;
import com.thoughtworks.xstream.security.AnyTypePermission;
import com.thoughtworks.xstream.security.NoTypePermission;
import com.thoughtworks.xstream.security.NullPermission;
import com.thoughtworks.xstream.security.PrimitiveTypePermission;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.Collection;

/**
 *
 * @author lgpmc
 */
public class AD02_Ejemplo_XStream {
    private ListadoPersonas listadoPersonas = new ListadoPersonas(); // lista con los objetos
    private XStream xstream; // objeto XStream que utilizaremos para la serialización
    
    //constructor por defecto
    public AD02_Ejemplo_XStream() {
        initXStream();
    }
    
    public AD02_Ejemplo_XStream(ListadoPersonas listadoPersonas) {
        this();
        this.listadoPersonas = listadoPersonas;
    }

    //getters and setters de los atributos de la clase
    public ListadoPersonas getListadoPersonas() {
        return listadoPersonas;
    }

    public void setListadoPersonas(ListadoPersonas listadoPersonas) {
        this.listadoPersonas = listadoPersonas;
    }
    
    private void initXStream() {
        try {
            //creo instancia de XStream pasándole un nuevo objeto DomDriver, para
            //el que especifico que utilice la codificación "UTF-8" y se representen
            //correctamente el acentos y otros carecters no ingleses
            xstream = new XStream(new DomDriver("UTF-8"));
            
            //hago uso de alias para definir las etiquetas a utilizar en el xml. Esto
            // no res obligatorio, pero evita tener etiquetas en las que apereza hasta
            //no solo el nombre de la clase o el compo sino que queda incluido también
            //toda la ruta con el o los nombres de los paquetes hasta llegar a la clase
            
            //el método alias me permite indicar que en el fichero xml utilizaré 
            //la etiqueta <personas> para los objetos de la clase ListadoPersonas
            xstream.alias("personas", ListadoPersonas.class);
            xstream.alias("persona", Persona.class);
            xstream.alias("telefono", Telefono.class);
            //con el método aliasField hago lo mismo pero para indicar etiquetas de
            //elementos internos del xml que se corresponden con atributos o campos
            //de un clase dada. En este caso utilizaré la etiqueta <numero> para
            //los atributos numTelf de los objetos de la clase telefono pues lo
            //prefiero a utilizar el nombre del campo como tal 
            xstream.aliasField("numero", Telefono.class, "numTelf");
            
            //Indico la clase y el atributo con la colección de los objetos que formarán
            //parte del ejemplar, es decir, los elementos hijos del ejemplar que van
            //a añadir uno a uno. En este caso, utilizaremos el campo o atributo 
            //listadoPersonas de la clase ListadoPersonas. Para ello hago uso del método 
            //addImplicitCollection(nombreclase.class, "nombreCampoColeccion"), con el
            //que me aseguro la addición implicita sin que se utilce como etiqueta
            //para el ejemplar o elemento raíz, el nombre del atributo como tal
            xstream.addImplicitCollection(ListadoPersonas.class, "listadoPersonas");
            
        } catch (Exception Ex) {
            System.out.println(Ex.getMessage());
        }
        
    }

    private void createXml(String strFileXml) {
        if (xstream != null) {
            try {
                    xstream.toXML(listadoPersonas, 
                            new FileOutputStream(new File(strFileXml), false));
            } catch (FileNotFoundException fnfEx) {
                System.out.println(fnfEx.getMessage());
            }
        } else {
            System.out.println("xsteam para guardar en XMl es nulo");
        } 
    }
    
    private void showXml() {
        if (xstream != null) {
            xstream.toXML(listadoPersonas, System.out);
        } else {
            System.out.println("xsteam para mostrar en consola es nulo");
        } 
    }
    
    private void readFromXml(String strFileXml) {
        // Es necesario, limpiar y establecer  una serie de permisos para 
        // evitar el posible fallo de seguridad:
        //
        //  "Security framework of XStream not explicitly initialized, using 
        //  predefined black list on your own risk."
        //
        // No tengo claro como funciona...
         XStream.setupDefaultSecurity(xstream);
        // clear out existing permissions and set own ones
        xstream.addPermission(NoTypePermission.NONE);
        // allow some basics
        xstream.addPermission(NullPermission.NULL);
        xstream.addPermission(PrimitiveTypePermission.PRIMITIVES);
        xstream.allowTypeHierarchy(Collection.class);
        xstream.addPermission(AnyTypePermission.ANY);
        
        ListadoPersonas listado = (ListadoPersonas) xstream.fromXML(new File(strFileXml));
        for (Persona p: listado.getListadoPersonas()) {
            System.out.printf("  Nombre: %s %n", p.getNombre()); 
            System.out.printf("Apellido: %s %n", p.getApellido());
            System.out.printf("Teléfono: (%s) %s %n", 
                    p.getTelefono().getCodigo(), p.getTelefono().getNumTelf()); 
            System.out.printf("     Fax: (%s) %s %n", 
                    p.getFax().getCodigo(), p.getFax().getNumTelf());
            System.out.println("");
        }
        
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        //nombre del fichero donde guardaré los datos de las personas para el 
        //ejemplo utilizando RandoAccessFile para guardar cada dato.
//        String strFileDatos = "datosPersonas.dat";
        
        //nombre del fichero donde guardaré los datos de las personas para el 
        //ejemplo, pero en este caso serializé el listado de personas mediante
        //un ObjectOutputStream y para leerlo luego con un ObjectInputStream
        String strFileDatos = "listaObjetosPersona.dat";

        //nombre del fichero xml con los objetos serializados
        String strFileXml = strFileDatos.split("\\.")[0] + ".xml";

        //Crear datos del ejemplo, se necesita una vez para crear al menos uno
        //de los ficheros de datos .dat anteriores y luego trabajar
        Datos datos = new Datos(strFileDatos);
//        datos.createFileDatosPersonas(); //fichero de datos idividuales
          datos.createFileListaObjetosPersonas(); // fichero listado serializado
        
        //creo un listado de personas a partir de un fichero de datos idividuales
        //de personas (vía RandomAccessFile)
//        ListadoPersonas listado = new ListadoPersonas(datos.leerFileDatosPersonas());
        
        //creo un listado de personas a partir de un fichero con la lista de personas
        //es decir, un fichero con listado de persona serializado.
        ListadoPersonas listado = new ListadoPersonas(datos.leerFileListaObjetosPersonas());
        
        //Creo una instancia del ejemplo pasándole un listado de personas previamente
        //creado
        AD02_Ejemplo_XStream ejemploXStream = new AD02_Ejemplo_XStream(listado);
        
        if (ejemploXStream != null) {
            //creo fichero xml a de los objetos personas pasados en el listado
            ejemploXStream.createXml(strFileXml);
            //muestro por consola el xml de los objetos personas pasados en el listado
            ejemploXStream.showXml();
            
            System.out.println("\n");
            ejemploXStream.readFromXml("datosPersonas.xml");
            
        } else {
            System.out.println("Objeto ejemploXStream es nulo");
        }
        
    }
}





